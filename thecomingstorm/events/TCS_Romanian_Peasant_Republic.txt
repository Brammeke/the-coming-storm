﻿###########################
# Romanian Events
###########################

add_namespace = RPR_revolt

country_event = {
	id = RPR_revolt.1
	title = RPR_revolt.1.t
	desc = RPR_revolt.1.d
	picture = GFX_event_ROM_Battle_of_Comanesti
		
	is_triggered_only = yes
		
		
	option = {
		name = RPR_revolt.1.a
		ai_chance = { factor = 100 }

	}
}

country_event = {
	id = RPR_revolt.2
	title = RPR_revolt.2.t
	desc = RPR_revolt.2.d
	picture = GFX_event_ROM_Peasant_Revolt_Members
		
	is_triggered_only = yes
		
		
	option = {
		name = RPR_revolt.2.a
		ai_chance = { factor = 100 }

	}
}