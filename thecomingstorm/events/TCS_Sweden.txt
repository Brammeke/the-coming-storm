﻿add_namespace = sweden
add_namespace = sweden_pol

country_event = {
	### Boström steps down ###
	id = sweden_pol.1
	title = sweden_pol.1.title
	desc = sweden_pol.1.desc

	is_triggered_only = yes
	

    fire_only_once = yes
	
	option = { 
		name = sweden_pol.1.a 
		add_political_power = -20
		set_country_flag = Bostrom_gone
		add_ideas = SWE_Johan_Ramstedt
	}
}
country_event = {
	### Ramstedt steps down ###
	id = sweden_pol.2
	title = sweden_pol.2.title
	desc = sweden_pol.2.desc

	is_triggered_only = yes
	
    fire_only_once = yes
	
	option = { 
		name = sweden_pol.2.a 
		add_political_power = -20
		set_country_flag = Ramstedt_gone
		custom_effect_tooltip = sweden2_event_tt
		hidden_effect = {
			add_ideas = {
				SWE_Christian_Lundeberg
				SWE_Fredrik_Wachtmeister
				SWE_Elof_Biesert
				SWE_Gustaf_Berg
			}
		}
		TCS_clear_coalitions = yes
		set_country_flag = soc_liberal_coalition
		set_country_flag = lib_conservatism_coalition
		set_country_flag = soc_conservatism_coalition
		TCS_auto_calc_opposition = yes
	}
}
country_event = {
	### 1905 Election Results ###
	id = sweden_pol.3
	title = sweden_pol.3.title
	desc = sweden_pol.3.desc

	is_triggered_only = yes

    fire_only_once = yes
	
	option = { 
		name = sweden_pol.3.a 
		set_country_flag = Staaff_government
		custom_effect_tooltip = sweden3a_event_tt
		hidden_effect = {
			activate_mission = SWE_pass_voting_reform
			add_ideas = {
				SWE_Karl_Staaff_hg
				SWE_Eric_Trolle
				SWE_Elof_Biesert
				SWE_Karl_Staaff_sm
			}
			set_popularities = {
				leninism = 0
				marxism = 0
				rev_socialism = 1
				soc_democracy = 8
				soc_liberal = 62
				lib_conservatism = 25
				soc_conservatism = 2
				auth_democrat = 2
			}
		}
		set_politics = {
			ruling_party = soc_liberal
		}
		load_focus_tree = SWE_focus_1905_election
		set_variable =  { riksdag_seat_array^0 = 108 }
		set_variable = { riksdag_seat_array^1 = 109 }
		set_variable = { riksdag_seat_array^2 = 13 }
		set_variable =  { riksdag_support_array^0 = 0 }
		set_variable = { riksdag_support_array^1 = 1 }
		set_variable = { riksdag_support_array^2 = 0 }
		clr_country_flag = SWE_dissolve_ties_reforms
		TCS_clear_coalitions = yes
		set_country_flag = issue_voting
		set_country_flag = reform_in_progress
		set_country_flag = soc_liberal_coalition
		TCS_auto_calc_opposition = yes
		SWE_forsta_kammaren_disapproval = yes
		SWE_refresh_riksdag = yes
		
	}
}

country_event = {
	### Staaff steps down ###
	id = sweden_pol.4
	title = sweden_pol.4.title
	desc = sweden_pol.4.desc

	is_triggered_only = yes
	
	option = { 
		name = sweden_pol.4.a 
		add_political_power = -40
		clr_country_flag = Staaff_government
		set_country_flag = Lindman_government
		set_country_flag = failed_voting_reform
		SWE_large_forsta_kammaren_support = yes
		custom_effect_tooltip = sweden4_event_tt
		load_focus_tree = SWE_focus_1905_election_con
		hidden_effect = {
			add_ideas = {
				SWE_Arvid_Lindman
				SWE_Eric_Trolle
				SWE_Carl_Swartz
				SWE_Albert_Petersson
			}
			set_variable =  { riksdag_support_array^0 = 0 }
			set_variable = { riksdag_support_array^1 = 0 }
			set_variable = { riksdag_support_array^2 = 1 }
			SWE_refresh_riksdag = yes
		}
	}
}
country_event = {
	### Karlstad Negotiations ###
	id = sweden_pol.5
	title = sweden_pol.5.title
	desc = sweden_pol.5.desc

	is_triggered_only = yes
	
	option = { 
		name = sweden_pol.5.a 
		add_political_power = 20
		set_country_flag = karlstad_negotiations
		remove_ideas = SWE_failing_union
	}
}