﻿### Germany ###

add_namespace = GER_political
add_namespace = GER_flavor
add_namespace = GER_revolts
add_namespace = GER_economics
add_namespace = GER_military
add_namespace = GER_elections

add_namespace = GER_news
# 1918
add_namespace = GER_political_1918
add_namespace = GER_news_1918

country_event = { #Strikes in the Ruhr
    id = GER_political.1
    title = GER_political.1.title
    desc = GER_political.1.desc
	picture = GFX_event_GER_miner_strike
	
	immediate = {
        hidden_effect = {
            complete_national_focus = GER_bergarbeiterstreik
        }
	}

    # trigger = {
    #     has_start_date < 1910.1.1
    #     date > 1905.1.6
    #     tag = GER
    # }

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political.1.a
    }
}

country_event = { #Maji Maji Rebellion
    id = GER_revolts.1
    title = GER_revolts.1.title
    desc = GER_revolts.1.desc
	picture = GFX_event_GER_maji_maji_rebellion
	
	immediate = {
        hidden_effect = {
			news_event = { days = 1 id = TZN_news.1 }
            TZN = {
				mark_focus_tree_layout_dirty = yes
				load_focus_tree = TCS_Maji_Maji
                transfer_state = 546
				transfer_state = 1022
				add_state_core = 546
				add_state_core = 1022
				add_state_core = 1021
				add_state_core = 1019
				add_state_core = 1023
				add_state_core = 1020
				add_state_core = 1025
				add_state_core = 768
				add_state_core = 769
				set_country_flag = GER_the_maji_maji_rebellion_flag
				set_country_flag = TZN_the_maji_maji_rebellion_ongoing_flag
				add_ideas = TZN_fighting_home_soil
				add_ideas = TZN_german_strategic_superiority_a
				load_oob = TZN_tribals
				country_event = {
					id = TZN_dips.2
					days = 25
				}
            }
			TZN = {
				set_province_controller = 13476
				set_province_controller = 13508
				set_province_controller = 13587
				set_province_controller = 13601
				set_province_controller = 13488
			}
            GER = {
				set_country_flag = GER_the_maji_maji_rebellion_flag
                set_province_controller = 13560
                set_province_controller = 13521
				set_province_controller = 13538
				load_oob = GER_maji_maji_rebellion
				declare_war_on = {
                    target = TZN
                    type = annex_everything
                }
            }
        }
	}

    is_triggered_only = yes
    fire_only_once = yes

    option = { #Gott mit uns!
        name = GER_revolts.1.a
		ai_chance = { factor = 100 }
		TZN = {
			hidden_effect = {
				delete_unit_template_and_units = {
					division_template = "Infanterie-Division"
					disband = no
				}
				delete_unit_template_and_units = {
					division_template = "Königlich Bayerische Infanterie-Division"
					disband = no
				}
				delete_unit_template_and_units = {
					division_template = "Garde-Infanterie-Division"
					disband = no
				}
				delete_unit_template_and_units = {
					division_template = "Größere Infanterie-Division"
					disband = no
				}
				delete_unit_template_and_units = {
					division_template = "Schutztruppe Division"
					disband = no
				}
				country_lock_all_division_template = yes
				set_division_template_lock = {
					division_template = "Idara ya Kikabila"
					is_locked = no
				}
			}
		}
    }
	
    option = { #Mungu ibariki Tangyanika! (Play as the Maji Maji Rebellion)
        name = GER_revolts.1.b
		custom_effect_tooltip = GER_Switch_Maji_Maji_tt
        hidden_effect = {
			TZN = {
				delete_unit_template_and_units = {
					division_template = "Infanterie-Division"
					disband = no
				}
				delete_unit_template_and_units = {
					division_template = "Königlich Bayerische Infanterie-Division"
					disband = no
				}
				delete_unit_template_and_units = {
					division_template = "Garde-Infanterie-Division"
					disband = no
				}
				delete_unit_template_and_units = {
					division_template = "Größere Infanterie-Division"
					disband = no
				}
				delete_unit_template_and_units = {
					division_template = "Schutztruppe Division"
					disband = no
				}
				country_lock_all_division_template = yes
				set_division_template_lock = {
					division_template = "Idara ya Kikabila"
					is_locked = no
				}
			}
            TZN = {
				change_tag_from = GER
            }
        }
		ai_chance = { factor = 0 }
    }
}

country_event = { #Kaiser visits Morocco
    id = GER_political.3
    title = GER_political.3.title
    desc = GER_political.3.desc
	picture = GFX_event_GER_miner_strike
	
	immediate = {
        hidden_effect = {
            TCS_crisis_setup_agadir = yes
            GER = {
                TCS_crisis_join_offensive = yes
            }
            FRA = {
                TCS_crisis_join_defensive = yes
            }
        }
	}

    # trigger = {
    #     has_start_date < 1910.1.1
    #     date > 1905.1.6
    #     tag = GER
    # }

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political.3.a
    }
}

country_event = { #Hidden remove opinion modifiers
    id = GER_economics.1
    title = GER_economics.1.title
    desc = GER_economics.1.desc
    picture = GFX_event_GER_miner_strike
    
    hidden = yes
	
	immediate = {
        hidden_effect = {
            every_country = {
                limit = {
                    has_opinion_modifier = GER_european_trade_opinion
                }
                remove_opinion_modifier = {
                    target = GER
                    modifier = GER_european_trade_opinion
                }
            }
        }
	}

    # trigger = {
    #     has_start_date < 1910.1.1
    #     date > 1905.1.6
    #     tag = GER
    # }

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_economics.1.a
    }
}

country_event = { #Invite Left Liberals (atm 100% chance to say yes)
    id = GER_political.4
    title = GER_political.4.title
    desc = GER_political.4.desc
	picture = GFX_event_GER_miner_strike

    # trigger = {
    #     has_start_date < 1910.1.1
    #     date > 1905.1.6
    #     tag = GER
    # }

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political.4.a
        country_event = {
            id = GER_political.8
            days = 2
        }
    }
}

country_event = { #Invite Free Conservatives (atm 100% chance to say yes)
    id = GER_political.5
    title = GER_political.5.title
    desc = GER_political.5.desc
	picture = GFX_event_GER_miner_strike

    # trigger = {
    #     has_start_date < 1910.1.1
    #     date > 1905.1.6
    #     tag = GER
    # }

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political.5.a
        country_event = {
            id = GER_political.9
            days = 2
        }
    }
}

country_event = { #Invite Right Liberals (atm 100% chance to say yes)
    id = GER_political.6
    title = GER_political.6.title
    desc = GER_political.6.desc
	picture = GFX_event_GER_miner_strike

    # trigger = {
    #     has_start_date < 1910.1.1
    #     date > 1905.1.6
    #     tag = GER
    # }

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political.6.a
        country_event = {
            id = GER_political.10
            days = 2
        }
    }
}

country_event = { #Invite Conservatives (atm 100% chance to say yes)
    id = GER_political.7
    title = GER_political.7.title
    desc = GER_political.7.desc
	picture = GFX_event_GER_miner_strike

    # trigger = {
    #     has_start_date < 1910.1.1
    #     date > 1905.1.6
    #     tag = GER
    # }

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political.7.a
        country_event = {
            id = GER_political.11
            days = 2
        }
    }
}

country_event = { #Left-Liberals Accept
    id = GER_political.8
    title = GER_political.8.title
    desc = GER_political.8.desc
	picture = GFX_event_GER_miner_strike

    # trigger = {
    #     has_start_date < 1910.1.1
    #     date > 1905.1.6
    #     tag = GER
    # }

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political.8.a
    }
}

country_event = { #Free Conservatives Accept
    id = GER_political.9
    title = GER_political.9.title
    desc = GER_political.9.desc
	picture = GFX_event_GER_miner_strike

    # trigger = {
    #     has_start_date < 1910.1.1
    #     date > 1905.1.6
    #     tag = GER
    # }

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political.9.a
    }
}

country_event = { #Right-Liberals Accept
    id = GER_political.10
    title = GER_political.10.title
    desc = GER_political.10.desc
	picture = GFX_event_GER_miner_strike

    # trigger = {
    #     has_start_date < 1910.1.1
    #     date > 1905.1.6
    #     tag = GER
    # }

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political.10.a
    }
}

country_event = { #Conservatives Accept
    id = GER_political.11
    title = GER_political.11.title
    desc = GER_political.11.desc
	picture = GFX_event_GER_miner_strike

    # trigger = {
    #     has_start_date < 1910.1.1
    #     date > 1905.1.6
    #     tag = GER
    # }

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political.11.a
    }
}

country_event = { #Death of Hendrik Witbooi
    id = GER_political.13
    title = GER_political.13.title
    desc = GER_political.13.desc
	picture = GFX_event_GER_miner_strike

    trigger = {
        has_start_date < 1910.1.1
        date > 1905.10.25
        tag = GER
    }

    mean_time_to_happen = {
        days = 10
    }
    
    fire_only_once = yes

    option = {
        name = GER_political.13.a
        custom_effect_tooltip = GER_add_10_procent_control_tt
        hidden_effect = {
			add_to_variable = {
				GER_herero_wars_percentage = 0.10
			}
			clamp_variable = {
				var = GER_herero_wars_percentage
				min = 0
				max = 1
			}
        }
    }
}

country_event = { #1907 Elections
    id = GER_elections.1
    title = GER_elections.1.title
    desc = GER_elections.1.desc
	picture = GFX_event_GER_elections

    trigger = {
        date > 1907.1.24
        date < 1907.1.26
        tag = GER
    }

    fire_only_once = yes

    option = {
        trigger = {
            has_country_flag = GER_amended_mining_laws
        }
        name = GER_elections.1.a
        hidden_effect = {
            set_politics = {	
                ruling_party = lib_conservatism
                last_election = "1908.1.8"
                election_frequency = 48
                elections_allowed = yes
            }
            
            set_popularities = {
                leninism = 1
                marxism = 10
                rev_socialism = 2
                soc_democracy = 16
                soc_liberal = 8
                lib_conservatism = 20
                soc_conservatism = 25
                auth_democrat = 4
                pat_autocrat = 11
                fascism = 3
            }
            load_focus_tree = {
                tree = TCS_Germany_1907
                keep_completed = yes
            }
        }
    }
    option = {
        trigger = {
            NOT = {
                has_country_flag = GER_amended_mining_laws
            }
        }
        name = GER_elections.1.b
        hidden_effect = {
            set_politics = {	
                ruling_party = lib_conservatism
                last_election = "1907.1.12"
                election_frequency = 48
                elections_allowed = yes
            }
            
            set_popularities = {
                leninism = 1
                marxism = 13
                rev_socialism = 8
                soc_democracy = 9
                soc_liberal = 8
                lib_conservatism = 18
                soc_conservatism = 25
                auth_democrat = 4
                pat_autocrat = 11
                fascism = 3
            }
            load_focus_tree = {
                tree = TCS_Germany_1907
                keep_completed = yes
            }
        }
    }
}

country_event = { #Introduction Event
    id = GER_flavor.1
    title = GER_flavor.1.title
    desc = GER_flavor.1.desc
	picture = GFX_event_GER_intro_event

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_flavor.1.a
    }
}

country_event = { #Opening of the Berliner Dom
    id = GER_flavor.2
    title = GER_flavor.2.title
    desc = GER_flavor.2.desc
	picture = GFX_event_GER_berliner_dom

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_flavor.2.a
    }
}

news_event = { ### Founding of the DNVP ###
    id = GER_political_1918.1
    title = GER_political_1918.1.title
    desc = GER_political_1918.1.desc
    picture = GFX_event_GER_founding_dnvp_news
    is_triggered_only = yes
    #fire_only_once = yes

    trigger = {}

    option = {
        name = GER_political_1918.1.a
        hidden_effect = {
            GER_DNVP_founded = yes
        }
    }
}

news_event = { ### Founding of the DVP ###
    id = GER_political_1918.2
    title = GER_political_1918.2.title
    desc = GER_political_1918.2.desc
    picture = GFX_event_GER_founding_dvp_news
    is_triggered_only = yes
    #fire_only_once = yes

    trigger = {}

    option = {
        name = GER_political_1918.2.a
        hidden_effect = {
            GER_DVP_founded = yes
        }
    }
}

news_event = { ### Founding of the KPD ###
    id = GER_political_1918.3
    title = GER_political_1918.3.title
    desc = GER_political_1918.3.desc
    picture = GFX_event_GER_founding_kpd_news
    is_triggered_only = yes
    #fire_only_once = yes

    trigger = {}

    option = {
        name = GER_political_1918.3.a
        hidden_effect = {
            GER_KPD_founded = yes   
        }
    }
}

news_event = { ### Founding of the DAP ###
    id = GER_political_1918.4
    title = GER_political_1918.4.title
    desc = GER_political_1918.4.desc
    picture = GFX_event_GER_founding_dap_news
    is_triggered_only = yes
    #fire_only_once = yes

    trigger = {}

    option = {
        name = GER_political_1918.4.a
        hidden_effect = {
            GER_DAP_founded = yes   
        }
    }
}

country_event = { # Inheritance Tax
    id = GER_economics.6
    title = GER_economics.6.title # Stengel's Inheritance Tax
    desc = GER_economics.6.desc
    picture = GFX_event_picture

    fire_only_once = yes

    trigger = {
        tag = GER
        has_start_date < 1910.1.1
        date > 1905.06.10
    }

    mean_time_to_happen = {
        days = 10
    }

    option = {
        name = GER_economics.6.a # Will this fix out ever increasing debt?
        hidden_effect = {
            country_event = {
                id = GER_economics.3
                days = 20
                random_days = 10
            }
        }
    }
}

country_event = { # Excise Tax
    id = GER_economics.2
    title = GER_economics.2.title # Stengel's Excise Tax
    desc = GER_economics.2.desc
    picture = GFX_event_picture

    fire_only_once = yes

    trigger = {
        tag = GER
        has_start_date < 1910.1.1
        date > 1905.06.02
    }

    mean_time_to_happen = {
        days = 10
    }

    option = {
        name = GER_economics.2.a # This must surely put us out of ever increasing debt
        hidden_effect = {
            country_event = {
                id = GER_economics.4
                days = 20
                random_days = 10
            }
        }
    }
}

country_event = { # Inheritance Tax Results
    id = GER_economics.3
    title = GER_economics.3.title # The Result of Stengel's Inheritance Tax
    desc = GER_economics.3.desc
    picture = GFX_event_picture

    is_triggered_only = yes
    fire_only_once = yes

    trigger = {}

    option = {
        name = GER_economics.3.a # Our economy is spiralling down
        hidden_effect = {
            GER_industrial_collapse = yes
            complete_national_focus = GER_inheritance_tax
        }
    }
}

country_event = { # Excise Tax Results
    id = GER_economics.4
    title = GER_economics.4.title # The Result of Stengel's Excise Tax
    desc = GER_economics.4.desc
    picture = GFX_event_picture

    is_triggered_only = yes
    fire_only_once = yes

    trigger = {}
    
    option = {
        name = GER_economics.4.a
        hidden_effect = {
            GER_industrial_collapse = yes
            complete_national_focus = GER_excise_tax
        }
    }
}

country_event = { # Increasing Debt Crisis
    id = GER_economics.5
    title = GER_economics.5.title
    desc = GER_economics.5.desc
    picture = GFX_event_picture

    is_triggered_only = yes
    fire_only_once = yes

    trigger = {}
    
    option = {
        name = GER_economics.5.a
    }
}

country_event = { #The Signing of the Treaty of Versailles
    id = GER_political_1918.5
    title = GER_political_1918.5.title
    desc = GER_political_1918.5.desc
    picture = GFX_event_picture

    is_triggered_only = yes
    fire_only_once = yes
    
    option = { #Germany might be broken, but peace is restored
        name = GER_political_1918.5.a
        hidden_effect = {
            #Occupation of the Rhine
            RHN = {
                transfer_state = 868
                transfer_state = 869
                transfer_state = 42
                transfer_state = 51
                every_owned_state = {
                    if = {
                        limit = {
                            is_core_of = GER
                        }
                        add_core_of = RHN
                    }
                }
                give_military_access = ENG
                give_military_access = FRA
                give_military_access = BEL
                give_military_access = USA
                set_cosmetic_tag = RHN_ALLIED
            }
            #Polish States
            POL = {
                annex_country = {
                    target = GGP
                }
                transfer_state = 86
                transfer_state = 85
                transfer_state = 762
                every_owned_state = {
                    if = {
                        limit = {
                            is_core_of = GER
                        }
                        add_core_of = POL
                        remove_core_of = GER
                        remove_core_of = GGP
                    }
                }
            }
            GER = {
                every_owned_state = {
                    if = {
                        limit = {
                            is_claimed_by = POL
                        }
                        remove_claim_by = POL
                    }
                }
            }
            #French States
            FRA = {
                transfer_state = 28
                transfer_state = 866
                every_owned_state = {
                    if = {
                        limit = {
                            is_core_of = GER
                        }
                        add_core_of = FRA
                        remove_core_of = GER
                    }
                }
            }
            #Belgian States
            BEL = {
                transfer_state = 853
                every_owned_state = {
                    if = {
                        limit = {
                            is_core_of = GER
                        }
                        add_core_of = BEL
                        remove_core_of = GER
                    }
                }
            }
            #Danish States
            DEN = {
                transfer_state = 863
                every_owned_state = {
                    if = {
                        limit = {
                            is_core_of = GER
                        }
                        add_core_of = DEN
                        remove_core_of = GER
                    }
                }
            }
            #Czech States
            CZE = {
                transfer_state = 878
                every_owned_state = {
                    if = {
                        limit = {
                            is_core_of = GER
                        }
                        add_core_of = CZE
                        remove_core_of = GER
                    }
                }
            }
            #Lithuanian States
            LIT = {
                transfer_state = 188
                every_owned_state = {
                    if = {
                        limit = {
                            is_core_of = GER
                        }
                        add_core_of = LIT
                        remove_core_of = GER
                    }
                }
            }
            #Danzig
            DAN = {
                transfer_state = 876
                every_owned_state = {
                    if = {
                        limit = {
                            is_core_of = GER
                        }
                        add_core_of = DAN
                        remove_core_of = GER
                    }
                }
            }
            #Saarland
            SAA = {
                transfer_state = 867
                every_owned_state = {
                    if = {
                        limit = {
                            is_core_of = GER
                        }
                        add_core_of = SAA
                        remove_core_of = GER
                    }
                }
            }
            #Demilitarized Zones
            50 = {
                set_demilitarized_zone = yes
            }
            871 = {
                set_demilitarized_zone = yes
            }
            870 = {
                set_demilitarized_zone = yes
            }
            # 868 = {
            #     set_demilitarized_zone = yes
            # }
            # 869 = {
            #     set_demilitarized_zone = yes
            # }
            # 42 = {
            #     set_demilitarized_zone = yes
            # }
            # 51 = {
            #     set_demilitarized_zone = yes
            # }
            # 867 = {
            #     set_demilitarized_zone = yes
            # }
            #News Event
            news_event = {
                id = GER_political_1918.6
                hours = 2
            }            
        }
    }
}

news_event = { #The Treaty of Versailles
    id = GER_political_1918.6
    title = GER_political_1918.6.title
    desc = GER_political_1918.6.desc
    picture = GFX_event_GENERIC_treaty_of_versailles

    is_triggered_only = yes

    major = yes

    option = { #Peace is finally upon Europe
        name = GER_political_1918.6.a
    }
}

country_event = { #Ceasefire in Trier
    id = GER_political_1918.7
    title = GER_political_1918.7.title
    desc = GER_political_1918.7.desc
    picture = GFX_event_picture

    is_triggered_only = yes
    fire_only_once = yes
    
    option = { #Stop wars make peace
        name = GER_political_1918.7.a
        hidden_effect = {
            white_peace = {
				tag = GGP
				message = GGP_trier_ceasefire_tt
			}
            POL = {
                puppet = GGP
            }
            GGP = {
                remove_ideas = GGP_freedom
            }
            GGP = { 
                delete_unit = { 
                    state = 86             
                    disband = yes #will refund equipment and manpower
                }
            }
            ##News Event
            #news_event = {
            #    id = GER_political_1918.6
            #    hours = 2
            #}            
        }
    }
}

country_event = { #Greater Poland Uprising
    id = GER_political_1918.8
    title = GER_political_1918.8.title
    desc = GER_political_1918.8.desc
 	picture = GFX_event_GER_1918_greaterpolish_uprising
	
    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political_1918.8.a
        hidden_effect = {
            GGP = {
                transfer_state = 86
                add_state_core = 86
            }
            GER = {
                #set_province_controller = 388
                set_province_controller = 243
                set_province_controller = 3460
                #set_province_controller = 11232
                set_province_controller = 11558
            }
            GER = {
                declare_war_on = {
                    target = GGP
                    type = annex_everything
                }
            }
            GGP = {
                division_template = {
	                name = "Brygada Piechoty" 
	                regiments = {
		                infantry = { x = 0 y = 0 }
		                infantry = { x = 0 y = 1 }
		                infantry = { x = 0 y = 2 }
		                infantry = { x = 1 y = 0 }
                        infantry = { x = 1 y = 1 }
                        artillery_brigade = { x = 2 y = 0 }
	                }
                    support = {
		                artillery = { x = 0 y = 0 }
		                recon = { x = 0 y = 1 }
		                engineer = { x = 0 y = 2 }
	                }
            }   }
            86 = {
			    create_unit = {
					division = "name = \"1 Brygada Straży Ludowej\" division_template = \"Brygada Piechoty\" start_experience_factor = 0.1 start_equipment_factor = 0.90 force_equipment_variants = { infantry_equipment_1 = { owner = GGP } }" 
					owner = GGP
				}
                create_unit = {
					division = "name = \"2 Brygada Straży Ludowej\" division_template = \"Brygada Piechoty\" start_experience_factor = 0.1 start_equipment_factor = 0.90 force_equipment_variants = { infantry_equipment_1 = { owner = GGP } }" 
					owner = GGP
				}
                create_unit = {
					division = "name = \"3 Brygada Straży Ludowej\" division_template = \"Brygada Piechoty\" start_experience_factor = 0.1 start_equipment_factor = 0.90 force_equipment_variants = { infantry_equipment_1 = { owner = GGP } }" 
					owner = GGP
				}
                create_unit = {
					division = "name = \"4 Brygada Straży Ludowej\" division_template = \"Brygada Piechoty\" start_experience_factor = 0.1 start_equipment_factor = 0.90 force_equipment_variants = { infantry_equipment_1 = { owner = GGP } }" 
					owner = GGP
				}
                create_unit = {
					division = "name = \"5 Brygada Straży Ludowej\" division_template = \"Brygada Piechoty\" start_experience_factor = 0.1 start_equipment_factor = 0.90 force_equipment_variants = { infantry_equipment_1 = { owner = GGP } }" 
					owner = GGP
				}
                create_unit = {
					division = "name = \"6 Brygada Straży Ludowej\" division_template = \"Brygada Piechoty\" start_experience_factor = 0.1 start_equipment_factor = 0.90 force_equipment_variants = { infantry_equipment_1 = { owner = GGP } }" 
					owner = GGP
				}
		    }
            news_event = {
				id = GER_news_1918.1
                days = 1
			}
        }
    }
}

news_event = { #Greater Pol. Uprising News
    id = GER_news_1918.1
    title = GER_news_1918.1.title
    desc = GER_news_1918.1.desc
    picture = GFX_event_GER_1918_greaterpolish_uprising_news

    is_triggered_only = yes

    major = yes

    option = {
        name = GER_news_1918.1.a
    }
}

country_event = { # Kaisers Visit to Tangier
    id = GER_political.12
    title = GER_political.12.title
    desc = GER_political.12.desc
    picture = GFX_event_picture

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political.12.a
        hidden_effect = {
            MOR = {
                country_event = {
                    id = MOR_political.1
                    days = 20
                }
            }
        }
    }
}

country_event = { # The Daily-Telegraph Affair
    id = GER_political.14
    title = GER_political.14.title
    desc = GER_political.14.desc
    picture = GFX_event_picture

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political.14.a
        hidden_effect = {
        }
    }
}

country_event = { # The Public Demands Abdication
    id = GER_political.15
    title = GER_political.15.title
    desc = GER_political.15.desc
    picture = GFX_event_picture

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_political.15.a
        hidden_effect = {
        }
    }
    option = {
        name = GER_political.15.b
        hidden_effect = {
            retire_character = GER_wilhelm_ii
            GER_wilhelm_von_preußen = {
                set_character_name = "Wilhelm III"
            }
        }
    }
}


country_event = { # Flight of the LZ 2
    id = GER_flavor.3
    title = GER_flavor.3.title
    desc = GER_flavor.3.desc
    picture = GFX_event_GER_zeppelin_lz2

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_flavor.3.a
    }
}

country_event = { # The Kaiserlichen Aero Clubs
    id = GER_flavor.4
    title = GER_flavor.4.title
    desc = GER_flavor.4.desc
    picture = GFX_event_GER_aero_clubs

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = GER_flavor.4.a
    }
}