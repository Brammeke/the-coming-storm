﻿add_namespace = sweden_diplo

country_event = {
	### Staaff steps down ###
	id = sweden_diplo.1
	title = sweden_diplo.1.title
	desc = sweden_diplo.1.desc

	is_triggered_only = yes
	
	option = { 
		name = sweden_diplo.1.a 
		reverse_add_opinion_modifier = {
			target = SWE
			modifier = SWE_trade_deal
		}
		SWE = {
		    add_ideas = SWE_narvik_lease
		}
	}
	option = { 
		name = sweden_diplo.1.b
		reverse_add_opinion_modifier = {
			target = SWE
			modifier = SWE_rejected_deal
		}
	}
}