﻿########################
### Hungarian Events ###
########################

add_namespace = HUN_political

country_event = { #Novi Sad Assembly declares union with Serbia
    id = HUN_political.1
    title = HUN_political.1.title
    desc = HUN_political.1.desc
	picture = GFX_event_GER_miner_strike

    # trigger = {
    #     has_start_date < 1910.1.1
    #     date > 1905.1.6
    #     tag = GER
    # }

    is_triggered_only = yes
    fire_only_once = yes

    option = {
        name = HUN_political.1.a
		hidden_effect = {
			remove_state_core = 45
			SER = {
				transfer_state = 45
				add_state_core = 45
                country_event = SER_unification.1
			}
		}
    }
}