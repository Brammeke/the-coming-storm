### Spanish Flu ###

add_namespace = SPANISH_FLU_political
#add_namespace = SPANISH_FLU_flavor
#add_namespace = SPANISH_FLU_revolts

country_event = { #1st Major Outbreak
    id = SPANISH_FLU_political.1
    title = SPANISH_FLU_political.1.title
    desc = SPANISH_FLU_political.1.desc
	picture = GFX_event_SPANISH_FLU_military_outbreak
	
	# immediate = {
    #     hidden_effect = {
    #         complete_national_focus = SPANISH_FLU_bergarbeiterstreik
    #     }
	# }

    trigger = {
		# has_start_date < 1910.1.1
		has_war = yes
        date > 1917.1.11
        OR = {
			tag = GER
			tag = FRA
			tag = ENG
			tag = BEL
		}
    }

    #is_triggered_only = yes
    fire_only_once = yes

    option = {
		name = SPANISH_FLU_political.1.a
		add_manpower = -100000
		add_political_power = -100
		add_war_support = -0.1

	}
	option = {
		name = SPANISH_FLU_political.1.b
		add_manpower = -200000
		add_stability = -0.05
    }
}