﻿add_namespace = TCS_XINHAI_revolt
add_namespace = TCS_XINHAI_debug
add_namespace = TCS_XINHAI_political

country_event = { # 1905 Debug Event
	id = TCS_XINHAI_debug.1 
    title = TCS_XINHAI_debug.1.t 
    desc = TCS_XINHAI_debug.1.d 

	is_triggered_only = yes 
	hidden = yes
	fire_only_once = yes

	immediate = {
		set_variable = {
			var = TCS_XINHAI_revolution_days
			value = 2473
		}
		activate_mission = TCS_XINHAI_wuchang_uprising
	}
	
	option = { #
		name = TCS_XINHAI_debug.1.a
	}
}

country_event = { # 1911 Wuchang Uprising
	id = TCS_XINHAI_revolt.1 
    title = TCS_XINHAI_revolt.1.t 
    desc = TCS_XINHAI_revolt.1.d 

	is_triggered_only = yes 
	
	fire_only_once = yes
	
	option = { #
		name = TCS_XINHAI_revolt.1.a
		CHI_XINHAI_revolution_start = yes
	}
	option = { #
		name = TCS_XINHAI_revolt.1.b
		CHI_XINHAI_revolution_start = yes
		hidden_effect = {
            CHI = {
				change_tag_from = QNG
            }
		}
		ai_chance = { factor = 0 }
	}
}

country_event = { # 1911 Hunan Defects
	id = TCS_XINHAI_revolt.2
    title = TCS_XINHAI_revolt.2.t 
    desc = TCS_XINHAI_revolt.2.d 

	is_triggered_only = yes
	fire_only_once = yes
	
	option = { #
		name = TCS_XINHAI_revolt.2.a
		hidden_effect = {
			CHI = {
				transfer_state = 1119
				transfer_state = 1128
				transfer_state = 602
				transfer_state = 1129
				transfer_state = 1118

				drop_cosmetic_tag = yes
				set_cosmetic_tag = CHI_MIL_GOVERNMENT
			}
			CHI = {
				if = {
					limit = {
						country_exists = QNG
					}
					QNG = {
						every_owned_state = {
							add_core_of = CHI
						}
					}
				}
			}
			CHI = {
				CHI_XINHAI_spawn_troops = yes
			}
			activate_mission = TCS_XINHAI_third_wave
		}
	}
}

country_event = { # 1911 Shaanxi/Shanxi/Jiangxi/Yunnan Defects
	id = TCS_XINHAI_revolt.3
    title = TCS_XINHAI_revolt.3.t 
    desc = TCS_XINHAI_revolt.3.d 

	is_triggered_only = yes
	fire_only_once = yes
	
	option = { #
		name = TCS_XINHAI_revolt.3.a
		hidden_effect = {
			CHI = {
				transfer_state = 325
				transfer_state = 747
				transfer_state = 600
				transfer_state = 744
				transfer_state = 622
				transfer_state = 749
				transfer_state = 615
				transfer_state = 746
			}
			CHI = {
				CHI_XINHAI_spawn_troops = yes
			}
			activate_mission = TCS_XINHAI_fourth_wave
		}
	}
}

country_event = { # 1911 Shandong/Zhejiang/Anhui/Fujian/Guangdong/Guangxi/Guizhou/Sichuan/Ningxia Defects
	id = TCS_XINHAI_revolt.4
    title = TCS_XINHAI_revolt.4.t 
    desc = TCS_XINHAI_revolt.4.d 

	is_triggered_only = yes
	fire_only_once = yes
	
	option = { #
		name = TCS_XINHAI_revolt.4.a
		hidden_effect = {
			CHI = {
				transfer_state = 603
				transfer_state = 599
				transfer_state = 594
				transfer_state = 1088
				transfer_state = 591
				transfer_state = 593
				transfer_state = 753
				transfer_state = 595
				transfer_state = 596
				transfer_state = 606
				transfer_state = 605
				transfer_state = 598
				transfer_state = 983
				transfer_state = 597
				transfer_state = 1087
				transfer_state = 981
				transfer_state = 748
				transfer_state = 1120
				transfer_state = 1122
				transfer_state = 751
				transfer_state = 752
				transfer_state = 757
				transfer_state = 613
				transfer_state = 1125
				transfer_state = 1124
				transfer_state = 1123
				transfer_state = 984
				transfer_state = 1121
				transfer_state = 283
				#transfer_state = 756
				#transfer_state = 1117
				transfer_state = 592
			}
			CHI = {
				CHI_XINHAI_spawn_troops = yes
			}
		}
	}
}

country_event = { # Mongolia and Tuva declare Independence
	id = TCS_XINHAI_revolt.5
    title = TCS_XINHAI_revolt.5.t 
    desc = TCS_XINHAI_revolt.5.d 

	is_triggered_only = yes 
	hidden = yes
	fire_only_once = yes

	immediate = {
		hidden_effect = {
			TAN = {
				transfer_state = 329
			}
			MON = {
				drop_cosmetic_tag = yes
				transfer_state = 1080
				retire_country_leader = yes
			}
			QNG = {
				transfer_state = 1079
				transfer_state = 1115
			}
		}
	}
	
	option = { #
		name = TCS_XINHAI_revolt.5.a
	}
}

country_event = { # Tibet declares Independence
	id = TCS_XINHAI_revolt.6
    title = TCS_XINHAI_revolt.6.t 
    desc = TCS_XINHAI_revolt.6.d 

	is_triggered_only = yes 
	hidden = yes
	fire_only_once = yes

	immediate = {
		hidden_effect = {
			ENG = {
				transfer_state = 434
			}
			TIB = {
				drop_cosmetic_tag = yes
			}
		}
	}
	
	option = { #
		name = TCS_XINHAI_revolt.6.a
	}
}

country_event = { # Provisional Republic of China proclaimed
	id = TCS_XINHAI_political.1 
    title = TCS_XINHAI_political.1.t 
    desc = TCS_XINHAI_political.1.d 

	is_triggered_only = yes 
	hidden = yes
	fire_only_once = yes

	immediate = {
		hidden_effect = {
			CHI = {
				drop_cosmetic_tag = yes
				set_cosmetic_tag = CHI_PROV
	
				retire_country_leader = yes
				set_capital = {
					state = 598
				}
			}
		}
	}
	
	option = { #
		name = TCS_XINHAI_political.1.a
	}
}

country_event = { # Qing Emperor abdicated, Republic of China under Yuan Shikai is formed.
	id = TCS_XINHAI_political.2
    title = TCS_XINHAI_political.2.t 
    desc = TCS_XINHAI_political.2.d 

	is_triggered_only = yes 
	hidden = yes
	fire_only_once = yes

	immediate = {
		hidden_effect = {
			CHI = {
				drop_cosmetic_tag = yes
				retire_country_leader = yes
				annex_country = {
					target = QNG
					transfer_troops = no
				}
				annex_country = {
					target = KUM
					transfer_troops = no
				}
				every_owned_state = {
					remove_core_of = QNG
					remove_core_of = KUM
				}
				set_capital = {
					state = 608
				}
				activate_mission = TCS_CHI_the_tibetan_issue
			}
			if = {
				limit = {
					TIB = {
						has_cosmetic_tag = QNG_puppet
					}
				}
				TIB = {
					drop_cosmetic_tag = yes
				}
				ENG = {
					transfer_state = 434
				}
			}
            CHI = {
				change_tag_from = QNG
            }
		}
	}
	
	option = { #
		name = TCS_XINHAI_political.2.a
	}
}