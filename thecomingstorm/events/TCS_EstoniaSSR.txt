########################
###  EstoniaSSR Events  ###
########################
add_namespace = tcs_estonian_commune

country_event = {
    id = tcs_estonian_commune.1
    title = tcs_estonian_commune.1.t
    desc = tcs_estonian_commune.1.d
    picture = GFX_event_LSR_declaration_latvianssr

    is_triggered_only = yes

    option = {
        name = tcs_estonian_commune.a
    }
}