﻿# add_namespace = TCS_Persia_constitutional_revolt

# ########################
# ### social democrats ###
# ########################
# country_event = {
# 	id = TCS_Persia_constitutional_revolt.1
# 	title = TCS_Persia_constitutional_revolt.1.t
# 	desc = TCS_Persia_constitutional_revolt.1.d
# 	picture = GFX_report_event_PER_revolutionary
	
# 	fire_only_once = yes
	
# 	is_triggered_only = yes
	
# 	immediate = {	
# 		set_country_flag = PER_constitutional_revolution_happened
# 	}
	
# 	option = { ####### conservatives
# 		name = TCS_Persia_constitutional_revolt.1.a
# 		if = { limit = { NOT = { has_country_flag = mirza_nasrullah_died } } add_ideas = { mirza_nasrullah_hog } else = { add_ideas = abdol_qasem_molk_hog } }  
# 		remove_ideas = { PER_constitutional_rise }
# 		set_politics = { ruling_party = soc_conservatism }
# 		create_country_leader = {
# 			name = "Iranian Parliament"
# 			desc = "POLITICS_MOHAMMED_ZAHIR_SHAH_DESC"
# 			picture = "gfx/leaders/PER/iranian_parlement.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_conservatism_subtype
# 			traits = {
				
# 			}
# 		}
# 		add_popularity = {
# 			ideology = soc_conservatism
# 			popularity = 0.3
# 		}
# 		complete_national_focus = PER_support_soc_dems
# 		add_ideas = male_suffrage
# 	}
# 	option = { #### soc dem
# 		name = TCS_Persia_constitutional_revolt.1.b
# 		add_ideas = { sattar_khan_hog }
# 		add_ideas = { morteza_gholi_sec }
# 		remove_ideas = { PER_constitutional_rise }
# 		set_politics = { ruling_party = soc_conservatism }
# 		create_country_leader = {
# 			name = "Iranian Parliament"
# 			desc = "POLITICS_MOHAMMED_ZAHIR_SHAH_DESC"
# 			picture = "gfx/leaders/PER/iranian_parlement.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_conservatism_subtype
# 			traits = {
				
# 			}
# 		}
# 		add_popularity = {
# 			ideology = soc_conservatism
# 			popularity = 0.2
# 		}
# 		complete_national_focus = PER_support_soc_dems
# 		add_ideas = male_suffrage
# 	}
# 	option = { #### religious
# 		name = TCS_Persia_constitutional_revolt.1.c
# 		add_ideas = { seyyed_abdollah_hog }
# 		add_ideas = { hossein_naini_sec }
# 		remove_ideas = { PER_constitutional_rise }
# 		set_politics = { ruling_party = soc_conservatism }
# 		create_country_leader = {
# 			name = "Iranian Parliament"
# 			desc = "POLITICS_MOHAMMED_ZAHIR_SHAH_DESC"
# 			picture = "gfx/leaders/PER/iranian_parlement.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_conservatism_subtype
# 			traits = {
				
# 			}
# 		}
# 		add_popularity = {
# 			ideology = soc_conservatism
# 			popularity = 0.2
# 		}
# 		complete_national_focus = PER_support_conservatists
# 		add_ideas = male_suffrage
# 	}
# }
# ### 
# country_event = { #######revolution crushed
# 	id = TCS_Persia_constitutional_revolt.2
# 	title = TCS_Persia_constitutional_revolt.2.t
# 	desc = TCS_Persia_constitutional_revolt.2.d
# 	picture = GFX_report_event_PER_revolutionary
	
# 	fire_only_once = yes
	
# 	is_triggered_only = yes
	
	
# 	option = {
# 		name = TCS_Persia_constitutional_revolt.2.a
# 		remove_ideas = PER_constitutional_rise
# 		add_stability = 0.05
# 		set_country_flag = PER_revolution_crushed
# 		clr_country_flag = PER_rebel_army_events
# 		add_ideas = no_elections
# 	}
# }
# ###
# country_event = { ####### mozaffer abdicates
# 	id = TCS_Persia_constitutional_revolt.3
# 	title = TCS_Persia_constitutional_revolt.3.t
# 	desc = TCS_Persia_constitutional_revolt.3.d
# 	picture = GFX_report_event_PER_ali_shah2
	
# 	fire_only_once = yes 
	
# 	is_triggered_only = yes 
	
# 	option = {
# 		name = TCS_Persia_constitutional_revolt.3.a
# 		if = { limit = { NOT = { has_country_flag = mirza_nasrullah_died } } add_ideas = { mirza_nasrullah_hog } else = { add_ideas = abdol_qasem_molk_hog } }  
# 		hidden_effect = {
# 		create_country_leader = {
# 			name = "Mohammad Ali Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Mohammad Ali Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = pat_autocrat_subtype
# 			traits = {
# 				PER_qajar_monarch
# 				PER_anti_constitutionalist
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Mohammad Ali Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Mohammad Ali Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = auth_democrat_subtype
# 			traits = {
# 				PER_qajar_monarch
# 				PER_anti_constitutionalist
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Mohammad Ali Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Mohammad Ali Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = fascism_subtype
# 			traits = {
# 				PER_qajar_monarch
# 				PER_anti_constitutionalist
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Mohammad Ali Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Mohammad Ali Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_conservatism_subtype
# 			traits = {
# 				PER_qajar_monarch
# 				PER_anti_constitutionalist
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Mohammad Ali Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Mohammad Ali Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_liberal_subtype
# 			traits = {
# 				PER_qajar_monarch
# 				PER_anti_constitutionalist
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Mohammad Ali Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Mohammad Ali Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_democracy_subtype
# 			traits = {
# 				PER_qajar_monarch
# 				PER_anti_constitutionalist
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Mohammad Ali Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Mohammad Ali Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = lib_conservatism_subtype
# 			traits = {
# 				PER_qajar_monarch
# 				PER_anti_constitutionalist
# 			}
# 		}
# 		}
# 		remove_ideas = PER_constitutional_rise
# 		set_politics = {
# 			ruling_party = soc_conservatism
# 		}
# 		add_popularity = {
# 			ideology = soc_conservatism
# 			popularity = 0.25
# 		}
# 		add_ideas = male_suffrage
# 	}
# }
# country_event = { ####### bombard parlement?
# 	id = TCS_Persia_constitutional_revolt.4
# 	title = TCS_Persia_constitutional_revolt.4.t
# 	desc = TCS_Persia_constitutional_revolt.4.d
# 	picture = GFX_report_event_PER_ali_shah2
	
# 	fire_only_once = yes 
	
# 	is_triggered_only = yes 
	
# 	option = { #### bombard!
# 		name = TCS_Persia_constitutional_revolt.4.a
# 		activate_mission_tooltip = PER_constitutional_movement_mission
# 		activate_mission = PER_constitutional_movement_mission
# 		set_country_flag = PER_antiparlement_flag
# 		set_country_flag = PER_rebel_army_events
# 		add_stability = -0.1
# 		var_persia_army_up_10 = yes
# 	}
# }
# ################ ahmad shah time
# country_event = {
# 	id = TCS_Persia_constitutional_revolt.5
# 	title = TCS_Persia_constitutional_revolt.5.t
# 	desc = TCS_Persia_constitutional_revolt.5.d
# 	picture = GFX_report_event_PER_ahmad_shah
	
# 	fire_only_once = yes
	
# 	is_triggered_only = yes
	
# 	immediate = {	
# 		set_country_flag = PER_bombard_parliament_fail
# 	}
# 	immediate = {	
# 		set_country_flag = PER_constitutional_revolution_happened
# 	}
	
# 	option = {
# 		name = TCS_Persia_constitutional_revolt.5.a
# 		if = { limit = { NOT = { has_country_flag = mirza_nasrullah_died } } add_ideas = { mirza_nasrullah_hog } else = { add_ideas = abdol_qasem_molk_hog } }  
# 		remove_ideas = { PER_constitutional_rise }
# 		set_politics = { ruling_party = soc_conservatism }
# 		hidden_effect = {
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = pat_autocrat_subtype
# 			traits = {
# 				PER_qajar_monarch
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = auth_democrat_subtype
# 			traits = {
# 				PER_qajar_monarch
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = fascism_subtype
# 			traits = {
# 				PER_qajar_monarch
				
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_conservatism_subtype
# 			traits = {
# 				PER_qajar_monarch
				
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_liberal_subtype
# 			traits = {
# 				PER_qajar_monarch
				
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_democracy_subtype
# 			traits = {
# 				PER_qajar_monarch
				
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = lib_conservatism_subtype
# 			traits = {
# 				PER_qajar_monarch
# 			}
# 		}
# 		}
# 		add_popularity = {
# 			ideology = soc_conservatism
# 			popularity = 0.3
# 		}
# 		complete_national_focus = PER_support_soc_dems
# 		add_ideas = male_suffrage
# 	}
# 	option = {
# 		name = TCS_Persia_constitutional_revolt.5.b
# 		add_ideas = { sattar_khan_hog }
# 		add_ideas = { morteza_gholi_sec }
# 		remove_ideas = { PER_constitutional_rise }
# 		set_politics = { ruling_party = soc_conservatism }
# 		hidden_effect = {
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = pat_autocrat_subtype
# 			traits = {
# 				PER_qajar_monarch
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = auth_democrat_subtype
# 			traits = {
# 				PER_qajar_monarch
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = fascism_subtype
# 			traits = {
# 				PER_qajar_monarch
				
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_conservatism_subtype
# 			traits = {
# 				PER_qajar_monarch
				
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_liberal_subtype
# 			traits = {
# 				PER_qajar_monarch
				
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_democracy_subtype
# 			traits = {
# 				PER_qajar_monarch
				
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = lib_conservatism_subtype
# 			traits = {
# 				PER_qajar_monarch
# 			}
# 		}
# 		}
# 		add_popularity = {
# 			ideology = soc_conservatism
# 			popularity = 0.2
# 		}
# 		complete_national_focus = PER_support_soc_dems
# 		add_ideas = male_suffrage
# 	}
# 	option = {
# 		name = TCS_Persia_constitutional_revolt.5.c
# 		add_ideas = { seyyed_abdollah_hog }
# 		add_ideas = { hossein_naini_sec }
# 		remove_ideas = { PER_constitutional_rise }
# 		set_politics = { ruling_party = soc_conservatism }
# 		hidden_effect = {
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = pat_autocrat_subtype
# 			traits = {
# 				PER_qajar_monarch
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = auth_democrat_subtype
# 			traits = {
# 				PER_qajar_monarch
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = fascism_subtype
# 			traits = {
# 				PER_qajar_monarch
				
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_conservatism_subtype
# 			traits = {
# 				PER_qajar_monarch
				
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_liberal_subtype
# 			traits = {
# 				PER_qajar_monarch
				
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = soc_democracy_subtype
# 			traits = {
# 				PER_qajar_monarch
				
# 			}
# 		}
# 		create_country_leader = {
# 			name = "Ahmad Shah Qajar"
# 			desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
# 			picture = "gfx/leaders/PER/Ahmad Shah Qajar.tga"
# 			expire = "1965.1.1"
# 			ideology = lib_conservatism_subtype
# 			traits = {
# 				PER_qajar_monarch
# 			}
# 		}
# 		}
# 		add_popularity = {
# 			ideology = soc_conservatism
# 			popularity = 0.2
# 		}
# 		complete_national_focus = PER_support_conservatists
# 		add_ideas = male_suffrage
# 	}
# }
		
		