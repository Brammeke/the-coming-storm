division_template = { #Freikorps
	name = "Freikorps-Division"
	is_locked = yes
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 }
	}
}

units = {
	division = {
		name = "Corps Graf Keller"
		location = 6222
		division_template = "Freikorps-Division"
		start_experience_factor = 0.1
	}
	division = {
		name = "Eisendivision"
		location = 265
		division_template = "Freikorps-Division"
		start_experience_factor = 0.3
	}
}