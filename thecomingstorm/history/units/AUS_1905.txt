﻿division_template = {
	name = "Landwehr-Division"
#	division_names_group = AUS_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }  
		artillery = { x = 0 y = 1 } 
	}
}

division_template = {
	name = "Gebirgsjäger-Division"
#	division_names_group = AUS_MNT_01

	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 } 
		artillery = { x = 0 y = 1 }  
	}
}

division_template = {
	name = "Dragoons" 		#Cavalry
#	division_names_group = AUS_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
	support = {
		recon = { x = 0 y = 0 }     
	}
}

units = {
	##### Österreichisches Bundesheer #####
	# I Korps
	division= {	
		name = "Dragonerregiment Erzherzog Albrecht"
		location = 11542
		division_template = "Dragoons"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3

	}
	division= {	
		name = "Dragonerregiment Graf Paar"
		location = 3569
		division_template = "Dragoons"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3

	}
	division= {	
		name = "Dragonerregiment Kaiser Ferdinand "
		location = 11666
		division_template = "Dragoons"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3

	}
	division= {	
		name = "Dragonerregiment Nikolaus I., Kaiser von Rußland"
		location = 9660
		division_template = "Dragoons"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3

	}
	division= {	# "1. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		location = 11899
		division_template = "Landwehr-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division= {	# "1. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		location = 9427
		division_template = "Landwehr-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division= {	# "1. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		location = 11479
		division_template = "Landwehr-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division= {	# "1. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		location = 577
		division_template = "Landwehr-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division= {	# "1. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		location = 6679
		division_template = "Landwehr-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division= {	# "1. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		location = 9606
		division_template = "Landwehr-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division= {	# "1. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		location = 11899
		division_template = "Landwehr-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division= {	# "1. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		location = 6626
		division_template = "Landwehr-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division= {	# "1. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		location = 9648
		division_template = "Landwehr-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division= {	# "1. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		location = 11520
		division_template = "Landwehr-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division= {	# "1. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		location = 11610
		division_template = "Landwehr-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division= {	
		name = "1. Gebirgstruppendivision"			# Used to form 2. Gebirgs-division post-Anschluss
		location = 11598
		division_template = "Gebirgsjäger-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {	
		name = "2. Gebirgstruppendivision"			# Used to form 2. Gebirgs-division post-Anschluss
		location = 9670
		division_template = "Gebirgsjäger-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {	
		name = "3. Gebirgstruppendivision"			# Used to form 2. Gebirgs-division post-Anschluss
		location = 11539
		division_template = "Gebirgsjäger-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {	
		name = "4. Gebirgstruppendivision"			# Used to form 2. Gebirgs-division post-Anschluss
		location = 3684
		division_template = "Gebirgsjäger-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}

}

### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "AUS"
		}
		requested_factories = 3
		progress = 0.54
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "AUS"
		}
		requested_factories = 2
		progress = 0.74
		efficiency = 100
	}
}