units = {

	##### NAVAL UNITS #####
	### Russian Imperial Navy ###
	## Siberian Flotilla (SF) ##
	fleet = {
		name = "Sibirskaya voyennaya Flot"
		naval_base = 957  # Vladivostok
		task_force = {
			name = "Sibirskaya voyennaya Flot"
			location = 957  # Vladivostok
			ship = { name = "Askold" definition = light_cruiser equipment = { light_cruiser_1 = { amount = 1 owner = RUS version_name = "Askold Class" } } }
			ship = { name = "Serdityy" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = RUS version_name = "Sokol-Class Torpedo Boat" } } }
			ship = { name = "Smelyy" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = RUS version_name = "Sokol-Class Torpedo Boat" } } }
			ship = { name = "Skoryy" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = RUS version_name = "Sokol-Class Torpedo Boat" } } }
			ship = { name = "Statnyy" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = RUS version_name = "Sokol-Class Torpedo Boat" } } }
			ship = { name = "Besposhchadnyy" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = RUS version_name = "Kit Class" } } }
			ship = { name = "Besstrashnyy" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = RUS version_name = "Kit Class" } } }
			ship = { name = "Besshumnyy" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = RUS version_name = "Kit Class" } } }
			ship = { name = "Vlastny" definition = destroyer equipment = { destroyer_2 = { amount = 1 owner = RUS version_name = "Forel Class" } } }
			ship = { name = "Grozovoy" definition = destroyer equipment = { destroyer_2 = { amount = 1 owner = RUS version_name = "Forel Class" } } }
			ship = { name = "Boykiy" definition = destroyer equipment = { destroyer_2 = { amount = 1 owner = RUS version_name = "Boykiy-Class Torpedo Boat" } } }
			ship = { name = "Delfin" definition = submarine equipment = { submarine_1 = { amount = 1 owner = RUS version_name = "Delfin Class" } } }
			ship = { name = "Som" definition = submarine equipment = { submarine_1 = { amount = 1 owner = RUS version_name = "Som-Class Submarine" } } }
			ship = { name = "Osyotr" definition = submarine equipment = { submarine_1 = { amount = 1 owner = RUS version_name = "Osetr Class" } } }
			ship = { name = "Kasatka" definition = submarine equipment = { submarine_1 = { amount = 1 owner = RUS version_name = "Kasatka Class" } } }
			ship = { name = "Nalim" definition = submarine equipment = { submarine_1 = { amount = 1 owner = RUS version_name = "Kasatka Class" } } }
			ship = { name = "Skat" definition = submarine equipment = { submarine_1 = { amount = 1 owner = RUS version_name = "Kasatka Class" } } }
			ship = { name = "Feldmarshal Graf Sheremetyev" definition = submarine equipment = { submarine_1 = { amount = 1 owner = RUS version_name = "Kasatka Class" } } }
		}
	}
}

## STARTING PRODUCTION ##

instant_effect = {

# Submarine (SS): Osyotr (Lake) Class Submarine (X5) ("Bychok" "Paltus" "Plotva" "Kefal" "Sig") - SF
	add_equipment_production = {
		equipment = {
			type = submarine_1
			creator = "RUS"
			version_name = "Osetr Class"
		}
		name = "Bychok"
		requested_factories = 1
		progress = 0.30
		amount = 1
	}
	add_equipment_production = {
		equipment = {
			type = submarine_1
			creator = "RUS"
			version_name = "Osetr Class"
		}
		name = "Paltus"
		requested_factories = 1
		progress = 0.30
		amount = 1
	}
	add_equipment_production = {
		equipment = {
			type = submarine_1
			creator = "RUS"
			version_name = "Osetr Class"
		}
		name = "Plotva"
		requested_factories = 1
		progress = 0.30
		amount = 1
	}
	add_equipment_production = {
		equipment = {
			type = submarine_1
			creator = "RUS"
			version_name = "Osetr Class"
		}
		name = "Kefal"
		requested_factories = 1
		progress = 0.30
		amount = 1
	}
	add_equipment_production = {
		equipment = {
			type = submarine_1
			creator = "RUS"
			version_name = "Osetr Class"
		}
		name = "Sig"
		requested_factories = 1
		progress = 0.30
		amount = 1
	}
}

