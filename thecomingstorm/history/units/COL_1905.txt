﻿division_template = {
	name = "Brigada del Ejército"

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
	}
}

division_template = {
	name = "División de Caballería"
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
}

units = {
	##### Ejército Nacional de Colombia #####
	division = {
		name = "1a Brigada del Ejército"
		location = 10747 # Bogota
		division_template = "Brigada del Ejército"	
		start_experience_factor = 0.4
		start_equipment_factor = 0.8
	}
	division = {
		name = "2a Brigada del Ejército"
		location = 10747 # Bogota
		division_template = "Brigada del Ejército"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.9
	}
	division = {
		name = "3a Brigada del Ejército"
		location = 10846 # Pasto
		division_template = "Brigada del Ejército"	
		start_experience_factor = 0.3
		start_equipment_factor = 0.6
	}
	division = {
		name = "4a Brigada del Ejército"
		location = 7936 # Cali
		division_template = "Brigada del Ejército"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}
	division = {
		name = "5a Brigada del Ejército"
		location = 12790 # Cartegena
		division_template = "Brigada del Ejército"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.7
	}
	division = {
		name = "1a División de Caballería"
		location = 10747 # Pasto
		division_template = "División de Caballería"	
		start_experience_factor = 0.5
		start_equipment_factor = 0.5
	}
}

instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_0
			creator = "COL"
		}
		requested_factories = 1
		progress = 0.47
		efficiency = 100
	}
}