division_template = {
    name = "Koninklijke Militie"
    division_names_group = HOL_MIL_01
    regiments = {
        TCS_militia = { x = 0 y = 0 }
		TCS_militia = { x = 0 y = 1 }
    }
}

units = {
    division = {
        division_name = {
            is_name_ordered = yes
            name_order = 1
        }
		location = 9335
		division_template= "Koninklijke Militie"
		start_experience_factor=0.1
    }
    division = {
        division_name = {
            is_name_ordered = yes
            name_order = 2
        }
		location = 391
		division_template= "Koninklijke Militie"
		start_experience_factor=0.1
    }
    division = {
        division_name = {
            is_name_ordered = yes
            name_order = 3
        }
		location = 6241
		division_template= "Koninklijke Militie"
		start_experience_factor=0.1
    }
}