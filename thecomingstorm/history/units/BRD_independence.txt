division_template = {
    name = "Ubg-okō In-gabo"
    division_names_group = BRD_INF_01
	is_locked = yes
    regiments = {
        TCS_militia = { x = 0 y = 0 }
		TCS_militia = { x = 0 y = 1 }
		TCS_militia = { x = 1 y = 0 }
		TCS_militia = { x = 1 y = 1 }
    }
}

units = {
    division = {
        division_name = {
            is_name_ordered = yes
            name_order = 1
        }
		location = 2220
		division_template= "Ubg-okō In-gabo"
		start_experience_factor = 0.1
    }
    division = {
        division_name = {
            is_name_ordered = yes
            name_order = 2
        }
		location = 2220
		division_template= "Ubg-okō In-gabo"
		start_experience_factor = 0.1
    }
}