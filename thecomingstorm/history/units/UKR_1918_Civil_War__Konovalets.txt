division_template = {
	name = "Zaporizhka Dyviziya"
	is_locked = yes				# Infantry Division
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 0 y = 1 }
		artillery_brigade = { x = 2 y = 0 }
		TCS_armored_car = { x = 3 y = 0 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Sira Dyviziya"
	is_locked = yes				# Infantry Division
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 1 }
		artillery_brigade = { x = 2 y = 0 }
	}
	support = {
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		signal_company = { x = 0 y = 2 }
		field_hospital = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Sichovi Striltsi"
	is_locked = yes				# Infantry Division
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 1 y = 0 }
		artillery_brigade = { x = 2 y = 0 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
		field_hospital = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Okremiy Zahin"
	is_locked = yes				# Infantry Division
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 0 y = 1 }
		cavalry = { x = 2 y = 0 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
}

units = {
	division = {
		name = "Zaporizhka Dyviziya"
		location = 525
		division_template = "Zaporizhka Dyviziya" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.9
	}
	division = {
		name = "Sira Dyviziya"
		location = 418
		division_template = "Sira Dyviziya"
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division = {
		name = "Sichovi Striltsi"
		location = 3412
		division_template = "Sichovi Striltsi" 
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division = {
		name = "Okremiy Zahin"
		location = 6497
		division_template = "Okremiy Zahin" 
		start_experience_factor = 0.7
		start_equipment_factor = 0.7
	}
	##### NO NAVAL UNITS #####
}

### Starting Production ###
### No Production, couse of lack of mil. industry ###