###################################################################

division_template = {
	name = "Rote Armee Miliz"
	regiments = {
		TCS_militia = { x = 0 y = 0 }
		TCS_militia = { x = 0 y = 1 }
		TCS_militia = { x = 1 y = 0 }
		TCS_militia = { x = 1 y = 1 }
	}
}

###################################################################

units = {
	division= { 
		name = "Rote Armee Miliz"
		location = 692 # 
		division_template="Rote Armee Miliz"
		start_experience_factor=0.1
	}
	division= { 
		name = "Rote Armee Miliz"
		location = 708 # 
		division_template="Rote Armee Miliz"
		start_experience_factor=0.1
	}
	division= { 
		name = "Rote Armee Miliz"
		location = 6693 # 
		division_template="Rote Armee Miliz"
		start_experience_factor=0.1
	}
}

