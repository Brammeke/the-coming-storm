﻿capital = 52

oob = "BAV_1918"

# Starting tech
set_technology = {
	infantry_weapons = 1
	gw_artillery = 1
	#early_fighter = 1
}

set_convoys = 10

recruit_character = BAV_kurt_eisner
recruit_character = BAV_ernst_niekisch
recruit_character = BAV_johannes_hoffmann

set_politics = {	
	ruling_party = rev_socialism
	last_election = "1912.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    leninism = 0
    marxism = 12
    rev_socialism = 34
    soc_democracy = 43
    soc_liberal = 3
    lib_conservatism = 3
    soc_conservatism = 4
    auth_democrat = 0
    pat_autocrat = 0
    fascism = 1
}

1918.11.11 = {
	unlock_national_focus = BAV_eisners_revolution
	unlock_national_focus = BAV_cooperation_with_SPD
	unlock_national_focus = BAV_talks_with_unions

	# add_ideas = {
	# 	#BAV_questionable_loyalty_spd
	# }

	inherit_technology = GER
}

set_country_flag = marxism_coalition
set_country_flag = rev_socialism_coalition
set_country_flag = soc_democracy_coalition