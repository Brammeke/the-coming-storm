﻿capital = 105

#oob = ""

set_research_slots = 3

set_technology = {
	tech_support = 1		
	tech_engineers = 1
	# tech_mountaineers = 1
	#early_fighter = 1
	##gwtank = 1
	#basic_light_tank = 1
	infantry_weapons = 1
	infantry_weapons1 = 1
}

set_politics = {
	ruling_party = auth_democrat
	last_election = "1933.3.5"
	election_frequency = 48
	elections_allowed = no

}

set_popularities = {
    leninism = 10
    marxism = 10
    rev_socialism = 10
    soc_democracy = 10
    soc_liberal = 10
    lib_conservatism = 10
    soc_conservatism = 10
    auth_democrat = 10
    pat_autocrat = 10
    fascism = 10
}


