﻿capital = 86

#oob = "GGP_1918"

set_research_slots = 2
set_stability = 0.7
set_war_support = 0.5

add_ideas = {
	extensive_conscription
	war_economy
	universal_suffrage
	GGP_freedom

	GGP_wojciech_korfanty_hos
	GGP_wojciech_korfanty_for
	GGP_stanislaw_adamski
	GGP_wladyslaw_seyda
}

# Starting tech
set_technology = {
	infantry_weapons = 1
	gw_artillery = 1
	#early_fighter = 1
}

1918.1.1 = {
	inherit_technology = POL
}

set_convoys = 10

set_country_flag = leninism_opposition
set_country_flag = marxism_opposition
set_country_flag = rev_socialism_opposition
set_country_flag = soc_democracy_opposition
set_country_flag = soc_liberal_opposition

set_country_flag = lib_conservatism_coalition
set_country_flag = soc_conservatism_coalition
set_country_flag = auth_democrat_coalition
set_country_flag = pat_autocrat_coalition
set_country_flag = fascism_coalition

set_politics = {	
	ruling_party = soc_conservatism
	last_election = "1918.11.8"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    leninism = 0
    marxism = 5
    rev_socialism = 5
    soc_democracy = 5
    soc_liberal = 5
    lib_conservatism = 15
    soc_conservatism = 40
    auth_democrat = 25
    pat_autocrat = 0
    fascism = 0
}

create_country_leader = {
	name = "Adolf Warski"
	desc = "POLITICS_ADOLF_WARSKI_DESC"
	picture = GFX_leader_unknown
	expire = "1965.1.1"
	ideology = leninism_subtype
	traits = {}
}

create_country_leader = {
	name = "Adolf Warski"
	desc = "POLITICS_ADOLF_WARSKI_DESC"
	picture = GFX_leader_unknown
	expire = "1965.1.1"
	ideology = marxism_subtype
	traits = {}
}

create_country_leader = {
	name = "Adolf Warski"
	desc = "POLITICS_ADOLF_WARSKI_DESC"
	picture = GFX_leader_unknown
	expire = "1965.1.1"
	ideology = rev_socialism_subtype
	traits = {}
}

create_country_leader = {
	name = "Józef Biniszkiewicz"
	desc = "POLITICS_JOZEF_BINISZKIEWICZ_DESC"
	picture = "gfx/leaders/leader_unknown.dds"
	expire = "1965.1.1"
	ideology = soc_democracy_subtype
	traits = {
		#
	}
}

create_country_leader = {
	name = "Józef Biniszkiewicz"
	desc = "POLITICS_JOZEF_BINISZKIEWICZ_DESC"
	picture = "gfx/leaders/leader_unknown.dds"
	expire = "1965.1.1"
	ideology = soc_liberal_subtype
	traits = {
		#
	}
}

create_country_leader = {
	name = "Supreme People's Council"
	desc = "POLITICS_SUPREME_PEOPLES_COUNCIL_DESC"
	picture = "gfx/leaders/GGP/Portrait_GGP_Supreme_Peoples_Council.tga"
	expire = "1965.1.1"
	ideology = lib_conservatism_subtype
	traits = {
		#
	}
}

create_country_leader = {
	name = "Supreme People's Council"
	desc = "POLITICS_SUPREME_PEOPLES_COUNCIL_DESC"
	picture = "gfx/leaders/GGP/Portrait_GGP_Supreme_Peoples_Council.tga"
	expire = "1965.1.1"
	ideology = soc_conservatism_subtype
	traits = {
		#
	}
}

create_country_leader = {
	name = "Supreme People's Council"
	desc = "POLITICS_SUPREME_PEOPLES_COUNCIL_DESC"
	picture = "gfx/leaders/GGP/Portrait_GGP_Supreme_Peoples_Council.tga"
	expire = "1965.1.1"
	ideology = auth_democrat_subtype
	traits = {
		#
	}
}

create_country_leader = {
	name = "Supreme People's Council"
	desc = "POLITICS_SUPREME_PEOPLES_COUNCIL_DESC"
	picture = "gfx/leaders/GGP/Portrait_GGP_Supreme_Peoples_Council.tga"
	expire = "1965.1.1"
	ideology = pat_autocrat_subtype
	traits = {
		#
	}
}

create_country_leader = {
	name = "Supreme People's Council"
	desc = "POLITICS_SUPREME_PEOPLES_COUNCIL_DESC"
	picture = "gfx/leaders/GGP/Portrait_GGP_Supreme_Peoples_Council.tga"
	expire = "1965.1.1"
	ideology = fascism_subtype
	traits = {
		#
	}
}

create_corps_commander = {
	name = "Stanisław Taczak"
	#picture = "Portrait_GGP_Stanisław_Taczak.tga"
	traits = { brilliant_strategist trickster }
	skill = 2
	attack_skill = 1
	defense_skill = 3
	planning_skill = 1
	logistics_skill = 2
}