﻿capital = 41

#oob = "SPR"

set_research_slots = 3

set_technology = {
	## trench_warfare = 1
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_recon = 1
	tech_support = 1		
	tech_engineers = 1
	## tech_mountaineers = 1
	gw_artillery = 1
	#early_fighter = 1
	#early_bomber = 1
	naval_bomber1 = 1

}

recruit_character = SPR_alfonso_xiii
recruit_character = SPR_alejandro_lerroux
recruit_character = SPR_josé_asensio_torrado

set_politics = {	
	ruling_party = soc_conservatism
	last_election = "1933.2.16"
	election_frequency = 36
	elections_allowed = no
}

set_popularities = {
    leninism = 0
    marxism = 0
    rev_socialism = 0
    soc_democracy = 9
    soc_liberal = 28
    lib_conservatism = 5
    soc_conservatism = 56
    auth_democrat = 0
    pat_autocrat = 1
    fascism = 1
}

set_convoys = 150
set_stability = 0.5