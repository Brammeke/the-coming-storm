﻿capital = 890

#oob = "LIV_1905"

set_research_slots = 2
set_stability = 0.21
set_war_support = 0.17

# Starting tech
inherit_technology = GER #Gets their base tech from GER

1918.11.11 = {
	add_ideas = {
		disarmed_nation
	
		LIV_nation_on_last_legs
	}
	
	set_country_flag = LIV_duchy_dissolution_flag
	mark_focus_tree_layout_dirty = yes
	load_focus_tree = TCS_Livonia_1918
	complete_national_focus = LIV_creation_ubd
}

set_convoys = 10

recruit_character = LIV_regency_council

set_popularities = {
    leninism = 0
    marxism = 0
    rev_socialism = 0
    soc_democracy = 0
    soc_liberal = 13
    lib_conservatism = 9
    soc_conservatism = 16
    auth_democrat = 52
    pat_autocrat = 10
    fascism = 0
}

set_politics = {
	ruling_party = auth_democrat
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}