﻿capital = 112

#oob = "POR"

set_research_slots = 3

set_technology = {
	# trench_warfare = 1
	infantry_weapons = 1
	infantry_weapons1 = 1
	gw_artillery = 1
	tech_support = 1		
	tech_engineers = 1
	#early_fighter = 1
	#early_bomber = 1
	#CAS1 = 1
}

add_ideas = {
	POR_jose_luciano_de_castro
	POR_antonio_eduardo_vilaca
	POR_manuel_afonso_de_espregueira
	POR_jose_de_alpoim
	POR_powerful_church
	POR_disorganized_armed_forces
	POR_kingdom_in_chaos
}

set_politics = {	
	ruling_party = soc_liberal
	last_election = "1904.6.26"
	election_frequency = 12
	elections_allowed = yes
}

set_popularities = {
    leninism = 0
    marxism = 0
    rev_socialism = 0
    soc_democracy = 1
    soc_liberal = 29
    lib_conservatism = 1
    soc_conservatism = 67
    auth_democrat = 1
    pat_autocrat = 1
    fascism = 0
}

set_convoys = 100
set_stability = 0.50
set_war_support = 0.3

recruit_character = POR_carlos_i
recruit_character = POR_josé_vicente_de_freitas
recruit_character = POR_fernando_tamagnini_de_abreu_e_silva
recruit_character = POR_tomás_a._g._rosado
recruit_character = POR_josé_e._de_m._sarmento
recruit_character = POR_joaquim_a._m.de_albuquerque
