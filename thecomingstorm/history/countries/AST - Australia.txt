﻿capital = 285

#oob = ""

# Starting tech
set_technology = {
	# trench_warfare = 1
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_support = 1		
	tech_engineers = 1
	motorised_infantry = 1
	#early_fighter = 1
}


set_politics = {	
	ruling_party = lib_conservatism
	last_election = "1934.9.15"
	election_frequency = 36
	elections_allowed = yes
}

set_popularities = {
    leninism = 0
    marxism = 0
    rev_socialism = 0
    soc_democracy = 31
    soc_liberal = 30
    lib_conservatism = 27
    soc_conservatism = 12
    auth_democrat = 0
    pat_autocrat = 0
    fascism = 0
}

recruit_character = AST_henry_northcote
recruit_character = AST_henry_mackenzie
recruit_character = AST_alf_cook
recruit_character = AST_frederick_cook
recruit_character = AST_bruce_forster
recruit_character = AST_frederick_tennyson
recruit_character = AST_alf_anderson
recruit_character = AST_adrian_issac
recruit_character = AST_shane_thesinger
recruit_character = AST_thomas_mackenzie


set_convoys = 100