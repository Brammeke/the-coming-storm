﻿capital = 815

#oob = ""

# Starting tech
set_technology = {
	infantry_weapons = 1
	gw_artillery = 1
	#early_fighter = 1
}

set_convoys = 10

set_politics = {	
	ruling_party = pat_autocrat
	last_election = "2126.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    leninism = 0
    marxism = 0
    rev_socialism = 0
    soc_democracy = 0
    soc_liberal = 0
    lib_conservatism = 0
    soc_conservatism = 0
    auth_democrat = 0
    pat_autocrat = 100
    fascism = 0
}

# create_country_leader = {
#     name = "Yayyo ibn Mahammad ibn Hanfadhe"
#     desc = "POLITICS_YAYYO_DESC"
#     #picture = "n"
#     expire = "1965.1.1"
#     ideology = pat_autocrat_subtype
#     traits = {
#         #
#     }
# }