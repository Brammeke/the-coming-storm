﻿capital = 267

#oob = "AFG"

set_technology = {
	infantry_weapons = 1
	##gwtank = 1
	#basic_light_tank = 1
}
set_war_support = 0.5
set_stability = 0.5

give_military_access = ENG


set_politics = {	
	ruling_party = pat_autocrat
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    pat_autocrat = 82
    fascism = 6
    auth_democrat = 12
}

# Mohammad Hashim Khan (Prime Minister) other potential candidate
recruit_character = AFG_habibullah_i
recruit_character = AFG_sardar_shah_wali_khan
recruit_character = AFG_muhammad_sattari
recruit_character = AFG_firuz_aldin_abdul
recruit_character = AFG_akbar_khan
recruit_character = AFG_kamran_ahmadi
recruit_character = AFG_humayun_nawabi
recruit_character = AFG_abdallah_shahnawaz
recruit_character = AFG_murad_ahmadi
recruit_character = AFG_akram_yasin
recruit_character = AFG_abd_alquddus_sherkhanzai
