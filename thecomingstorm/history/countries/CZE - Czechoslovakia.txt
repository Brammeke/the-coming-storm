﻿capital = 9

oob = "CZE"

set_technology = {
	## trench_warfare = 1
	
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_support = 1
	tech_recon = 1
	## tech_mountaineers = 1
	gw_artillery = 1
	##gwtank = 1
	#basic_light_tank = 1
	#improved_light_tank = 1
	interwar_antiair = 1
	#early_fighter = 1
	#early_bomber = 1
	#CAS1 = 1
}

set_research_slots = 2

recruit_character = CZE_national_committee_of_czechoslovakia
recruit_character = CZE_revolutionary_national_assembly
recruit_character = CZE_tomas_masaryk

set_politics = {	
	ruling_party = soc_conservatism
	last_election = "1918.5.19"
	election_frequency = 36
	elections_allowed = yes
}

set_popularities = {
    leninism = 4
    marxism = 2
    rev_socialism = 2
    soc_democracy = 18
    soc_liberal = 11
    lib_conservatism = 26
    soc_conservatism = 26
    auth_democrat = 11
    pat_autocrat = 0
    fascism = 0
}

# set_country_flag = leninism_coalition
# set_country_flag = marxism_coalition
# set_country_flag = rev_socialism_coalition
set_country_flag = soc_democracy_coalition
set_country_flag = soc_liberal_coalition
set_country_flag = lib_conservatism_coalition
set_country_flag = soc_conservatism_coalition
set_country_flag = auth_democrat_coalition
# set_country_flag = pat_autocrat_coalition
# set_country_flag = fascism_coalition

1918.1.1 = { #1918 Start
	# OOB #
	oob = "CZE_1918"
	# Renames #
    9 = { set_state_name = "Severní Čechy" }
	902 = { set_state_name = "Jižní Čechy" }
	75 = { set_state_name = "Jihlava" }
	72 = { set_state_name = "Morava" }
	1007 = { set_state_name = "Frýdek" }
	set_province_name = {
		id = 11542
		name = "Praha"
	}
	set_province_name = {
		id = 6418
		name = "Plzeň"
	}
	set_province_name = {
		id = 9569
		name = "Tábor"
	}
	set_province_name = {
		id = 6562
		name = "Olomouc"
	}
	# Czechoslovak-Hungarian War #
	declare_war_on = {
		target = HUN
		type = annex_everything
	}
	CZE = {
		set_province_controller = 9551
		set_province_controller = 9539
	}
	set_technology = {
		early_bolt_action_mech = 1
		infantry_weapons = 1
		improved_bolt_action_mech = 1
		infantry_weapons1 = 1
		copper_jacketed_bullet = 1
		tubular_magazine = 1
		smokeless_powder = 1
		box_magazine = 1
		improved_infantry_weapons = 1
		stripper_clips = 1
		improved_rifle_locking_lugs = 1
		advanced_infantry_weapons = 1
		detachable_box_magazine = 1
		experimental_light_machine_gun = 1
		increased_clip_size = 1
		improved_ammunition = 1

		gatling_gun = 1
		new_cartridges_and_mech = 1
		hotchkiss_revolving_canon = 1
		elevation_travesability_tripod = 1
		recoil_firing_system_gas_operation = 1
		water_cooling_system = 1
		maxim_gun = 1
		maxim_gun_smokeless = 1
		feed_strip_mg = 1
		machine_gun = 1
		articulated_strip = 1
		advanced_watercooling = 1
		experimental_recoil_mech = 1
		advanced_machine_gun = 1

		horse_drawn_carriages = 1
	
		gw_artillery = 1

		tech_support = 1
		air_cooled_systems = 1
		standardized_support_weapons = 1
		standardized_support_weapons_1919 = 1
		adaptive_equipment = 1
		light_machine_gun = 1
		light_machine_gun_2 = 1
		consolidation_decreased_weight = 1
		modern_infantry_kits = 1
		modern_infantry_weapons = 1
		elephant_gun = 1
	}
}