﻿capital = 774

#oob = ""

# Starting tech
set_technology = {
	infantry_weapons = 1
	gw_artillery = 1
	#early_fighter = 1
}

set_convoys = 10

set_politics = {	
	ruling_party = pat_autocrat
	last_election = "1900.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    auth_democrat = 9
    pat_autocrat = 91
}

# set_country_flag = TCS_allowed_to_raid

recruit_character = WAD_muhammad_salih

add_ideas = {
    WAD_french_incursions
    WAD_imperial_ambitions

    no_elections
}