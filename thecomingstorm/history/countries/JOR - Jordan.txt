﻿capital = 455

# Starting tech
set_technology = {
	infantry_weapons = 1
	##gwtank = 1
}

set_convoys = 5

set_politics = {	
	ruling_party = soc_conservatism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
    soc_conservatism = 25
    fascism = 20
    marxism = 5
    auth_democrat = 50
}

create_country_leader = {
	name = "Hashim Kheir"
	desc = ""
	picture = "gfx/leaders/JOR/Portrait_Arabia_Generic_2.dds"
	ideology = soc_conservatism_subtype
	traits = {
		#
	}
}

create_country_leader = {
	name = "Saeed Ahmadi"
	desc = ""
	picture = "gfx/leaders/JOR/Portrait_Arabia_Generic_fascism1.dds"
	ideology = fascism_subtype
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdullah bin al-Hussein"
	desc = ""
	picture = "gfx/leaders/JOR/Portrait_Arabia_Generic_land_3.dds"
	ideology = auth_democrat_subtype
	traits = {
		#
	}
}

create_country_leader = {
	name = "Fu'ad Nassar"
	desc = ""
	picture = "gfx/leaders/JOR/Portrait_Arabia_Generic_navy_2.dds"
	ideology = marxism_subtype
	traits = {
		#
	}
}