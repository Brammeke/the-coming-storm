﻿capital = 48

oob = "BUL_1905"

set_naval_oob = "BUL_1905_naval"

set_convoys = 5

add_ideas = {
	BUL_Bulgarian_nationalism
	BUL_scatterd_populace
	BUL_agricultural_nation
	BUL_Macedonian_Struggle

	BUL_Racho_Petrov_hog
	BUL_Racho_Petrov_for
	BUL_Lazar_Payakov_eco
	#BUL_Dimitar_Petkov_sec

	limited_conscription
	limited_exports
	sham_elections
	ai_BUL_not_join_OTT
}

set_technology = {
	# Infantry - Machine Guns
	gatling_gun = 1
	new_cartridges_and_mech = 1
	hotchkiss_revolving_canon = 1
	elevation_travesability_tripod = 1
	recoil_firing_system_gas_operation = 1
	water_cooling_system = 1
	maxim_gun = 1
	maxim_gun_smokeless = 1
	feed_strip_mg = 1
	machine_gun = 1
	articulated_strip = 1
	# Infantry Motorized
	horse_drawn_carriages = 1
	steam_wagon = 1
	early_motorised = 1
	# Infantry - Normal
	early_bolt_action_mech = 1
	infantry_weapons = 1
	improved_bolt_action_mech = 1
	infantry_weapons1 = 1
	copper_jacketed_bullet = 1
	tubular_magazine = 1
	smokeless_powder = 1
	box_magazine = 1
	improved_infantry_weapons = 1
	stripper_clips = 1
	improved_rifle_locking_lugs = 1
	advanced_infantry_weapons = 1
	detachable_box_magazine = 1
	advanced_infantry_weapons = 1
	experimental_light_machine_gun = 1
	# Support
	tech_recon = 1
	tech_support = 1
	tech_engineers = 1
	tech_military_police = 1
	# Artillery
	gw_artillery = 1
	# Armoured Vehicles
	tracked_running_gear = 1
	prototype_armored_car = 1
	# Naval Doctrine
	# Naval Ships
	early_tb_destroyer = 1
	# Engineering
	steam_engine = 1
	internal_combustion_engine = 1
}

1905.1.1 = { #Ottoman shit + general 1905 stuff
	if = {
		limit = {
			has_dlc = "Together for Victory"
		}
		OTT = {
			set_autonomy = {
				target = BUL
				autonomous_state = autonomy_vassal
				freedom_level = 0.6 
			}
		}
		else = {
			puppet = BUL
		}
	}

	set_cosmetic_tag = BUL_principality

	set_politics = {
		ruling_party = soc_conservatism
		last_election = "1903.10.19"
		election_frequency = 48
		elections_allowed = no
	}

	set_country_flag = soc_conservatism_coalition
	set_country_flag = auth_democrat_coalition

    set_popularities = {
        leninism = 0
        marxism = 3
        rev_socialism = 0
        soc_democracy = 3
        soc_liberal = 18
        lib_conservatism = 31
        soc_conservatism = 33
        auth_democrat = 10 
        pat_autocrat = 2
        fascism = 0
    }

    set_party_name = { ideology = leninism long_name = BUL_principality_leninism_party_long name = BUL_principality_leninism_party }
 	set_party_name = { ideology = marxism long_name = BUL_principality_marxism_party_long name = BUL_principality_marxism_party }
 	set_party_name = { ideology = rev_socialism long_name = BUL_principality_rev_socialism_party_long name = BUL_principality_rev_socialism_party }
 	set_party_name = { ideology = soc_democracy long_name = BUL_principality_soc_democracy_party_long name = BUL_principality_soc_democracy_party }
 	set_party_name = { ideology = soc_liberal long_name = BUL_principality_soc_liberal_party_long name = BUL_principality_soc_liberal_party }
 	set_party_name = { ideology = lib_conservatism long_name = BUL_principality_lib_conservatism_party_long name = BUL_principality_lib_conservatism_party }
 	set_party_name = { ideology = soc_conservatism long_name = BUL_principality_soc_conservatism_party_long name = BUL_principality_soc_conservatism_party }
 	set_party_name = { ideology = auth_democrat long_name = BUL_principality_auth_democrat_party_long name = BUL_principality_auth_democrat_party }
 	set_party_name = { ideology = pat_autocrat long_name = BUL_principality_pat_autocrat_party_long name = BUL_principality_pat_autocrat_party }
 	set_party_name = { ideology = fascism long_name = BUL_principality_fascism_party_long name = BUL_principality_fascism_party }
}

1910.1.1 = { #Removing 1905 stuff
	OTT = { end_puppet = BUL }
	drop_cosmetic_tag = yes
	clr_country_flag = soc_conservatism_coalition
	clr_country_flag = auth_democrat_coalition

	set_country_flag = BUL_date_1910
	set_country_flag = BUL_declared_independence

	remove_ideas = BUL_Macedonian_Struggle
	remove_ideas = BUL_agricultural_nation
	remove_ideas = BUL_scatterd_populace
	remove_ideas = BUL_Bulgarian_nationalism

	add_ideas = {
		BUL_Bulgarian_nationalism5
		BUL_scatterd_populace
		BUL_agricultural_nation3
		BUL_Macedonian_Struggle
	}

	load_focus_tree = {
 	    tree = TCS_Bulgaria_Malinov
 	    keep_completed = yes
    }

    unlock_national_focus = BUL_Malinov_cabinet
    unlock_national_focus = BUL_ties_russian_empire
    unlock_national_focus = BUL_appeal_stpetersburg
    unlock_national_focus = BUL_appeal_france
    unlock_national_focus = BUL_talks_austrians
    unlock_national_focus = BUL_geshov_incident
    unlock_national_focus = BUL_ORC_railway_question
    unlock_national_focus = BUL_nationalize_ORC
    unlock_national_focus = BUL_ending_university_crisis
    unlock_national_focus = BUL_reverse_Stambolovist_education
    unlock_national_focus = BUL_democratise_local_governing
    unlock_national_focus = BUL_declare_independence
    unlock_national_focus = BUL_end_media_repression

    unlock_national_focus = BUL_Ottoman_compensation
    unlock_national_focus = BUL_Russian_Turkish_convention
    unlock_national_focus = BUL_Bulgarian_Turkish_convention

	set_politics = {
        ruling_party = lib_conservatism
        last_election = "1903.10.19"
        election_frequency = 48
        elections_allowed = no
    }

    set_popularities = {
        leninism = 0
        marxism = 3
        rev_socialism = 0
        soc_democracy = 4
        soc_liberal = 20
        lib_conservatism = 54
        soc_conservatism = 17
        auth_democrat = 2
        pat_autocrat = 0
        fascism = 0
    }
}

1914.1.1 = { #Removing 1910 stuff
	OTT = { end_puppet = BUL }
	drop_cosmetic_tag = yes

	load_focus_tree = TCS_Bulgaria_1914

	set_technology = {
		early_bolt_action_mech = 1
		infantry_weapons = 1
		improved_bolt_action_mech = 1
		infantry_weapons1 = 1
		copper_jacketed_bullet = 1
		tubular_magazine = 1
		smokeless_powder = 1
		box_magazine = 1
		improved_infantry_weapons = 1
		stripper_clips = 1
		improved_rifle_locking_lugs = 1
		advanced_infantry_weapons = 1
		detachable_box_magazine = 1
		experimental_light_machine_gun = 1
		increased_clip_size = 1
		improved_ammunition = 1

		gatling_gun = 1
		new_cartridges_and_mech = 1
		hotchkiss_revolving_canon = 1
		elevation_travesability_tripod = 1
		recoil_firing_system_gas_operation = 1
		water_cooling_system = 1
		maxim_gun = 1
		maxim_gun_smokeless = 1
		feed_strip_mg = 1
		machine_gun = 1
		articulated_strip = 1
		advanced_watercooling = 1
		experimental_recoil_mech = 1
		advanced_machine_gun = 1

		horse_drawn_carriages = 1
	
		gw_artillery = 1

		tech_support = 1
	}
}

1918.11.11 = { 
	OTT = { end_puppet = BUL }
	drop_cosmetic_tag = yes

	set_politics = {	
		ruling_party = lib_conservatism
		last_election = "1901.01.18"
		election_frequency = 48
		elections_allowed = no
	}

	set_popularities = { 
		leninism = 0
		marxism = 14
		rev_socialism = 9
		soc_democracy = 28
		soc_liberal = 12
		lib_conservatism = 22
		soc_conservatism = 15
		auth_democrat = 0
		pat_autocrat = 0
		fascism = 0
	}
	
	set_country_flag = lib_conservatism_coalition
	set_country_flag = soc_conservatism_coalition

	set_party_name = { ideology = leninism long_name = BUL_1918_leninism_party_long name = BUL_1918_leninism_party }
 	set_party_name = { ideology = marxism long_name = BUL_1918_marxism_party_long name = BUL_1918_marxism_party }
 	set_party_name = { ideology = rev_socialism long_name = BUL_1918_rev_socialism_party_long name = BUL_1918_rev_socialism_party }
 	set_party_name = { ideology = soc_democracy long_name = BUL_1918_soc_democracy_party_long name = BUL_1918_soc_democracy_party }
 	set_party_name = { ideology = soc_liberal long_name = BUL_1918_soc_liberal_party_long name = BUL_1918_soc_liberal_party }
 	set_party_name = { ideology = lib_conservatism long_name = BUL_1918_lib_conservatism_party_long name = BUL_1918_lib_conservatism_party }
 	set_party_name = { ideology = soc_conservatism long_name = BUL_1918_soc_conservatism_party_long name = BUL_1918_soc_conservatism_party }
 	set_party_name = { ideology = auth_democrat long_name = BUL_1918_auth_democrat_party_long name = BUL_1918_auth_democrat_party }
 	set_party_name = { ideology = pat_autocrat long_name = BUL_1918_pat_autocrat_party_long name = BUL_1918_pat_autocrat_party }
 	set_party_name = { ideology = fascism long_name = BUL_1918_fascism_party_long name = BUL_1918_fascism_party }

	create_country_leader = {
		name = "Boris III"
		picture = "gfx/leaders/BUL/Portrait_boris.tga"
		expire = "1965.1.1"
		ideology = lib_conservatism_subtype
		traits = {
		#
		}
	}

	load_focus_tree = generic_focus
}

recruit_character = BUL_ferdinand_i
recruit_character = BUL_vasil_kutinchev
recruit_character = BUL_radko_dimitriev
recruit_character = BUL_vladimir_vasov
recruit_character = BUL_kliment_boyadzhiev
recruit_character = BUL_nikola_ivanov
recruit_character = BUL_danail_nikolaev
recruit_character = BUL_stancho_dimitriev


### Ship Variants ###
create_equipment_variant = {
	name = "Classe Durandal"
	type = destroyer_1
	upgrades = {
		#
	}
}