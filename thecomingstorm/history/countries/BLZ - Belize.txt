﻿capital = 311

#oob = ""

# Starting tech
set_technology = {
	infantry_weapons = 1
}

set_convoys = 5

set_politics = {	
	ruling_party = soc_conservatism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
    soc_conservatism = 75
    fascism = 1
    marxism = 1
    auth_democrat = 23
}

#create_country_leader = {
#	
#	name = "Jonas Lote"
#	picture = "gfx//leaders//Africa//Portrait_Africa_Generic_2.dds"
#	expire = "1965.1.1"
#	ideology = auth_democrat_subtype
#	traits = {
#		#
#	}
#}