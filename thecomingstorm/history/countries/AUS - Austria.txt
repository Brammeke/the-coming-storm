﻿capital = 4

oob = "AUS_1905"

set_research_slots = 3

set_technology = {
	# Infantry - Machine Guns
	gatling_gun = 1
	new_cartridges_and_mech = 1
	hotchkiss_revolving_canon = 1
	elevation_travesability_tripod = 1
	recoil_firing_system_gas_operation = 1
	water_cooling_system = 1
	maxim_gun = 1
	maxim_gun_smokeless = 1
	feed_strip_mg = 1
	machine_gun = 1
	articulated_strip = 1
	# Infantry Motorized
	horse_drawn_carriages = 1
	steam_wagon = 1
	early_motorised = 1
	# Infantry - Normal
	early_bolt_action_mech = 1
	infantry_weapons = 1
	improved_bolt_action_mech = 1
	infantry_weapons1 = 1
	copper_jacketed_bullet = 1
	tubular_magazine = 1
	smokeless_powder = 1
	box_magazine = 1
	improved_infantry_weapons = 1
	stripper_clips = 1
	improved_rifle_locking_lugs = 1
	advanced_infantry_weapons = 1
	detachable_box_magazine = 1
	advanced_infantry_weapons = 1
	# Support
	tech_recon = 1
	tech_support = 1
	tech_engineers = 1
	tech_military_police = 1
	# Artillery
	gw_artillery = 1
	# Armoured Vehicles
	tracked_running_gear = 1
	prototype_armored_car = 1
	# Naval Doctrine
	# Naval Ships
	# Engineering
	steam_engine = 1
	internal_combustion_engine = 1
}

add_ideas = {
	AUS_dual_monarchy
	AUS_badeni_legacy
	AUS_hungarian_army_question
	AUS_ethnic_tensions
	AUS_Paul_Gautsch_von_Frankenthurn_hog
	AUS_Artur_von_Bylandt_Rheidt_int
	AUS_Agenor_Maria_Goluchowski_fm
	AUS_Stephan_Burian_von_Rajecz_eco
}

set_politics = {
	ruling_party = soc_conservatism
	last_election = "1901.01.18"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    leninism = 4
    marxism = 6
    rev_socialism = 7
    soc_democracy = 14
    soc_liberal = 9
    lib_conservatism = 8
    soc_conservatism = 29
    auth_democrat = 8
    pat_autocrat = 9
    fascism = 6
}

set_cosmetic_tag = AUS_AUH

1910.1.1 = {#1910 Start
}

1914.1.1 = {
	set_technology = {
		early_bolt_action_mech = 1
		infantry_weapons = 1
		improved_bolt_action_mech = 1
		infantry_weapons1 = 1
		copper_jacketed_bullet = 1
		tubular_magazine = 1
		smokeless_powder = 1
		box_magazine = 1
		improved_infantry_weapons = 1
		stripper_clips = 1
		improved_rifle_locking_lugs = 1
		advanced_infantry_weapons = 1
		detachable_box_magazine = 1
		experimental_light_machine_gun = 1
		increased_clip_size = 1
		improved_ammunition = 1

		gatling_gun = 1
		new_cartridges_and_mech = 1
		hotchkiss_revolving_canon = 1
		elevation_travesability_tripod = 1
		recoil_firing_system_gas_operation = 1
		water_cooling_system = 1
		maxim_gun = 1
		maxim_gun_smokeless = 1
		feed_strip_mg = 1
		machine_gun = 1
		articulated_strip = 1
		advanced_watercooling = 1
		experimental_recoil_mech = 1
		advanced_machine_gun = 1

		horse_drawn_carriages = 1
	
		gw_artillery = 1

		tech_support = 1
	}

	load_focus_tree = TCS_Austria_1914
}

1918.11.11 = { #1918 Start
	drop_cosmetic_tag = yes
	set_cosmetic_tag = AUS_GER
	oob = "AUS_1918"

	remove_ideas = {
	    AUS_dual_monarchy
	    AUS_badeni_legacy
	    AUS_hungarian_army_question
	    AUS_ethnic_tensions
	    AUS_Paul_Gautsch_von_Frankenthurn_hog
	    AUS_Artur_von_Bylandt_Rheidt_int
	    AUS_Agenor_Maria_Goluchowski_fm
	    AUS_Stephan_Burian_von_Rajecz_eco
    }

	set_politics = {
		ruling_party = soc_conservatism
		last_election = "1901.01.18"
		election_frequency = 48
		elections_allowed = no
	}
	set_popularities = {
		leninism = 1
		marxism = 2
		rev_socialism = 3
		soc_democracy = 12
		soc_liberal = 3
		lib_conservatism = 2
		soc_conservatism = 31
		auth_democrat = 46
		pat_autocrat = 0
		fascism = 0
	}
	create_country_leader = {
		name = "Staatsrat"
		desc = "POLITICS_KURT_SCHUSCHNIGG_DESC"
		picture = "gfx/leaders/AUS/Staatsrat.tga"
		expire = "1965.1.1"
		ideology = soc_conservatism_subtype
		traits = {
			#
		}
	}

	set_technology = {
		air_cooled_systems = 1
		standardized_support_weapons = 1
		standardized_support_weapons_1919 = 1
		adaptive_equipment = 1
		light_machine_gun = 1
		light_machine_gun_2 = 1
		consolidation_decreased_weight = 1
		modern_infantry_kits = 1
		modern_infantry_weapons = 1
		elephant_gun = 1
	}
}

set_stability = 0.50

recruit_character = AUS_franz_joseph_i
recruit_character = AUS_friedrich_von_Österreichteschen
recruit_character = AUS_franz_conrad_von_hötzendorf
recruit_character = AUS_hermann_kövess_von_kövesshaza
recruit_character = AUS_svetozar_boroević_von_bojna
recruit_character = AUS_victor_dankl
recruit_character = AUS_karl_von_pflanzerbaltin
recruit_character = AUS_josef_roth
recruit_character = AUS_rudolf_stögersteiner
recruit_character = AUS_arthur_arz_von_straußenburg
recruit_character = AUS_franz_rohr_von_denta
recruit_character = AUS_karl_tersztyánszky
recruit_character = AUS_stjepan_sarkotić
recruit_character = AUS_Alfred_Georg_Freiherr_von_Waldstatten
recruit_character = AUS_ferdinand_goglia
recruit_character = AUS_johann_von_kirchbach_auf_lauterbach
recruit_character = AUS_adolf_von_rhemen
recruit_character = AUS_viktor_graf_von_scheuchenstuel
recruit_character = AUS_moritz_von_auffenberg
recruit_character = AUS_alexander_von_krobatin
recruit_character = AUS_oskar_potiorek
recruit_character = AUS_paul_puhallo
recruit_character = AUS_liborius_ritter_von_frank
recruit_character = AUS_karl_ritter_durski_von_trzasko
#recruit_character = AUS_rudolf_von_brudermann