﻿capital = 439

oob = "RAJ"


set_stability = 0.6
set_war_support = 0.1
# Starting tech
set_technology = {
	# trench_warfare = 1
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_support = 1		
	tech_engineers = 1
	tech_recon = 1
	# trench_warfare = 1
	fleet_in_being = 1
	#CAS1 = 1
	
}

set_convoys = 20

set_cosmetic_tag = RAJ_UK # British Raj


set_politics = {	
	ruling_party = auth_democrat
	last_election = "1899.1.6"
	election_frequency = 72
	elections_allowed = no
}

set_popularities = {
    leninism = 0
    marxism = 0
    rev_socialism = 0
    soc_democracy = 4
    soc_liberal = 7
    lib_conservatism = 5
    soc_conservatism = 11
    auth_democrat = 70
    pat_autocrat = 3
    fascism = 0
}


create_country_leader = {
	name = "Lord Curzon"
	desc = "POLITICS_LORD_CURZON_DESC"
	picture = "gfx/leaders/RAJ/Portrait_British_Raj_Lord_Curzon.tga"
	expire = "1925.1.1"
	ideology = auth_democrat_subtype
	traits = {
		
	}
}

#create_country_leader = {
#	name = "Lord Minto"
#	desc = "POLITICS_LORD_MINTO_DESC"
#	picture = "gfx/leaders/RAJ/Portrait_British_Raj_Lord_Minto.tga"
#	expire = "1914.1.1"
#	ideology = auth_democrat_subtype
#	traits = {
#		
#	}
#}

# REVISIT Unclear if appropriate for this ideology
create_country_leader = {
	name = "V. D. Savarkar"
	desc = "POLITICS_VD_SAVARKAR_DESC"
	picture = "GFX_RAJ_veer_savarkar"
	expire = "1965.1.1"
	ideology = fascism_subtype
	traits = {
		
	}
}

create_country_leader = {
	name = "P. Krishna Pillai"
	desc = "POLITICS_P_KRISHNA_PILLAI_DESC"
	picture = "GFX_RAJ_krishna_pillai"
	expire = "1965.1.1"
	ideology = marxism_subtype
	traits = {
		
	}
}

create_country_leader = {
	name = "B. P. Sitaramayya"
	desc = "POLITICS_B_P_SITARAMAYYA_DESC"
	picture = "GFX_RAJ_pattabhi_sitaramayya"
	expire = "1965.1.1"
	ideology = soc_conservatism_subtype
	traits = {
		
	}
}

create_corps_commander = {
	name = "Noel Beresford-Peirse"
	gfx = "GFX_RAJ_noel_beresford_peirse"
	traits = { armor_officer }
	skill = 3
	attack_skill = 3
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 1
}

create_corps_commander = {
	name = "Frank Messervy"
	gfx = "GFX_RAJ_frank_messervy"
	traits = { hill_fighter }
	skill = 3
	attack_skill = 2
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Douglas Gracey"
	gfx = "GFX_RAJ_douglas_gracey"
	traits = { desert_fox }
	skill = 3
	attack_skill = 1
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 3
}

create_navy_leader = {
	name = "Herbert Fitzherbert"
	gfx = "GFX_RAJ_herbert_fitzherbert"
	traits = { }
	skill = 3
}