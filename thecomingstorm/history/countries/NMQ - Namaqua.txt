﻿capital = 1036

oob = "NMQ_1905"

set_research_slots = 3

set_technology = {
	tech_support = 1		
	tech_engineers = 1
	# tech_mountaineers = 1
	#early_fighter = 1
	##gwtank = 1
	#basic_light_tank = 1
	infantry_weapons = 1
	infantry_weapons1 = 1

}

# declare_war_on = {
# 	target = GER
# 	type = annex_everything
# }

recruit_character = NMQ_samuel_maharero

set_politics = {
	ruling_party = auth_democrat
	last_election = "1933.3.5"
	election_frequency = 48
	elections_allowed = no
}

add_ideas = {
	NMQ_herero_wars
}

set_popularities = {
    leninism = 0
    marxism = 0
    rev_socialism = 0
    soc_democracy = 0
    soc_liberal = 0
    lib_conservatism = 0
    soc_conservatism = 0
    auth_democrat = 66
    pat_autocrat = 32
    fascism = 2
}
