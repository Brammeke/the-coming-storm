﻿capital = 604

#oob = "XSM"

# Starting tech
set_technology = {
	infantry_weapons = 1
	#mass_assault = 1
}

recruit_character = XSM_ma_qi

set_politics = {
	ruling_party = auth_democrat
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    soc_conservatism = 0
    fascism = 0
    marxism = 0
    auth_democrat = 100
}