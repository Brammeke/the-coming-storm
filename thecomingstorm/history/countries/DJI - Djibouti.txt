﻿capital = 268

# Starting tech
set_technology = {
	infantry_weapons = 1
}

set_convoys = 10

set_politics = {	
	ruling_party = auth_democrat
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    soc_conservatism = 25
    fascism = 20
    marxism = 5
    auth_democrat = 50
}