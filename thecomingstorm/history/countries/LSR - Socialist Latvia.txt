capital = 885

oob = "LSR"

set_research_slots = 2
set_stability = 0.28
set_war_support = 0.34

give_military_access = SOV

# Starting tech
set_technology = {
	# Infantry - Machine Guns
	gatling_gun = 1
	new_cartridges_and_mech = 1
	hotchkiss_revolving_canon = 1
	elevation_travesability_tripod = 1
	recoil_firing_system_gas_operation = 1
	water_cooling_system = 1
	maxim_gun = 1
	maxim_gun_smokeless = 1
	feed_strip_mg = 1
	machine_gun = 1
	articulated_strip = 1
	# Infantry - Normal
	early_bolt_action_mech = 1
	infantry_weapons = 1
	improved_bolt_action_mech = 1
	infantry_weapons1 = 1
	copper_jacketed_bullet = 1
	tubular_magazine = 1
	smokeless_powder = 1
	box_magazine = 1
	improved_infantry_weapons = 1
	stripper_clips = 1
	improved_rifle_locking_lugs = 1
	advanced_infantry_weapons = 1
	detachable_box_magazine = 1
	experimental_light_machine_gun = 1
	# Infantry Motorized
	horse_drawn_carriages = 1
	# Support
	tech_recon = 1
	tech_support = 1
	tech_engineers = 1
	tech_military_police = 1
	# Artillery
	gw_artillery = 1
	# Armoured Vehicles
	tracked_running_gear = 1
	prototype_armored_car = 1
	# Naval Ships
	early_tb_destroyer = 1
	early_protected_cruiser = 1
	early_armoured_cruiser = 1
	early_submarine = 1
}

# Leaders & Ministers
recruit_character = LSR_peteris_stucka
# Generals
recruit_character = LSR_peteris_slavens

set_convoys = 10

set_politics = {
	ruling_party = leninism
	last_election = "1905.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    leninism = 41
    marxism = 18
    rev_socialism = 5
    soc_democracy = 19
    soc_liberal = 8
    pat_autocrat = 9
}

add_ideas = {
	no_elections
	limited_conscription
	partial_economic_mobilisation

	LSR_peteris_stucka
	LSR_karl_peterson
	LSR_rudolf_endrups
	LSR_janis_lencmanis
	
	LSR_soviet_military_backing
	LSR_legacy_iskolat
}

set_country_flag = leninism_coalition
set_country_flag = marxism_coalition
#set_country_flag = rev_socialism_coalition
# set_country_flag = soc_democracy_coalition
# set_country_flag = soc_liberal_coalition
# set_country_flag = lib_conservatism_coalition
# set_country_flag = soc_conservatism_coalition
# set_country_flag = auth_democrat_coalition
# set_country_flag = pat_autocrat_coalition
# set_country_flag = fascism_coalition

1914.1.1 = { #1914 Start
    set_technology = {
		improved_infantry_weapons = 1
		stripper_clips = 1
		improved_rifle_locking_lugs = 1
		advanced_infantry_weapons = 1
		detachable_box_magazine = 1
		experimental_light_machine_gun = 1
		increased_clip_size = 1
		improved_ammunition = 1

		advanced_watercooling = 1
		experimental_recoil_mech = 1
		advanced_machine_gun = 1
	}
}

1918.11.11 = { #1918 Start
	set_technology = {
		air_cooled_systems = 1
		light_machine_gun = 1
		consolidation_decreased_weight = 1
		light_machine_gun_2 = 1
		adaptive_equipment = 1
		modern_infantry_kits = 1
		elephant_gun = 1
		modern_infantry_weapons = 1
	}
}