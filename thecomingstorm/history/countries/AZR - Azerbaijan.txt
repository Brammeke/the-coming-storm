﻿capital = 229 #Baku

#oob = "AZR"

set_research_slots = 3

# Starting tech
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_recon = 1
	tech_support = 1		
	tech_engineers = 1
	tech_military_police = 1
	# tech_mountaineers = 1
	motorised_infantry = 1
	#paratroopers = 1
	gw_artillery = 1
	##gwtank = 1
	#basic_light_tank = 1  # PLACEHOLDER
	##basic_heavy_tank = 1  # PLACEHOLDER
	##basic_medium_tank = 1 # PLACEHOLDER
	#early_fighter = 1
	fighter1 = 1
	#early_bomber = 1
	strategic_bomber1 = 1
	naval_bomber1 = 1
	#mass_assault = 1
	fleet_in_being = 1
}

set_politics = {
	ruling_party = soc_conservatism
	last_election = "1933.3.5"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	leninism = 7
	marxism = 9
	rev_socialism = 2
	soc_democracy = 7
	soc_liberal = 0
	lib_conservatism = 20
	soc_conservatism = 48
	auth_democrat = 2
	pat_autocrat = 4
	fascism = 1
}

1918.11.11 = {

	recruit_character = AZR_mahammad_amin_rasulzade

	declare_war_on = {
		target = KAR
		type = annex_everything
	}
	declare_war_on = {
		target = ARM
		type = annex_everything
	}
	puppet = ARA
	ARA = {
		set_popularities = {
			leninism = 0
			marxism = 0
			rev_socialism = 0
			soc_democracy = 0
			soc_liberal = 3
			lib_conservatism = 7
			soc_conservatism = 54
			auth_democrat = 29
			pat_autocrat = 7
			fascism = 0
		}
		
		set_politics = {
			ruling_party = soc_conservatism
			last_election = "1936.1.1"
			election_frequency = 48
			elections_allowed = no
		}
	}
}