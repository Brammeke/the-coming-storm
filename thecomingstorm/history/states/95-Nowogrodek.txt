
state={
	id=95
	name="STATE_95"

	history={
		victory_points = {
			9304 1
		}
		owner = RUS
		buildings = {
			infrastructure = 4

		}
		add_core_of = RUS
		1918.11.11 = {
			owner = BLR
			add_core_of = BLR
			remove_core_of = RUS
		}

	}

	provinces={
		3309 6306 9236 9304 9359 11341
	}
	manpower=1598900
	buildings_max_level_factor=1.000
	state_category=rural
}
