
state={
	id=676
	name="STATE_676"
	resources={
		oil=8.000
	}

	history={
		owner = OTT
		buildings = {
			infrastructure = 3
		}
		victory_points = {
			10106 1
		}
		add_core_of = OTT
		1918.1.1 = {
			owner = ENG
			controller = ENG
		}
	}
	provinces={
		3916 5014 6826 8123 10811
	}
	manpower=1182741
	buildings_max_level_factor=1.000
	state_category=rural
}
