state={
	id=906
	name="STATE_906"
	state_category = rural
	manpower=473123


	history = {
		owner = AUS
		buildings = { infrastructure = 3 }
		victory_points = { 746 1 }
		add_core_of = AUS

		1918.11.11 = {
			owner = HUN
			controller = HUN
			add_core_of = HUN
			add_core_of = TRS
		}
	}

	provinces={ 746 9704 }
}
