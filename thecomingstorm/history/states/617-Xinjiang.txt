
state={
	id=617
	name="STATE_617"

	history={
		owner = QNG
		add_core_of = QNG
		1914.1.1 = {
			owner = CHI
			add_core_of = CHI
			remove_core_of = QNG

		}
		1918.1.1 = {
			owner = XIN
			add_core_of = XIN
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 2

		}
		victory_points = {
			4709 3
		}

	}

	provinces={
		1703 4682 4709 4770 4828 7702 7732 7792 10545 10614 12656 13728
	}
	manpower=880000
	buildings_max_level_factor=1.000
	state_category=rural
}
