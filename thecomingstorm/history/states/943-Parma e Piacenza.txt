state={
	id=943
	name="STATE_943"
	provinces={
		3773 6790 6973
	}
	history = {
		victory_points = {
			3773 1
		}
		buildings = {
			infrastructure = 5
			# 6973 = {
				# 	naval_base = 10
				# }
		}
		owner = ITA
		add_core_of = ITA
	}
	manpower=585261
	buildings_max_level_factor=1.000
	state_category = rural
}
