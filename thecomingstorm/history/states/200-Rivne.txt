
state={
	id=200
	name="STATE_200"
	state_category = rural
	manpower = 562000

	history={
		owner = RUS
		victory_points = {
			11405 1
		}
		buildings = {
			infrastructure = 3

		}
		add_core_of = RUS
		1918.11.11 = {
			owner = UKR
			controller = UKR
			remove_core_of = RUS
		}
	}

	provinces={
		6423 9433 11422 11437
	}
	buildings_max_level_factor=1.000
}
