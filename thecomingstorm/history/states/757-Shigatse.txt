
state={
	id=757
	name="STATE_757"

	history={
		owner = QNG
		buildings = {
			infrastructure = 1
			industrial_complex = 1
			arms_factory = 1

		}
		victory_points = {
			5033 5
		}
		add_core_of = QNG
		1914.1.1 = {
			owner = CHI
			add_core_of = CHI
			remove_core_of = QNG
		}
		1918.1.1 = {
			owner = TIB
			add_core_of = TIB
		}
	}

	provinces={
		4407 7526 8104 13699 13727
	}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category=rural
}
