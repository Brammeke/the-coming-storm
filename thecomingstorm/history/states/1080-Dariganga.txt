state={
	id=1080
	name="STATE_1080"
	provinces={
		4873 10575
	}
	history = {
		owner = QNG
		add_core_of = QNG
		1914.1.1 = {
			owner = MON
			add_core_of = MON
			remove_core_of = QNG
		}
	}
	manpower=52300
	state_category = rural
	buildings_max_level_factor=1.000
}
