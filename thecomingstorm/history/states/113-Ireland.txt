
state={
	id=113
	name="STATE_113" # Leinster
	manpower = 1155200
	resources={
		aluminium=0 # was: 2
		steel=2 # was: 3
	}

	history={
		owner = ENG
		victory_points = {
			11293 20
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			arms_factory = 1
			dockyard = 1
			11293 = {
				naval_base = 4
			}
		}
		add_core_of = ENG
	}

	provinces={
		285 303 369 3342 3359 7377 11293 11303
	}
	state_category = city
}
