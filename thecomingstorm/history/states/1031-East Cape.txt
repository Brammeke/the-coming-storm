state={
	id=1031
	name="STATE_1031"
	provinces={
		4696 12547 13224 13227 13231 13232 13235 13238 13239 13240 13243 13246 13249 13250 13253 13256 13258 13260 13265
	}
	history={
		victory_points = {
			4696 1
		}
		owner = SAF
		add_core_of = SAF
	}
	manpower=100000
	state_category=rural
	buildings_max_level_factor=1.000
}
