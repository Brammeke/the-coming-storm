state={
	id=888
	name="STATE_888"
	provinces={
		592 6099 6408 9221 9485
	}
	history = {
		owner = RUS
		add_core_of = RUS
		buildings = {
			infrastructure = 3
		}
		1918.11.11 = {
			owner = LIV
			controller = LIV
			remove_core_of = RUS
		}
	}
	manpower=100000
	state_category = rural
	buildings_max_level_factor=1.000
}
