state={
	id=905
	name="STATE_905"
	resources={
		steel=2
	}

	history = {
		owner = AUS
		add_core_of = AUS

		buildings = { infrastructure = 1 }
		victory_points = { 9670 1 }
		1918.11.11 = {
			owner = HUN
			controller = HUN
			add_core_of = HUN
			add_core_of = TRS
		}
	}

	manpower=374006
	state_category = pastoral
	provinces={ 711 3689 9668 9670 9685 }
}
