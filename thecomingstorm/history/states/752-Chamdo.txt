
state={
	id=752
	name="STATE_752"

	history={
		owner = QNG
		add_core_of = QNG
		1914.1.1 = {
			owner = CHI
			add_core_of = CHI
			remove_core_of = QNG
		}
		1918.1.1 = {
			owner = CHW
			add_core_of = CHW
		}
		victory_points = {
			4999 1
		}
		buildings = {
			infrastructure = 1

		}

	}

	provinces={
		1588 1918 1999 4999 7988 10841 12724 12837 13673 13708 13713
	}
	manpower=1898000
	buildings_max_level_factor=1.000
	state_category=rural
}
