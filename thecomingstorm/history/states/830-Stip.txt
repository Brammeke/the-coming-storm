state={
	id=830
	name="STATE_830"
	provinces={
		907 3833 6886
	}
	history = {
		owner = OTT
		add_core_of = OTT
		add_core_of = MAC
		victory_points = { 3833 1 }
		1914.1.1 = {
			owner = SER
			add_core_of = SER
			remove_core_of = OTT
		}
		1918.11.11 = {
			owner = SER
			add_core_of = SER
			remove_core_of = OTT
		}
	}
	manpower=100000
	buildings_max_level_factor=1.000
	state_category = rural
}
