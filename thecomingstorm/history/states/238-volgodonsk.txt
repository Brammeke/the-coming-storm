
state={
	id=238
	name="STATE_238"

	history={
		owner = RUS
		buildings = {
			infrastructure = 3

		}
		add_core_of = RUS
		1918.11.11 = {
			owner = SOV
			controller = SOV
			add_core_of = SOV
			DON = {
				set_province_controller = 3464
				set_province_controller = 6417
				set_province_controller = 11403
				set_province_controller = 426
				set_province_controller = 585
				set_province_controller = 3417
				set_province_controller = 3443
				set_province_controller = 6490
			}
			VOA = {
				set_province_controller = 3587
			}
		}
		victory_points = {
			775 1
		}
	}

	provinces={
		426 585 775 3417 3443 3464 3587 3777 3779 6417 6468 6490 6765 6796 9415 9581 11400 11403 11416
	}
	manpower=556677
	buildings_max_level_factor=1.000
	state_category=rural
}
