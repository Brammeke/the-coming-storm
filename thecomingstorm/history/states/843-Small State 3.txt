state={
	id=843
	name="STATE_843"
	provinces={
		13630
	}
	history = {
		owner = OTT
		add_core_of = OTT
		add_core_of = MAC
		1914.1.1 = {
			owner = BUL
			controller = BUL
			remove_core_of = OTT
			add_core_of = BUL
		}
		1918.11.11 = {
			owner = BUL
			controller = BUL
			remove_core_of = OTT
		}
		victory_points = {
			13630 1
		}
	}
	manpower=10350
	buildings_max_level_factor=1.000
	state_category = rural
}
