state={
	id=1130
	name="STATE_1130"
	provinces={
		1550 4476 7546 7590 12347
	}
	history={
		owner = CUB
		buildings = {
			infrastructure = 1
			industrial_complex = 1
			1550 = {
				naval_base = 1
			}
		}
		add_core_of = CUB
		victory_points = {
			7590 1
		}
	}
	manpower=819592
	buildings_max_level_factor=1.000
	state_category=city
}
