
state={
	id=602
	name="STATE_602"
	resources={
		tungsten=6.000
	}

	history={
		owner = QNG
		add_core_of = QNG
		1914.1.1 = {
			owner = CHI
			add_core_of = CHI
			remove_core_of = QNG

		}
		buildings = {
			infrastructure = 5
			arms_factory = 1

		}
		victory_points = {
			7097 5
		}

	}

	provinces={
		1640 7097 7665 9999 10517 11988 12310 13140 13705
	}
	manpower=22169700
	buildings_max_level_factor=1.000
	state_category=rural
}
