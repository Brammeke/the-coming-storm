
state={
	id=229
	name="STATE_229"
	resources={
		oil=100.000
	}

	history={
		owner = RUS
		victory_points = {
			7661 15
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			fuel_silo = 1

		}
		add_core_of = RUS
		1918.11.11 = {
			owner = AZR
			controller = AZR
			add_core_of = AZR
			remove_core_of = RUS
		}

	}

	provinces={
		1536 4458 4545 4614 7495 7661 12434
	}
	manpower=211900
	buildings_max_level_factor=1.000
	state_category=town
}
