state={
	id=874
	name="STATE_874"
	provinces={
		3532 9252 11260 11288
	}
	history = {
		owner = GER
		add_core_of = GER
		buildings = {
			infrastructure = 4
		}
		1918.11.11 = {
			owner = GER
			controller = GER
			add_claim_by = POL
		}
	}
	manpower=267954
	buildings_max_level_factor=1.000
	state_category = city
}
