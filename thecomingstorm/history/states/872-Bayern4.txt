state={
	id=872
	name="STATE_872"
	provinces={
		708 6540 9666 11638
	}
	history = {
		victory_points = {
			708 3
		}
		owner = GER
		add_core_of = GER
		buildings = {
			infrastructure = 5
		}
		1918.11.11 = {
			owner = BAV
			add_core_of = BAV
		}
	}
	manpower=713681
	buildings_max_level_factor=1.000
	state_category = large_city
}
