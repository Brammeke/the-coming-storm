state={
	id=84
	name="STATE_84"
	resources={
		steel=12
		tungsten=5
	}

	history={
		owner = AUS
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 1

		}
		victory_points = { 6679 2 }
		victory_points = { 9619 1 }
		add_core_of = AUS

		1918.11.11 = {
			owner = HUN
			controller = HUN
			add_core_of = HUN
			add_core_of = TRS
		}

	}

	provinces={ 676 3665 6679 9619 }
	manpower=450967
	buildings_max_level_factor=1.000
	state_category=rural
}
