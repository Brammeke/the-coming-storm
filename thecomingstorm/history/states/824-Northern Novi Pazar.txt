state={
	id=824
	name="STATE_824"
	provinces={
		937 3939
	}
	history = {
		owner = AUS
		1910.1.1 = {
			owner = OTT
			add_core_of = OTT
			remove_core_of = AUS
		}
		1914.1.1 = {
			owner = SER
			add_core_of = SER
			remove_core_of = OTT
		}
		1918.11.11 = {
			owner = SER
			add_core_of = SER
		}
	}
	manpower=100000
	buildings_max_level_factor=1.000
	state_category = rural
}
