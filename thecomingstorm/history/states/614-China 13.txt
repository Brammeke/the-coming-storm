
state={
	id=614
	name="STATE_614"

	history={
		owner = QNG
		add_core_of = QNG
		1914.1.1 = {
			owner = CHI
			add_core_of = CHI
			remove_core_of = QNG
		}
		1918.1.1 = {
			owner = ZHI
			add_core_of = ZHI
		}
		buildings = {
			infrastructure = 4
		}
	}

	provinces={
		1134 1169 1188 4032 4093 4114 4137 4140 4190 4208 6828 7000 7092 7109 9969 10022 10053 10368 10379 11933 11944 11980 11996 12026 12062 13640 13650 13748
	}
	manpower=20937000
	buildings_max_level_factor=1.000
	state_category=rural
}
