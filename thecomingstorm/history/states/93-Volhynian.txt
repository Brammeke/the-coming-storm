
state={
	id=93
	name="STATE_93"

	history={
		victory_points = {
			6557 1
		}
		owner = RUS
		buildings = {
			infrastructure = 5

		}
		add_core_of = RUS
		1918.11.11 = {
			owner = UKR
			controller = UKR
			remove_core_of = RUS
		}

	}

	provinces={
		474 513 572 6435 6520 6557 11503 11543
	}
	manpower=1985600
	buildings_max_level_factor=1.000
	state_category=rural
}
