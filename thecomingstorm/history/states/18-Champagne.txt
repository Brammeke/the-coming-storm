
state={
	id=18
	name="STATE_18"

	history={
		owner = FRA
		victory_points = {
			9490 5
		}
		victory_points = {
			13011 5
		}
		victory_points = {
			3560 1
		}
		victory_points = {
			9472 1
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			9472 = {
				bunker = 4
			}
			13011 = {
				trench = 6
			}
			3546 = {
				trench = 6
			}
			3560 = {
				trench = 6
			}
		}
		add_core_of = FRA

	}

	provinces={
		551 3506 3533 3546 3560 6531 6545 6949 9472 9490 9505 10785 11488 11518 11732 13011 13583 13617
	}
	manpower=1370800
	buildings_max_level_factor=1.000
	state_category=city
}
