state={
	id=1111
	name="STATE_1111"
	provinces={
		5011
	}
	history={
		owner = QNG
		add_core_of = QNG
		1914.1.1 = {
			owner = CHI
			add_core_of = CHI
			remove_core_of = QNG
		}
		1918.1.1 = {
			owner = XSM
			add_core_of = XSM
		}
	}
	manpower=100000
	buildings_max_level_factor=1.000
	state_category = wasteland
	impassable = yes
}
