state={
	id=932
	name="STATE_932"
	provinces={
		966 3844 6930
	}
	history = {
		owner = OTT
		add_core_of = OTT
		1914.1.1 = {
			owner = GRE
			controller = GRE
			remove_core_of = OTT
		}
	}
	manpower=100000
	buildings_max_level_factor=1.000
	state_category = rural
}
