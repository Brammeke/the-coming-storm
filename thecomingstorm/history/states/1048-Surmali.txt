state={
	id=1048
	name="STATE_1048"
	provinces={
		4583
	}
	history={
		owner = RUS
		add_core_of = RUS
		buildings = {
			infrastructure = 2

		}
		1918.11.11 = {
			owner = OTT
			controller = OTT
			add_core_of = OTT
			remove_core_of = RUS
		}
	}
	manpower=100000
	buildings_max_level_factor=1.000
	state_category=rural
}
