
state={
	id=64
	name="STATE_64"

	history={
		owner = GER
		victory_points = {
			6521 50
		}
		buildings = {
			infrastructure = 3
			arms_factory = 3
			industrial_complex = 2
		}
		add_core_of = GER

	}

	provinces={
		375 3207 3312 3367 3499 6521 9456 11219 11444 11505
	}
	manpower=3818152
	buildings_max_level_factor=1.000
	state_category = metropolis
	local_supplies=12.0
}
