
state={
	id=412
	name="STATE_412"
	resources={
		oil=10.000
	}

	history={
		owner = PER
		victory_points = {
			8117 10
		}
		victory_points = {
			12863 2
		}
		buildings = {
			infrastructure = 2
		}
		add_core_of = PER
	}

	provinces={
		4966 8031 8117 10797 12780
	}
	manpower=768330
	buildings_max_level_factor=1.000
	state_category=rural
}
