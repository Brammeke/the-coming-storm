state={
	id=823
	name="STATE_823"
	provinces={
		797 3609 3956 9599
	}
	history = {
		owner = SER
		add_core_of = SER
		buildings = {
			infrastructure = 2
			industrial_complex = 1
		}
		victory_points = {
			3609 3
		}
	}
	manpower=345283
	buildings_max_level_factor=1.000
	state_category = town
}
