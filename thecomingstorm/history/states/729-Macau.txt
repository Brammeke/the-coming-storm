state={
	id=729
	name="STATE_729"
	manpower = 74866

	state_category = enclave

	history={
		owner = POR
		add_core_of = QNG
		1914.1.1 = {
			#owner = CHI
			add_core_of = CHI
			remove_core_of = QNG
		}
		victory_points = {
			4189 1
		}
		buildings = {
			infrastructure = 4
			4189 = {
				naval_base = 1
			}
		}
	}

	provinces={
		4189
	}
}
