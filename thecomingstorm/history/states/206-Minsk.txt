
state={
	id=206
	name="STATE_206"
	state_category = large_town

	history={
		owner = RUS
		victory_points = {
			11370 20
		}
		victory_points = {
			9289 5
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 2
		}
		add_core_of = RUS
		1918.11.11 = {
			owner = BLR
			add_core_of = BLR
			remove_core_of = RUS
		}

	}

	provinces={
		216 294 304 331 360 386 3267 3350 3759 6220 6292 6371 11216 11313 11322 11370
	}
	manpower=1163163
	buildings_max_level_factor=1.000
}
