CET_politics_cat = {
    CET_declare_enosis_ottoman_political_crisis = {
        icon = generic_nationalism
        visible = {
            original_tag = CET
            has_global_flag = OTT_post_yildiz_crisis
            NOT = {
                has_country_flag = CET_unilateral_union_with_greece_flag
            }
        }
        fire_only_once = yes
        fixed_random_seed = no
        complete_effect = {
            set_country_flag = CET_unilateral_union_with_greece_flag
            custom_effect_tooltip = CET_enosis_ottoman_political_crisis_tt
            hidden_effect = {
                set_cosmetic_tag = CET_enosis
                GRE = {
                    news_event = { id = cet_news.5 } 
                }
                OTT = {
                    news_event = { id = cet_news.5 } 
                }
            }
        }
    }
    CET_declare_enosis_ottoman_civil_war = {
        icon = generic_nationalism
        visible = {
            original_tag = CET
            has_global_flag = OTT_civil_war_war
            NOT = {
                has_country_flag = CET_unilateral_union_with_greece_flag
            }
        }
        fire_only_once = yes
        fixed_random_seed = no
        complete_effect = {
            GRE = {
                annex_country = {
                    target = CET
                    transfer_troops = yes
                }
            }
            hidden_effect = {
                news_event = { id = cet_news.7 } 
            }
        }
    }
}