TCS_BAV_integration_with_germany_category = {
	TCS_BAV_integration_with_germany = {
		icon = GFX_decision_generic_decision
		activation = {
			has_country_flag = BAV_soviets_defeated
			has_completed_focus = BAV_bamberg_constitution
		}
		available = {
			always = no
		}

		days_mission_timeout = 100
		fire_only_once = yes
		is_good = yes

		timeout_effect = {
			custom_effect_tooltip = TCS_BAV_integration_with_germany_tt
			hidden_effect = {
				BAV = {
					every_owned_state = {
						add_core_of = GER
					}
				}
				GER = {
					change_tag_from = BAV
				}
				GER = {
					annex_country = {
						target = BAV
						transfer_troops = yes
					}
				}
				GER = {
					every_owned_state = {
						remove_core_of = BAV
						remove_core_of = BSR
					}
				}
			}
		}
	}
}