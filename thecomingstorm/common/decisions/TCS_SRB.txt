SRB_decisions = {
	SRB_transfer_east_belarus_to_soviet_russia = {

		allowed = {
			original_tag = SRB
		}

		available = {
			has_completed_focus = SRB_litbel
			controls_state = 242
			controls_state = 990
			controls_state = 207
			controls_state = 204
			controls_state = 241
		}

		ai_will_do = {
			factor = 200
		}

		visible = {
			has_completed_focus = SRB_litbel
			controls_state = 242
			controls_state = 990
			controls_state = 207
			controls_state = 204
			controls_state = 241
		}
		fire_only_once = yes
		complete_effect = {
			SOV = {
				transfer_state = 242
				transfer_state = 990
				transfer_state = 207
				transfer_state = 204
				transfer_state = 241
			} 
		}
	}
}