TCS_CHI_post_revolution_category = {
	TCS_CHI_the_tibetan_issue = {
		icon = XINHAI_wuchang

		allowed = {
			always = no
		}
		available = {
			always = no
		}

		days_mission_timeout = 20
		fire_only_once = yes
		is_good = no

		# highlight_states = {
		# 	highlight_state_targets = {
		# 		state = 620 # Hupeh
		# 	}
		# }

		#war_with_on_timeout = CHI

		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: TCS_XINHAI_the_abdication timeout"
			#custom_effect_tooltip = TCS_XINHAI_wuchang_uprising_tt
			hidden_effect = {
				country_event = {
					id = TCS_CHI_political.1 #The Tibetan Issue
				}
			}
		}
	}
}