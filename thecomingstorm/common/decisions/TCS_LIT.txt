LIT_seize_palanga_category = {
	LIT_seize_palanga_de = {
        icon = infiltrate_state
        available = {
			LSR = { controls_state = 883 }
        }
		custom_cost_trigger = {
			command_power > 14.99
		}
		custom_cost_text = LAT_command_power_more_than_15
        fire_only_once = yes
        visible = {
			NOT = { is_in_faction_with = LSR }
			NOT = { has_war_with = LSR }
			LSR = { controls_state = 883 }
			LSR = { owns_state = 883 }
			owns_state = 189
			controls_province = 6314
        }
        fixed_random_seed = no
        days_remove = 5

		complete_effect = {
			hidden_effect = {
				add_command_power = -15	
			}
		}

        remove_effect = {
			transfer_state = 883
			hidden_effect = {
				country_event = {
					id = latvianssr.17
				}			
				LSR = { remove_state_core = 883 }
				LSR = {
					country_event = {
						id = latvianssr.18
					}
				}
			}
        }
		ai_will_do = {
			factor = 100
		}
    }
}