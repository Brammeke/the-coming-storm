OTT_opda_cat = {
     OTT_take_out_loan = {
        cost = 50
        modifier = {
            consumer_goods_factor = -0.1
            production_speed_buildings_factor = 0.2
        }
        available = {
            check_variable = {
                var = OTT.opda_influence
                value = 0.7
                compare = less_than_or_equals
            }
        }
        days_remove = 100
        days_re_enable = 30
        remove_effect = {
            custom_effect_tooltip = OTT_tt_5
            hidden_effect = {
                add_to_variable = { OTT.opda_influence = 0.05 } 
            }
        }
    }
    OTT_pay_off_loan = {
        cost = 50
        modifier = {
            consumer_goods_factor = 0.1
            production_speed_buildings_factor = -0.2
        }
        available = {
            check_variable = {
                var = OTT.opda_influence
                value = 0
                compare = greater_than
            }
        }
        days_remove = 100
        days_re_enable = 30
        remove_effect = {
            custom_effect_tooltip = OTT_tt_6
            hidden_effect = {
                add_to_variable = { OTT.opda_influence = -0.03 } 
            }
        }
    }
    OTT_abolish_opda = {
        cost = 120
        visible = {
            check_variable = {
                var = OTT.opda_influence
                value = 0
                compare = less_than_or_equals
            }
        }
        fire_only_once = yes
        fixed_random_seed = no
        complete_effect = {
            custom_effect_tooltip = OTT_tt_7
            country_event= {
                days = 1 
                id = ott.3
            }
        }
    }
}
OTT_operations_cat = {
    OTT_YEM_rebellion_war_mission = {
        icon = generic_operation
        visible = {
            date < 1909.12.1
        }
        allowed = {
            tag = OTT
            #has_war_with = YEM
        }
        available = {
            NOT= {
                country_exists = YEM
            }
        }
        days_mission_timeout = 150
        fire_only_once = yes
        activation = {
            tag = OTT
            has_war_with = YEM
        }
        is_good = no
        complete_effect = {
            custom_effect_tooltip = OTT_annex_yemen_tt
        }
        timeout_effect = {   
            custom_effect_tooltip = OTT_yemen_vassal_bad_tt
            hidden_effect = {
                YEM = {
                    remove_ideas = YEM_anti_ottoman_revolt
                }
                white_peace = YEM
                if = {
                    limit = {
                        OR = {
                            has_dlc = "Together for Victory"
                            has_dlc = "Man the Guns"
                        }
                    }
                    OTT = {
                        set_autonomy = {
                            target = YEM
                            autonomous_state = autonomy_vassal
                            freedom_level = 0.4
                        }
                    }
                    else = {
                        puppet = YEM
                    }
                }
            }
        }
        ai_will_do = {
            factor = 1
        }
    }
}
OTT_extending_railways_cat = {
    OTT_Hejaz_to_Mecca_1 = {
        cost = 15
        available = {
            owns_state = 958
        }
        fire_only_once = yes
        icon = GFX_decision_hol_exchange_intelligence_data
        visible = {
            has_country_flag = Hejaz_Line_Built
        }
        fixed_random_seed = no
        complete_effect = {
            country_event = ott.29
        }
    }
    OTT_Hejaz_to_Mecca_2 = {
        cost = 100
        available = {
            always = yes
        }
        fire_only_once = yes
        icon = GFX_decision_generic_construction
        days_remove = 70
        visible = {
            has_country_flag = Hejaz_Line_ready_Cairo
        }
        fixed_random_seed = no
        complete_effect = {
            958 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
        }    
    }
    OTT_link_Hejaz_and_Cairo_1 = {
        cost = 15
        available = {
            always = yes
        }
        fire_only_once = yes
        icon = GFX_decision_hol_exchange_intelligence_data
        visible = {
            has_country_flag = Hejaz_Line_Built
        }
        fixed_random_seed = no
        complete_effect = {
            ENG = {
                country_event = ott.26
            }
        }
    }
    OTT_link_Hejaz_and_Cairo_2 = {
        cost = 100
        available = {
            owns_state = 961
        }
        icon = GFX_decision_generic_construction
        fire_only_once = yes
        days_remove = 140
        visible = {
            has_country_flag = Hejaz_Line_ready_Cairo
        }
        fixed_random_seed = no
        complete_effect = {
            452 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
            447 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
        }    
    }
    OTT_Anatolia_to_Antalya = {
        cost = 100
        available = {
            owns_state = 342
        }
        icon = GFX_decision_generic_construction
        fire_only_once = yes
        days_remove = 70
        visible = {
            has_country_flag = anatolia_line_built
        }
        fixed_random_seed = no
        complete_effect = {
            342 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
        }
    }
    OTT_Rumelia_to_Tirana = {
        cost = 100
        available = {
            owns_state = 44
        }
        icon = GFX_decision_generic_construction
        fire_only_once = yes
        days_remove = 70
        visible = {
            always = yes
        }
        fixed_random_seed = no
        complete_effect = {
            44 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
        }
    }
    OTT_Rumelia_to_Sarajevo_1 = {
        cost = 15
        available = {
            always = yes
        }
        fire_only_once = yes
        icon = GFX_decision_hol_exchange_intelligence_data
        visible = {
            always = yes
        }
        fixed_random_seed = no
        complete_effect = {
            AUS = {
                country_event = ott.36
            }
        }
    }
    OTT_Rumelia_to_Sarajevo_2 = {
        cost = 100
        available = {
            owns_state = 827
        }
        fire_only_once = yes
        days_remove = 210
        visible = {
            has_country_flag = Rumelia_Line_Ready_Sarajevo
        }
        icon = GFX_decision_generic_construction
        fixed_random_seed = no
        remove_effect = {
            827 = {
                set_building_level = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
            824 = {
                set_building_level = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
            104 = {
                set_building_level = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
        }
    }
    OTT_Hejaz_to_Aden_1 = {
        cost = 15
        available = {
            always = yes
        }
        icon = GFX_decision_hol_exchange_intelligence_data
        fire_only_once = yes
        visible = {
            has_country_flag = Hejaz_Line_Finished_Mecca
            has_country_flag = no_opda
            not = {
                owns_state = 952
            }
        }
        fixed_random_seed = no
        complete_effect = {
            ENG = {
                country_event = OTT.33
            }
        }
    }
    OTT_Hejaz_to_Aden_2 = {
        cost = 100
        available = {
            owns_state = 954
            OR = {
                owns_state = 953
                owns_state = 293
                YEM = {
                    is_puppet_of = OTT
                }
            }
        }
        fire_only_once = yes
        icon = GFX_decision_generic_construction
        days_remove = 280
        visible = {
            or = {
                has_country_flag = Hejaz_Line_ready_Aden
                owns_state = 952
                YEM = {
                    owns_state = 952
                }
            }
        }
        fixed_random_seed = no
        complete_effect = {
            954 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
            953 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
            293 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
            952 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
        }
    }
    OTT_Tobruk_Tripoli_Line = {
        cost = 100
        available = {
            owns_state = 448
            owns_state = 449
            owns_state = 450
            owns_state = 451
        }
        fire_only_once = yes
        days_remove = 280
        icon = GFX_decision_generic_construction
        visible = {
            has_country_flag = no_opda
        }
        fixed_random_seed = no
        complete_effect = {
            set_country_flag = Tripoli_line_built
            448 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
            449 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
            450 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
            451 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
        }
    }
    OTT_Tripoli_to_Alexandria = {
        cost = 100
        available = {
            always = yes
        }
        fire_only_once = yes
        days_remove = 140
        icon = GFX_decision_generic_construction
        visible = {
            has_country_flag = Tripoli_line_built
            EGY = {
                is_puppet_of = OTT
            }
        }
        fixed_random_seed = no
        complete_effect = {
            452 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
            447 = {
                add_building_construction = {
                    type = infrastructure
                    level = 1
                    instant_build = yes
                }
            }
        }
    } 
}
OTT_parliament_cat = {
    OTT_senate_up = {
        cost = 0
        fire_only_once = yes
        fixed_random_seed = no
        days_re_enable = 1
        complete_effect = {
        	add_to_variable = { var = heyet-i_ayan_support value = 7 } 
        }
    }
    OTT_senate_down = {
        cost = 0
        fire_only_once = yes
        fixed_random_seed = no
        days_re_enable = 1
        complete_effect = {
        	add_to_variable = { var = heyet-i_ayan_support value = -7 } 
        }
    }
    OTT_deputies_up = {
        cost = 0
        fire_only_once = yes
        fixed_random_seed = no
        days_re_enable = 1
        complete_effect = {
        	add_to_variable = { var = heyet-i_mebusan_support value = 11 }
        }
    }
    OTT_deputies_down = {
        cost = 0
        fire_only_once = yes
        fixed_random_seed = no
        days_re_enable = 1
        complete_effect = {
        	add_to_variable = { var = heyet-i_mebusan_support value = -11 }
        }
    }
    OTT_debug_bulgaria = {
        cost = 0
        fire_only_once = yes
        fixed_random_seed = no
        days_re_enable = 1
        complete_effect = {
            end_puppet = BUL
        }
    }
}

# OTT_italo_turkish_war = {
#     # OTT_fedai_officers = {
#     #     cost = 100
#     #     fire_only_once = yes
#     #     fixed_random_seed = no
#     #     complete_effect = {
#     #     }
#     # }
#     # OTT_fund_senussi = {
#     #     cost = 100
#     #     fire_only_once = yes
#     #     fixed_random_seed = no
#     #     complete_effect = {
#     #     }
#     # }
# }

OTT_first_balkan_war = {
    #OTT_OHC_detente = {
    #    cost = 100
    #    fire_only_once = yes
    #    fixed_random_seed = no 
    #    complete_effect = {
    #    }
    #}
    #OTT_OHC_Salonika_branch = {
    #    cost = 100
    #    fire_only_once = yes
    #    fixed_random_seed = no 
    #    complete_effect = {
    #    }
    #}
    #OTT_loyalists_thrace = {
    #    cost = 100
    #    fire_only_once = yes
    #    fixed_random_seed = no 
    #    complete_effect = {
    #    }
    #}
    #OTT_loyalists_near_uskub = {
    #    cost = 100
    #    fire_only_once = yes
    #    fixed_random_seed = no 
    #    complete_effect = {
    #    }
    #}
	OTT_balkan_war_peace_deal = {
        icon = generic_operation

		allowed = { always = no }
		available = { always = no }

		days_mission_timeout = 12
		fire_only_once = yes
		is_good = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision OTT_balkan_war_peace_deal"
		}
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout OTT_balkan_war_peace_deal"
			clr_global_flag = OTT_first_balkan_war
            set_global_flag = OTT_first_balkan_war_lost
            ## Ottomans Surrender Event ##
            country_event = {
                id = balkan_war.2
            }
            ### Crete and Greece Unify ###
            CET = {
                country_event = {
                    id = cet.32 
                    hours = 1
                }
            }
            GRE = {
                country_event = {
                    id = cet.32
                    hours = 1
                }
            }
		}
    }
}