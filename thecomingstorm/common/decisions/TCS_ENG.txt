ENG_Anglo_German_Arms_Race = {

	ENG_lay_down_dreadnought = {
	
		icon = generic_naval
		
		allowed = {
			tag = ENG
		}
		
		available = {
			date > 1905.10.02

		}
		
		cost = 50
		
		days_remove = 426
		
		#remove_effect = { 
			#create_equipment_variant = { 
				#name = "Dreadnought Class" 
				#type = pre_dreadnaught_2

			#}
			
			#load_oob = "ENG_1905_Dreadnought"
			#hidden_effect = {
			#	news_event = {
            #		id = tcs_flavor.15
        	#	}
			#}


		#}
		
		ai_will_do = {
			factor = 100 
		}
	
	
	}
	
	ENG_lay_down_bellerophon = {
	
		icon = generic_naval
		
		allowed = {
			tag = ENG
		}
		
		available = {
			date > 1906.12.03

		}
		
		cost = 50
		
		days_remove = 817
		
		#remove_effect = { 
			#create_equipment_variant = { 
				#name = "Bellerophon class" 
				#type = pre_dreadnaught_2

			#}
			
			#load_oob = "ENG_1905_Bellerophon"

		#}
		
		ai_will_do = {
			factor = 100 
		}
	
	
	}






}
ENG_political_cat = {
 	ENG_taba_ultimatum = {
		allowed = {
	        always = no
	    }
	    available = {
	        has_political_power > 0
	    }
	    cost = 0
		selectable_mission = yes
		days_mission_timeout = 100
		is_good = no
		fire_only_once = yes
		ai_will_do = {
			factor = 50
		}
		complete_effect = {
			OTT = {
				#country_event = { id = ott.20 }
			}
		}
		timeout_effect = { 
			#country_event = { id = ott.21 } 
			EGY = {
				#country_event = { id = ott.21 }
			}
			OTT = {
				#country_event = { id = ott.22 }
			}
			961 = {
		        add_building_construction = {
		            type = bunker
		            level = 3
		            instant_build = yes
		            province = 1015
		        }
		    }
		}
	}
}