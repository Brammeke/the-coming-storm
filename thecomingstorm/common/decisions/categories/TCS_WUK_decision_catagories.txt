WUK_Polish_Ukrainian_War_cat = {
    icon = GFX_decision_WUK_POL_UKR_War_Cat
    picture = GFX_decision_WUK_POL_UKR_War_picture
    allowed = {
		original_tag = WUK
		has_war_with = POL
	}
	visible = {
        is_puppet = no
		has_capitulated = no
	}
}