ROM_The_Prezan_Plan = {
    icon = GFX_ROM_decision_decision_prezan_plan
    picture = GFX_ROM_prezan_plan_cat
    allowed = { original_tag = ROM }

    visible = { 
    }
}

ROM_Military_Reform = {
    icon = GFX_decision_category_army_reform
    #picture = 
    allowed = { original_tag = ROM }

    visible = {  }
}

ROM_The_Peasant_Revolt = {
    icon = GFX_decision_category_army_reform
    #picture = 
    allowed = { original_tag = ROM }

    visible = {
        has_war = yes
        has_defensive_war_with = RPR
        any_state = { is_owned_by = RPR }
    }
}

ROM_Independence_of_Moldova = {
    icon = GFX_decision_category_army_reform
    #picture = 
    allowed = { 
    	original_tag = ROM
    }

    visible = {  }
}