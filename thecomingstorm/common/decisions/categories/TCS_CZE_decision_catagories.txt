CZE_independence_struggle_cat = {
	priority = 9
    icon = border_conflicts
    picture = GFX_decision_CZE_struggle_for_independence
    allowed = {
		original_tag = CZE
	}
	available = {
		original_tag = CZE
	}
	visible_when_empty = yes
	visible = {
        is_puppet = no
		has_capitulated = no
	}
}
