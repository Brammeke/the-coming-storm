LSR_latvian_war_independence = {
    icon = GFX_decision_category_military_operation
    allowed = {
	original_tag = LSR
	}
    visible = {
		NOT = { has_country_flag = latvian_struggle_over }
    }
}
LSR_consolidating_power = {
    icon = GFX_decision_category_sov_great_patriotic_war
    allowed = {
	original_tag = LSR
	}
    visible = {
		has_completed_focus = LSR_Red_Victory_Latvia
    }
}
