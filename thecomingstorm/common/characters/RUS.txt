characters={
	RUS_nicholas_ii={
		name="Nicholas II"
		portraits={
			civilian={
				large="gfx/leaders/RUS/Portrait_RUS_Nicholas_II.tga"
			}
		}
		country_leader={
			ideology = lib_conservatism_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			ideology = soc_conservatism_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			ideology = auth_democrat_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			ideology = pat_autocrat_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			ideology = fascism_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
	RUS_vladimir_lenin={
		name="Vladimir Lenin"
		portraits={
			civilian={
				large="gfx/leaders/SOV/Portrait_SOV_Vladimir_Lenin.tga"
			}
		}
		country_leader={
			desc="POLITICS_VLADIMIR_LENIN_DESC"
			ideology=leninism_bolshevism_subtype
			expire="1953.3.1.1"
			id=-1
		}
	}
	RUS_alexander_kerensky={
		name="Alexander Kerensky"
		portraits={
			civilian={
				large="gfx/leaders/RUS/Portrait_RUS_Alexander_Kerensky.tga"
			}
		}
		country_leader={
			desc=""
			ideology=soc_democracy_subtype
			expire="1953.3.1.1"
			id=-1
		}
	}
	RUS_viktor_chernov={
		name="Viktor Chernov"
		portraits={
			civilian={
				#large="gfx/leaders/OTT/Portrait_Hasan_Tahsin_Pasha.tga"
			}
		}
		country_leader={
			ideology=rev_socialism_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			ideology=soc_liberal_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader = {
			ideology = soc_democracy_subtype
		}
	}
	RUS_julius_martov={
		name="Julius Martov"
		portraits={
			civilian={
				#large="gfx/leaders/OTT/Portrait_Hasan_Tahsin_Pasha.tga"
			}
		}
		country_leader={
			ideology = marxism_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
	RUS_alexander_dubrovin={
		name="Alexander Dubrovin"
		portraits={
			civilian={
				#large="gfx/leaders/OTT/Portrait_Hasan_Tahsin_Pasha.tga"
			}
		}
		country_leader={
			ideology=fascism_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
	RUS_nicholas_nikolaevich={
		name="Nicholas Nikolaevich"
		portraits={
			army={
				#small=""
			}
			army={
				large="gfx/leaders/RUS/Nicholas_Nikolaevich.tga"
			}
		}
		field_marshal={
			traits={ old_guard }
			skill=2
			attack_skill=2
			defense_skill=3
			planning_skill=2
			logistics_skill=2
			legacy_id=191700
		}
	}
	RUS_dmitry_milyutin={
		name="Dmitry Milyutin"
		portraits={
			army={
				#small=""
			}
			army={
				large="gfx/leaders/RUS/Dmitry_Milyutin.tga"
			}
		}
		field_marshal={
			traits={ old_guard career_officer }
			skill=4
			attack_skill=3
			defense_skill=2
			planning_skill=3
			logistics_skill=4
			legacy_id=191701
		}
	}
	RUS_mikhail_alekseyev={
		name="Mikhail Alekseyev"
		portraits={
			army={
				#small=""
			}
			army={
				large="gfx/leaders/RUS/Mikhail_Alekseyev.tga"
			}
		}
		field_marshal={
			skill=2
			attack_skill=2
			defense_skill=2
			planning_skill=2
			logistics_skill=2
			legacy_id=191702
		}
	}
	RUS_yakov_zhilinsky={
		name="Yakov Zhilinsky"
		portraits={
			army={
				#small=""
			}
			army={
				large="gfx/leaders/RUS/Yakov_Zhilinsky.tga"
			}
		}
		field_marshal={
			traits={ old_guard trait_reckless }
			skill=2
			attack_skill=3
			defense_skill=2
			planning_skill=1
			logistics_skill=2
			legacy_id=191703
		}
	}
	RUS_alexander_samsonov={
		name="Alexander Samsonov"
		portraits={
			army={
				#small=""
			}
			army={
				large="gfx/leaders/RUS/Alexander_Samsonov.tga"
			}
		}
		field_marshal={
			skill=2
			attack_skill=2
			defense_skill=2
			planning_skill=2
			logistics_skill=2
			legacy_id=191704
		}
	}
	RUS_aleksandr_krymov={
		name="Aleksandr Krymov"
		portraits={
			army={
				#small=" _small"
			}
			army={
				#large=" "
			}
		}
		corps_commander={
			skill=2
			attack_skill=2
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=191705
		}
	}
	RUS_aleksey_kuropatkin={
		name="Aleksey Kuropatkin"
		portraits={
			army={
				#small=""
			}
			army={
				large="gfx/leaders/RUS/Alexsey_Kuro_TEMP.tga"
			}
		}
		corps_commander={
			traits={ trait_cautious }
			skill=2
			attack_skill=1
			defense_skill=3
			planning_skill=2
			logistics_skill=2
			legacy_id=191706
		}
	}
	RUS_alexei_evert={
		name="Alexei Evert"
		portraits={
			army={
				#small=""
			}
			army={
				large="gfx/leaders/RUS/Aleksei_Evert.tga"
			}
		}
		corps_commander={
			traits={ career_officer organizer }
			skill=2
			attack_skill=1
			defense_skill=2
			planning_skill=1
			logistics_skill=3
			legacy_id=191707
		}
	}
	RUS_nikolai_ivanov={
		name="Nikolai Ivanov"
		portraits={
			army={
				#small=""
			}
			army={
				large="gfx/leaders/RUS/Nikolai_Ivanov.tga"
			}
		}
		corps_commander={
			traits={ trait_cautious }
			skill=2
			attack_skill=2
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=191708
		}
	}
	RUS_fyodor_dubasov={
		name="Fyodor Dubasov"
		portraits={
			army={
				#small=""
			}
			army={
				#large="gfx/leaders/RUS/ "
			}
		}
		navy_leader={
			skill=2
			attack_skill=1
			defense_skill=1
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=-1
		}
	}
	RUS_ivan_konstantinovich_grigorovich={
		name="Ivan Konstantinovich Grigorovich"
		portraits={
			army={
				#small=""
			}
			army={
				#large="gfx/leaders/RUS/ "
			}
		}
		navy_leader={
			skill=2
			attack_skill=1
			defense_skill=1
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=-1
		}
	}
	RUS_mikhail_kedrov={
		name="Mikhail Kedrov"
		portraits={
			army={
				#small=""
			}
			army={
				#large="gfx/leaders/RUS/ "
			}
		}
		navy_leader={
			skill=2
			attack_skill=1
			defense_skill=1
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=-1
		}
	}
	RUS_alexander_kolchak={
		name="Alexander Kolchak"
		portraits={
			army={
				#small=""
			}
			army={
				#large="gfx/leaders/RUS/ "
			}
		}
		navy_leader={
			skill=2
			attack_skill=1
			defense_skill=1
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=-1
		}
	}
	RUS_nikolai_kolomeitsev={
		name="Nikolai Kolomeitsev"
		portraits={
			army={
				#small=""
			}
			army={
				#large="gfx/leaders/RUS/ "
			}
		}
		navy_leader={
			skill=2
			attack_skill=1
			defense_skill=1
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=-1
		}
	}
	RUS_zinovy_rozhestvensky={
		name="Zinovy Rozhestvensky"
		portraits={
			army={
				#small=""
			}
			army={
				#large="gfx/leaders/RUS/ "
			}
		}
		navy_leader={
			skill=2
			attack_skill=1
			defense_skill=1
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=-1
		}
	}
}
