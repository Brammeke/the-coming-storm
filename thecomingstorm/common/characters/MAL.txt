characters={
	MAL_onn_jaafar={
		name="Onn Jaafar"
		portraits={
			civilian={
				large="gfx/leaders/Asia/Portrait_Asia_Generic_democracy.dds"
			}
		}
		country_leader={
			ideology=soc_conservatism_subtype
			expire="1953.3.1.1"
			id=-1
		}
	}
	MAL_shenton_thomas={
		name="Shenton Thomas"
		portraits={
			civilian={
				large="GFX_portrait_malaya_shenton_thomas"
			}
		}
		country_leader={
			ideology=soc_conservatism_subtype
			expire="1953.3.1.1"
			id=-1
		}
	}
	MAL_lai_teck={
		name="Lai Teck"
		portraits={
			civilian={
				large="gfx/leaders/Asia/Portrait_Asia_Generic_1.dds"
			}
		}
		country_leader={
			ideology=marxism_subtype
			expire="1953.3.1.1"
			id=-1
		}
	}
	MAL_ibrahim_hj_yaacob={
		name="Ibrahim Hj Yaacob"
		portraits={
			civilian={
				large="gfx/leaders/Asia/Portrait_Asia_Generic_fascism.dds"
			}
		}
		country_leader={
			ideology=auth_democrat_subtype
			expire="1953.3.1.1"
			id=-1
		}
	}
	MAL_jake_montgomery={
		name="Jake Montgomery"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=leninism_bolshevism_subtype
			expire="1923.1.1.12"
			id=-1
		}
	}
	MAL_thomas_dundas={
		name="Thomas Dundas"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=rev_socialism_syndicalism_subtype
			expire="1917.1.1.12"
			id=-1
		}
	}
	MAL_alfred_fisher={
		name="Alfred Fisher"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=soc_democracy_subtype
			traits={  }
			expire="1910.1.1.12"
			id=-1
		}
	}
	MAL_david_fisher={
		name="David Fisher"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=soc_liberal_subtype
			traits={  }
			expire="1920.1.1.12"
			id=-1
		}
	}
	MAL_cyril_havelock={
		name="Cyril Havelock"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=lib_conservatism_transformism_subtype
			traits={  }
			expire="1913.1.1.12"
			id=-1
		}
	}
	MAL_edward_brown={
		name="Edward Brown"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
			}
		}
		country_leader={
			ideology=pat_autocrat_islamic_fundamentalism_subtype
			expire="1923.1.1.12"
			id=-1
		}
	}
	MAL_edmund_buller={
		name="Edmund Buller"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
			}
		}
		country_leader={
			ideology=fascism_subtype
			traits={  }
			expire="1916.1.1.12"
			id=-1
		}
	}
}
