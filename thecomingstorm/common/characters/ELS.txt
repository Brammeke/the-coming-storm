characters={
	ELS_pedro_josé_escalón={
		name="Pedro José Escalón"
		portraits={
			civilian={
				large="gfx/leaders/ELS/Portrait_El_Salvador_Pedro_Jose_Escalon.tga"
			}
		}
		country_leader={
			#desc="POLITICS_MAXIMILIANO_HERNANDEZ_MARTINEZ_DESC"
			ideology=soc_conservatism_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
	ELS_benito_andino={
		name="Benito Andino"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=leninism_subtype
			traits={  }
			expire="1920.1.1.12"
			id=-1
		}
	}
	ELS_justo_araujo={
		name="Justo Araujo"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=marxism_council_communism_subtype
			expire="1920.1.1.12"
			id=-1
		}
	}
	ELS_basilio_estrada={
		name="Basilio Estrada"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=rev_socialism_anarchism_subtype
			expire="1920.1.1.12"
			id=-1
		}
	}
	ELS_marcelino_ezeta={
		name="Marcelino Ezeta"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=soc_democracy_subtype
			traits={  }
			expire="1919.1.1.12"
			id=-1
		}
	}
	ELS_juan_fernández={
		name="Juan Fernández"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=soc_liberal_subtype
			traits={  }
			expire="1919.1.1.12"
			id=-1
		}
	}
	ELS_lorenzo_aguilar={
		name="Lorenzo Aguilar"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=lib_conservatism_subtype
			traits={  }
			expire="1919.1.1.12"
			id=-1
		}
	}
	ELS_bernardo_montes={
		name="Bernardo Montes"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
			}
		}
		country_leader={
			ideology=auth_democrat_theoconservatism_subtype
			expire="1918.1.1.12"
			id=-1
		}
	}
	ELS_Óscar_montes={
		name="Óscar Montes"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
			}
		}
		country_leader={
			ideology=pat_autocrat_islamic_fundamentalism_subtype
			expire="1918.1.1.12"
			id=-1
		}
	}
	ELS_doroteo_estrada={
		name="Doroteo Estrada"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
			}
		}
		country_leader={
			ideology=fascism_subtype
			traits={  }
			expire="1918.1.1.12"
			id=-1
		}
	}
}
