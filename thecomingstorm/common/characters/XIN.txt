characters={
	XIN_yang_zengxin = {
		name = "Yang Zengxin"
		portraits = {
			civilian = {
				large = "gfx/leaders/XIN/Portrait_XIN_Yang_Zengxin.tga"
			}
		}
		country_leader = {
			ideology = auth_democrat_subtype
		}
	}
}
