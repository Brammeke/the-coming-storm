characters={
	TAN_kombudorzhu={
		name="Kombu-Dorzhu"
		portraits={
			civilian={
				large="gfx/leaders/Asia/Portrait_Asia_Generic_1.dds"
			}
		}
		country_leader={
			desc="POLITICS_SALCHAK_TOKA_DESC"
			ideology=auth_democrat_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
	TAN_oboi_linge={
		name="Oboi Linge"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=leninism_subtype
			traits={  }
			expire="1927.1.1.12"
			id=-1
		}
	}
	TAN_deng_xingde={
		name="Deng Xingde"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=marxism_subtype
			expire="1931.1.1.12"
			id=-1
		}
	}
	TAN_fu_shangzhi={
		name="Fu Shangzhi"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=rev_socialism_subtype
			traits={  }
			expire="1920.1.1.12"
			id=-1
		}
	}
	TAN_deng_xiangying={
		name="Deng Xiangying"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=soc_democracy_subtype
			traits={  }
			expire="1925.1.1.12"
			id=-1
		}
	}
	TAN_guan_zhanshan={
		name="Guan Zhanshan"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=soc_liberal_subtype
			expire="1929.1.1.12"
			id=-1
		}
	}
	TAN_ma_xiangying={
		name="Ma Xiangying"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=lib_conservatism_subtype
			expire="1934.1.1.12"
			id=-1
		}
	}
	TAN_guan_kangan={
		name="Guan Kang'an"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=soc_conservatism_transformism_subtype
			traits={  }
			expire="1922.1.1.12"
			id=-1
		}
	}
	TAN_ma_shangzhi={
		name="Ma Shangzhi"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
			}
		}
		country_leader={
			ideology=pat_autocrat_subtype
			traits={  }
			expire="1927.1.1.12"
			id=-1
		}
	}
	TAN_zhao_xiangying={
		name="Zhao Xiangying"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
			}
		}
		country_leader={
			ideology=fascism_subtype
			traits={  }
			expire="1932.1.1.12"
			id=-1
		}
	}
}
