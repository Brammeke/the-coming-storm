characters={
	ROM_constantin_dobrogeanugherea={
		name="Constantin Dobrogeanu-Gherea"
		portraits={
			civilian={
				#large="gfx/leaders/ROM/Constantin_Dobrogeanu-Gherea.dds"
			}
		}
		country_leader={
			desc="POLITICS_GHEREA_DESC"
			ideology=leninism_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			desc="POLITICS_GHEREA_DESC"
			ideology=marxism_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			desc="POLITICS_GHEREA_DESC"
			ideology=rev_socialism_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
	ROM_carol_i={
		name="Carol I"
		portraits={
			civilian={
				large="gfx/leaders/ROM/ROM_Carol_I.png"
			}
		}
		country_leader={
			desc="POLITICS_CAROL_I_DESC"
			ideology=soc_democracy_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			desc="POLITICS_CAROL_I_DESC"
			ideology=soc_liberal_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			desc="POLITICS_CAROL_I_DESC"
			ideology=lib_conservatism_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			desc="POLITICS_CAROL_I_DESC"
			ideology=soc_conservatism_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			desc="POLITICS_CAROL_I_DESC"
			ideology=auth_democrat_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}

	ROM_ferdinand_i={
		name="Ferdinand I"
		portraits={
			civilian={
				large="gfx/leaders/ROM/Portrait_ROM_Ferdinand_I.png"
			}
		}
		country_leader={
			desc="POLITICS_FERDINAND_I_DESC"
			ideology=soc_democracy_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			desc="POLITICS_FERDINAND_I_DESC"
			ideology=soc_liberal_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			desc="POLITICS_FERDINAND_I_DESC"
			ideology=lib_conservatism_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			desc="POLITICS_FERDINAND_I_DESC"
			ideology=soc_conservatism_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			desc="POLITICS_FERDINAND_I_DESC"
			ideology=auth_democrat_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}

	ROM_alexandru_c._cuza={
		name="Alexandru C. Cuza"
		portraits={
			civilian={
				large="gfx/leaders/ROM/ROM_Alexandru_Cuza.png"
			}
		}
		country_leader={
			desc="POLITICS_CUZA_DESC"
			ideology=pat_autocrat_subtype
			expire="1965.1.1.1"
			id=-1
		}
		country_leader={
			desc="POLITICS_CUZA_DESC"
			ideology=fascism_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
	ROM_constantin_prezan={
		name="Constantin Prezan"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Constantin_Prezen.png"
			}
		}
		field_marshal={
			traits={ inflexible_strategist infantry_officer }
			skill=3
			attack_skill=1
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=4000
		}
	}
	ROM_ioan_culcer={
		name="Ioan Culcer"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Ioan_Culcer.png"
			}
		}
		field_marshal={
			traits={ brilliant_strategist trait_engineer }
			skill=3
			attack_skill=3
			defense_skill=2
			planning_skill=3
			logistics_skill=3
			legacy_id=4001
		}
	}
	ROM_alexandru_averescu={
		name="Alexandru Averescu"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Alexandru_Averescu.png"
				small = GFX_idea_ROM_Alexandru_Averescu
			}
		}
		corps_commander={
			traits={ cavalry_officer politically_connected }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=2
			legacy_id=4003
		}
		advisor={
			slot = army_chief
			idea_token = alexandru_averescu
			ledger = army
			allowed = {
				original_tag = ROM
			}
			traits = {
				army_chief_maneuver_2
			}
			cost = 100
			ai_will_do = {
				factor = 1.500
			}
		}
	}
	ROM_constantin_coandă={
		name="Constantin Coandă"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Constantin_Coanda.png"
			}
		}
		corps_commander={
			traits={ career_officer }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=2
			logistics_skill=3
			legacy_id=4005
		}
	}
	ROM_vasile_zottu={
		name="Vasile Zottu"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Vasile_Zottu.png"
			}
		}
		corps_commander={
			traits={ politically_connected }
			skill=2
			attack_skill=1
			defense_skill=2
			planning_skill=3
			logistics_skill=2
			legacy_id=4004
		}
	}
	ROM_panait_warthiadi={
		name="Panait Warthiadi"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Panait_Warthiadi.png"
			}
		}
		corps_commander={
			traits={ old_guard war_hero }
			skill=2
			attack_skill=2
			defense_skill=1
			planning_skill=2
			logistics_skill=2
			legacy_id=4006
		}
	}
	ROM_grigore_crăiniceanu={
		name="Grigore Crăiniceanu"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Grigore_Crainiceanu.png"
				small = GFX_idea_ROM_grigore_crainiceanu
			}
		}
		corps_commander={
			traits={ old_guard trait_cautious }
			skill=2
			attack_skill=1
			defense_skill=2
			planning_skill=1
			logistics_skill=2
			legacy_id=4007
		}
		advisor={
			slot = army_chief
			idea_token = grigore_crainiceanu
			ledger = army
			allowed = {
				original_tag = ROM
			}
			traits = {
				army_chief_old_guard
			}
			cost = 50
			ai_will_do = {
				factor = 1.500
			}
		}
	}
	ROM_artur_văitoianu={
		name="Artur Văitoianu"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Artur_Vaitoianu.png"
			}
		}
		corps_commander={
			traits={ trait_reckless infantry_officer urban_assault_specialist }
			skill=2
			attack_skill=2
			defense_skill=1
			planning_skill=2
			logistics_skill=2
			legacy_id=4008
		}
	}
	ROM_ioan_vercescu={
		name="Ioan Vercescu"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Ioan_Vercescu.png"
			}
		}
		corps_commander={
			traits={ infantry_officer }
			skill=1
			attack_skill=1
			defense_skill=2
			planning_skill=3
			logistics_skill=1
			legacy_id=4009
		}
	}
	ROM_nicolae_popovici={
		name="Nicolae Popovici"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Nicolae_Popovici.png"
			}
		}
		corps_commander={
			traits={ old_guard politically_connected harsh_leader }
			skill=1
			attack_skill=2
			defense_skill=1
			planning_skill=2
			logistics_skill=2
			legacy_id=4010
		}
	}
	ROM_eremia_grigorescu={
		name="Eremia Grigorescu"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Eremia_Grigorescu.png"
			}
		}
		corps_commander={
			traits={ media_personality }
			skill=1
			attack_skill=2
			defense_skill=1
			planning_skill=1
			logistics_skill=2
			legacy_id=4011
		}
	}
	ROM_emanoil_koslinski={
		name="Emanoil Koslinski"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Emanoil_Koslinski.png"
			}
		}
		navy_leader={
			traits={ gentlemanly naval_lineage }
			skill=2
			attack_skill=3
			defense_skill=4
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=4012
		}
	}
	ROM_dan_zaharia={
		name="Dan Zaharia"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Dan_Zaharia.png"
			}
		}
		navy_leader={
			traits={ bold navy_career_officer destroyer_leader }
			skill=1
			attack_skill=3
			defense_skill=3
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=4013
		}
	}
	ROM_eustaţiu_sebastian={
		name="Eustaţiu Sebastian"
		portraits={
			army={
				large="gfx/leaders/ROM/ROM_Eustatiu_Sebastian.png"
			}
		}
		navy_leader={
			traits={ old_guard_navy }
			skill=1
			attack_skill=1
			defense_skill=2
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=4014
		}
	}
	ROM_ioan_istrati={
		name="Ioan Istrati"
		portraits={
			army={
				small = GFX_idea_ROM_Ioan_Istrati
			}
		}
		advisor={
			slot = army_chief
			idea_token = ioan_istrati
			ledger = army
			allowed = {
				original_tag = ROM
			}
			traits = {
				army_chief_reform_2
			}
			cost = 100
			ai_will_do = {
				factor = 1.500
			}
		}
	}
}
