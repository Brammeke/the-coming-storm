characters = {
	### Leaders & Ministers ###
	CRN_gabriele_dannunzio={
		name="Gabriele d'Annunzio"
		portraits={
			civilian={
				large="gfx/leaders/CRN/CRN_Gabriele_d_Annunzio_Portrait.tga"
			}
		}
		country_leader={
			#desc="POLITICS_ADOLF_HITLER_DESC"
			ideology=fascism_subtype
			expire="1965.1.1.1"
			id=-1
		}
		corps_commander = {
			skill = 4
			attack_skill = 2
			defense_skill = 1
			planning_skill = 3
			logistics_skill = 2
		}
	}
	CRN_riccardo_zanella={
		name="Riccardo Zanella"
		portraits={
			civilian={
				large="gfx/leaders/CRN/Portrait_Fiume_Riccardo_Zanella.tga"
			}
		}
		country_leader={
			#desc="POLITICS_ADOLF_HITLER_DESC"
			ideology=soc_conservatism_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
	CRN_antonio_grossich={
		name="Antonio Grossich"
		portraits={
			civilian={
				large="gfx/leaders/CRN/Portrait_Antonio_Grossich_Fiume.tga"
			}
		}
		country_leader={
			#desc="POLITICS_ADOLF_HITLER_DESC"
			ideology=lib_conservatism_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
	CRN_alceste_de_ambris={
		name="Alceste de Ambris"
		portraits={
			civilian={
				large="gfx/leaders/CRN/Portrait_Italy_Alceste_de_Ambrice.tga"
			}
		}
		country_leader={
			#desc="POLITICS_ADOLF_HITLER_DESC"
			ideology=rev_socialism_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
}