characters={
	BUK_abdulahad_bin_muzaffar_aldin={
		name="Abdul-Ahad bin Muzaffar al-Din"
		portraits={
			civilian={
				large="gfx/leaders/BUK/Abdul-Ahad_bin_Muzaffar_al-Din.tga"
			}
		}
		country_leader={
			ideology=pat_autocrat_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
	BUK_bob_white={
		name="Bob White"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=leninism_bolshevism_subtype
			traits={  }
			expire="1919.1.1.12"
			id=-1
		}
	}
	BUK_alex_jones={
		name="Alex Jones"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=marxism_subtype
			traits={  }
			expire="1925.1.1.12"
			id=-1
		}
	}
	BUK_marcus_williams={
		name="Marcus Williams"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=rev_socialism_makhnovism_subtype
			expire="1915.1.1.12"
			id=-1
		}
	}
	BUK_john_williams={
		name="John Williams"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=soc_democracy_subtype
			expire="1921.1.1.12"
			id=-1
		}
	}
	BUK_alexander_walker={
		name="Alexander Walker"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=soc_liberal_subtype
			traits={  }
			expire="1911.1.1.12"
			id=-1
		}
	}
	BUK_jim_brown={
		name="Jim Brown"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=lib_conservatism_subtype
			traits={  }
			expire="1917.1.1.12"
			id=-1
		}
	}
	BUK_peter_evans={
		name="Peter Evans"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=soc_conservatism_transformism_subtype
			traits={  }
			expire="1922.1.1.12"
			id=-1
		}
	}
	BUK_lucas_evans={
		name="Lucas Evans"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
			}
		}
		country_leader={
			ideology=auth_democrat_subtype
			traits={  }
			expire="1912.1.1.12"
			id=-1
		}
	}
	BUK_alexander_roberts={
		name="Alexander Roberts"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
			}
		}
		country_leader={
			ideology=fascism_subtype
			traits={  }
			expire="1918.1.1.12"
			id=-1
		}
	}
}
