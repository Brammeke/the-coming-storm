characters={
	ARM_avetik_sahakyan={
		name="Avetik Sahakyan"
		portraits={
			civilian={
				large="gfx/leaders/ARM/Portrait_ARM_Avetik_Sahakyan.tga"
			}
		}
		country_leader={
			ideology=soc_democracy_subtype
			expire="1953.3.1.1"
			id=-1
		}
	}
}
