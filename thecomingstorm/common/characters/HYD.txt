characters={
	HYD_asaf_jah_vi={
		name="Asaf Jah VI"
		portraits={
			civilian={
				large="gfx/leaders/HYD/Portrait_HYD_Asaf_Jah_VI.tga"
			}
		}
		country_leader={
			#desc="POLITICS_HUBERT_PIERLOT_DESC"
			ideology=pat_autocrat_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
}
