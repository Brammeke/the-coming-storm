characters={
	HUJ_li_tiancai={
		name="Li Tiancai"
		portraits={
			civilian={
				large="gfx/leaders/HUJ/Portrait_HUJ_Li_Tiancai.tga"
			}
		}
		country_leader={
			ideology=auth_democrat_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
	HUJ_alex_williams={
		name="Alex Williams"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=soc_liberal_agrarianism_subtype
			expire="1932.11.12.12"
			id=-1
		}
	}
	HUJ_bob_evans={
		name="Bob Evans"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
			}
		}
		country_leader={
			ideology=pat_autocrat_subtype
			traits={  }
			expire="1928.11.12.12"
			id=-1
		}
	}
	HUJ_john_evans={
		name="John Evans"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=leninism_bolshevism_subtype
			traits={  }
			expire="1936.11.12.12"
			id=-1
		}
	}
	HUJ_john_roberts={
		name="John Roberts"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=marxism_subtype
			traits={  }
			expire="1935.11.12.12"
			id=-1
		}
	}
	HUJ_lucas_jones={
		name="Lucas Jones"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
			}
		}
		country_leader={
			ideology=rev_socialism_syndicalism_subtype
			expire="1934.11.12.12"
			id=-1
		}
	}
	HUJ_jim_taylor={
		name="Jim Taylor"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=soc_democracy_subtype
			traits={  }
			expire="1933.11.12.12"
			id=-1
		}
	}
	HUJ_bob_wilson={
		name="Bob Wilson"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=lib_conservatism_subtype
			traits={  }
			expire="1930.11.12.12"
			id=-1
		}
	}
	HUJ_jonas_white={
		name="Jonas White"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
			}
		}
		country_leader={
			ideology=soc_conservatism_subtype
			expire="1929.11.12.12"
			id=-1
		}
	}
	HUJ_alex_walker={
		name="Alex Walker"
		portraits={
			civilian={
				large="gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
			}
		}
		country_leader={
			ideology=fascism_volkisch_subtype
			traits={  }
			expire="1927.11.12.12"
			id=-1
		}
	}
}
