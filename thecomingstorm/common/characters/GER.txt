characters={
	### Leaders & Ministers ###
	GER_wilhelm_ii={
		name="Wilhelm II"
		portraits={
			civilian={
				large="gfx/leaders/GER/Portrait_GER_Wilhelm_II_2.tga"
			}
		}
		country_leader={
			#desc="POLITICS_ADOLF_HITLER_DESC"
			ideology=lib_conservatism_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
	GER_friedrich_ebert = {
		name = "Friedrich Ebert"
		portraits = {
			civilian = {
				large = "gfx/leaders/GER/Portrait_GER_Friedrich_Ebert.tga"
				small = "gfx/interface/ministers/GER/GER_friedrich_ebert.tga"
			}
		}
		country_leader = {
			#desc = "POLITICS_KURT_SCHUSCHNIGG_DESC"
			#picture = "gfx/leaders/GER/Portrait_GER_Friedrich_Ebert.tga"
			expire = "1965.1.1"
			ideology = soc_democracy_subtype
			traits = {
				#
			}
		}
	}
	### Generals ###
	GER_paul_von_hindenburg={
		name="Paul von Hindenburg"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Paul_von_Hindenburg.tga"
			}
		}
		field_marshal={
			traits={ old_guard brilliant_strategist media_personality war_hero inspirational_leader }
			skill=4
			attack_skill=4
			defense_skill=3
			planning_skill=4
			logistics_skill=3
			legacy_id=445001
		}
	}
	GER_karl_von_bülow={
		name="Karl von Bülow"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Karl_von_Bulow.tga"
			}
		}
		field_marshal={
			traits={ old_guard infantry_officer defensive_doctrine }
			skill=2
			attack_skill=2
			defense_skill=2
			planning_skill=2
			logistics_skill=2
			legacy_id=445002
		}
	}
	GER_august_von_mackensen={
		name="August von Mackensen"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_August_von_Mackensen.tga"
			}
		}
		field_marshal={
			traits={ old_guard brilliant_strategist cavalry_officer }
			skill=4
			attack_skill=5
			defense_skill=3
			planning_skill=4
			logistics_skill=3
			legacy_id=445003
		}
	}
	GER_alfred_von_schlieffen={
		name="Alfred von Schlieffen"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Alfred_von_Schlieffen.tga"
			}
		}
		field_marshal={
			traits={ old_guard inflexible_strategist war_hero }
			skill=3
			attack_skill=3
			defense_skill=2
			planning_skill=3
			logistics_skill=2
			legacy_id=445004
		}
	}
	GER_leopold_von_bayern={
		name="Leopold von Bayern"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Leopold_von_Bayern.tga"
			}
		}
		field_marshal={
			traits={ old_guard war_hero skilled_staffer }
			skill=4
			attack_skill=4
			defense_skill=3
			planning_skill=4
			logistics_skill=2
			legacy_id=445016
		}
	}
	GER_helmuth_von_moltke={
		name="Helmuth von Moltke"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Helmuth_von_Moltke.tga"
			}
		}
		corps_commander={
			traits={ old_guard infantry_officer politically_connected }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=2
			logistics_skill=1
			legacy_id=445005
		}
	}
	GER_remus_von_woyrsch={
		name="Remus von Woyrsch"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Remus_von_Woyrsch.tga"
			}
		}
		corps_commander={
			traits={ old_guard infantry_officer }
			skill=2
			attack_skill=2
			defense_skill=3
			planning_skill=3
			logistics_skill=1
			legacy_id=445006
		}
	}
	GER_hermann_von_eichhorn={
		name="Hermann von Eichhorn"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Hermann_von_Eichhorn.tga"
			}
		}
		corps_commander={
			traits={ old_guard war_hero }
			skill=2
			attack_skill=3
			defense_skill=2
			planning_skill=2
			logistics_skill=2
			legacy_id=445007
		}
	}
	GER_alexander_von_kluck={
		name="Alexander von Kluck"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Alexander_von_Kluck.tga"
			}
		}
		corps_commander={
			traits={ old_guard trait_cautious offensive_doctrine }
			skill=3
			attack_skill=3
			defense_skill=2
			planning_skill=3
			logistics_skill=2
			legacy_id=445008
		}
	}
	GER_ferdinand_von_quast={
		name="Ferdinand von Quast"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Ferdinand_von_Quast.tga"
			}
		}
		corps_commander={
			traits={ old_guard inflexible_strategist infantry_officer }
			skill=3
			attack_skill=2
			defense_skill=4
			planning_skill=4
			logistics_skill=3
			legacy_id=445009
		}
	}
	GER_erich_ludendorff={
		name="Erich Ludendorff"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Erich_Ludendorff.tga"
			}
		}
		corps_commander={
			traits={ brilliant_strategist career_officer war_hero }
			skill=4
			attack_skill=3
			defense_skill=3
			planning_skill=4
			logistics_skill=4
			legacy_id=445010
		}
	}
	GER_albrecht_von_württemberg={
		name="Albrecht von Württemberg"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Albrecht_von_Wurttemberg.tga"
			}
		}
		corps_commander={
			traits={ cavalry_officer politically_connected }
			skill=3
			attack_skill=4
			defense_skill=2
			planning_skill=3
			logistics_skill=3
			legacy_id=445011
		}
	}
	GER_rupprecht_von_bayern={
		name="Rupprecht von Bayern"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Rupprecht_von_Bayern.tga"
			}
		}
		corps_commander={
			traits={ inflexible_strategist politically_connected }
			skill=3
			attack_skill=2
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=445012
		}
	}
	GER_max_von_gallwitz={
		name="Max von Gallwitz"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Max_von_Gallwitz.tga"
			}
		}
		corps_commander={
			traits={ brilliant_strategist career_officer }
			skill=4
			attack_skill=4
			defense_skill=4
			planning_skill=3
			logistics_skill=3
			legacy_id=445013
		}
	}
	GER_otto_von_below={
		name="Otto von Below"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Otto_von_Below.tga"
			}
		}
		corps_commander={
			traits={ trait_reckless infantry_officer }
			skill=3
			attack_skill=3
			defense_skill=2
			planning_skill=3
			logistics_skill=3
			legacy_id=445014
		}
	}
	GER_wilhelm_von_preußen={
		name="Wilhelm von Preußen"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Wilhelm_von_Preussen.tga"
			}
			civilian={
				large="gfx/leaders/GER/Portrait_GER_Wilhelm_III_2.tga"
			}
		}
		corps_commander={
			traits={ media_personality politically_connected }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=445015
		}
		country_leader={
			#desc="POLITICS_ADOLF_HITLER_DESC"
			ideology=lib_conservatism_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
	GER_felix_von_bothmer={
		name="Felix von Bothmer"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Felix_von_Bothmer.tga"
			}
		}
		corps_commander={
			traits={ inflexible_strategist infantry_officer }
			skill=2
			attack_skill=2
			defense_skill=2
			planning_skill=2
			logistics_skill=2
			legacy_id=445017
		}
	}
	GER_hermann_von_françois={
		name="Hermann von François"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Hermann_von_Francois.tga"
			}
		}
		corps_commander={
			traits={ brilliant_strategist trait_reckless }
			skill=3
			attack_skill=3
			defense_skill=2
			planning_skill=3
			logistics_skill=2
			legacy_id=445018
		}
	}
	GER_wilhelm_ii_von_württemberg={
		name="Wilhelm II von Württemberg"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Wilhelm_II_von_Wurttemberg.tga"
			}
		}
		corps_commander={
			traits={ cavalry_officer politically_connected }
			skill=1
			attack_skill=2
			defense_skill=2
			planning_skill=1
			logistics_skill=1
			legacy_id=445019
		}
	}
	GER_georg_von_der_marwitz={
		name="Georg von der Marwitz"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Georg_von_der_Marwitz.tga"
			}
		}
		corps_commander={
			traits={ cavalry_officer }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=2
			legacy_id=445020
		}
	}
	GER_oskar_von_hutier={
		name="Oskar von Hutier"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Oskar_von_Hutier.tga"
			}
		}
		corps_commander={
			traits={ career_officer }
			skill=3
			attack_skill=3
			defense_skill=2
			planning_skill=3
			logistics_skill=4
			legacy_id=445021
		}
	}
	GER_walther_von_lüttwitz={
		name="Walther von Lüttwitz"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Walther_von_Luttwitz.tga"
			}
		}
		corps_commander={
			traits={ career_officer infantry_officer war_hero }
			skill=2
			attack_skill=3
			defense_skill=2
			planning_skill=3
			logistics_skill=2
			legacy_id=445022
		}
	}
	GER_friedrich_sixt_von_armin={
		name="Friedrich Sixt von Armin"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Friedrich_Sixt_von_Armin.tga"
			}
		}
		corps_commander={
			traits={ old_guard inflexible_strategist }
			skill=3
			attack_skill=1
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=445023
		}
	}
	GER_josias_von_heeringen={
		name="Josias von Heeringen"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Josias_von_Heeringen.tga"
			}
		}
		corps_commander={
			traits={ old_guard war_hero }
			skill=2
			attack_skill=2
			defense_skill=3
			planning_skill=2
			logistics_skill=2
			legacy_id=445024
		}
	}
	GER_maximilian_von_prittwitz_und_gaffron={
		name="Maximilian von Prittwitz und Gaffron"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Maximilian_von_Prittwitz_und_Gaffron.tga"
			}
		}
		corps_commander={
			traits={ old_guard }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=2
			legacy_id=445025
		}
	}
	GER_ludwig_von_falkenhausen={
		name="Ludwig von Falkenhausen"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Ludwig_von_Falkenhausen.tga"
			}
		}
		corps_commander={
			traits={ cavalry_officer }
			skill=2
			attack_skill=2
			defense_skill=2
			planning_skill=2
			logistics_skill=1
			legacy_id=445026
		}
	}
	GER_hans_hartwig_von_beseler={
		name="Hans Hartwig von Beseler"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Hans_Hartwig_von_Beseler.tga"
			}
		}
		corps_commander={
			traits={ old_guard politically_connected }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=445027
		}
	}
	GER_friedrich_august_iii_von_sachsen={
		name="Friedrich August III von Sachsen"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Friedrich_August_III_von_Sachsen.tga"
			}
		}
		corps_commander={
			traits={ trait_cautious cavalry_officer politically_connected }
			skill=2
			attack_skill=2
			defense_skill=1
			planning_skill=2
			logistics_skill=2
			legacy_id=445028
		}
	}
	GER_friedrich_von_scholtz={
		name="Friedrich von Scholtz"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Friedrich_von_Scholtz.tga"
			}
		}
		corps_commander={
			traits={ career_officer skilled_staffer }
			skill=2
			attack_skill=3
			defense_skill=2
			planning_skill=2
			logistics_skill=2
			legacy_id=445029
		}
	}
	GER_alexander_von_linsingen={
		name="Alexander von Linsingen"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Alexander_von_Linsingen.tga"
			}
		}
		corps_commander={
			traits={ harsh_leader infantry_officer war_hero }
			skill=2
			attack_skill=3
			defense_skill=2
			planning_skill=3
			logistics_skill=1
			legacy_id=445030
		}
	}
	GER_erich_von_falkenhayn={
		name="Erich von Falkenhayn"
		portraits={
			army={
				##small=""
			}
			army={
				large="gfx/leaders/GER/Portrait_GER_Erich_von_Falkenhayn.tga"
			}
		}
		corps_commander={
			traits={ old_guard infantry_officer }
			skill=4
			attack_skill=3
			defense_skill=3
			planning_skill=4
			logistics_skill=4
			legacy_id=445031
		}
	}
}
