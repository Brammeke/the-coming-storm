characters={
	COG_leopold_ii={
		name="Leopold II"
		portraits={
			civilian={
				large="gfx/leaders/BEL/Portrait_BEL_Leopold_II.tga"
			}
		}
		country_leader={
			#desc="POLITICS_HUBERT_PIERLOT_DESC"
			ideology=pat_autocrat_subtype
			expire="1965.1.1.1"
			id=-1
		}
	}
}
