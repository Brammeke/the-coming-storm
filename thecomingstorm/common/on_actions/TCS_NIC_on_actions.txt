on_actions = {
    on_startup = {
        effect = {
            if = {
                limit = {
					has_start_date < 1910.1.1
            		has_start_date > 1905.01.01
                }
            	NIC = {
					country_event = {
						id = NIC_political.1
						days = 735
					}
            	}
			}
		}
	}
	on_capitulation = {
		effect = {
			if = {
				limit = {
					FROM = {
						tag = NIC
					}
					ROOT = {
						tag = HON
					}
				}
				NIC = {
					white_peace = HON
					white_peace = ELS
				}
				NIC = {
					puppet = HON
				}
				HON = {
					set_politics = {	
						ruling_party = soc_liberal
						last_election = "1902.5.13"
						election_frequency = 48
						elections_allowed = yes
					}
					set_popularities = {
						leninism = 0
						marxism = 0
						rev_socialism = 0
						soc_democracy = 0
						soc_liberal = 47
						lib_conservatism = 8
						soc_conservatism = 43
						auth_democrat = 2
						pat_autocrat = 0
						fascism = 0
					}
				}
			}
		}
	}
}