on_actions = {
    on_startup = {
        effect = {
			if = {
                limit = {
            		has_start_date > 1918.11.11
                }
            	POL = {
					add_equipment_to_stockpile = {
                        type = infantry_equipment_1
                        amount = 7000
                        producer = GER
                    }
					add_equipment_to_stockpile = {
                        type = artillery_equipment_1
                        amount = 280
                        producer = AUS
                    }
					add_equipment_to_stockpile = {
                        type = artillery_equipment_1
                        amount = 280
                        producer = GER
                    }
                    add_equipment_to_stockpile = {
                        type = support_equipment_1
                        amount = 3000
                        producer = GER
                    }
            	}
			}

            if = {
                limit = {
            		has_start_date > 1918.11.11
                }
            	GGP = {
					add_equipment_to_stockpile = {
                        type = infantry_equipment_1
                        amount = 3500
                        producer = GER
                    }
					add_equipment_to_stockpile = {
                        type = artillery_equipment_1
                        amount = 280
                        producer = AUS
                    }
					add_equipment_to_stockpile = {
                        type = artillery_equipment_1
                        amount = 280
                        producer = GER
                    }
                    add_equipment_to_stockpile = {
                        type = support_equipment_1
                        amount = 2000
                        producer = GER
                    }
            	}
			}
            if = {
                limit = {
            		has_start_date > 1918.11.11
                }
            	POL = {
					country_event = {
						id = poland_1918.1
						days = 2
					}
            	}
			}
            if = {
                limit = {
            		has_start_date > 1918.11.11
                }
            	GER = {
					country_event = {
						id = GER_political_1918.8
						days = 45
					}
            	}
			}

            if = {
                limit = {
            		has_start_date > 1918.11.11
                }
            	GER = {
					country_event = {
						id = GER_political_1918.7
						days = 96
					}
            	}
			}
        }
    }
    # ROOT is capitulated country, FROM is winner
	on_capitulation = {
		effect = {
			if = {
				limit = {
					#POL = { has_country_flag = GRE_megali_idea_approved }
					original_tag = WUK
					has_war_with = POL
				}
				POL = { country_event = { id = poland_1918_dip.7 days = 0 } }
			}
		}
	}
}
