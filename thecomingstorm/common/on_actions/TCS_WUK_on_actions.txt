on_actions = {
    on_startup = {
        effect = {
			if = {
                limit = {
            		has_start_date > 1918.11.11
                }
            	WUK = {
					add_equipment_to_stockpile = {
                        type = infantry_equipment_1
                        amount = 2500
                        producer = AUS
                    }
					add_equipment_to_stockpile = {
                        type = artillery_equipment_1
                        amount = 40
                        producer = AUS
                    }
                    add_equipment_to_stockpile = {
                        type = support_equipment_1
                        amount = 200
                        producer = AUS
                    }
            	}
			}
            if = {
                limit = {
            		has_start_date > 1918.11.11
                }
            	WUK = {
					country_event = {
						id = wuk.0
						days = 1
					}
            	}
			}
        }
    }
}
