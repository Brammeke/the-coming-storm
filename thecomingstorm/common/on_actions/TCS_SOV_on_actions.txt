on_actions = {
    on_startup = {
        effect = {
			if = {
                limit = {
            		has_start_date > 1918.11.11
                }
            	SOV = {
					add_equipment_to_stockpile = {
                        type = infantry_equipment_1
                        amount = 25000
                        producer = SOV
                    }
					add_equipment_to_stockpile = {
                        type = artillery_equipment_1
                        amount = 280
                        producer = SOV
                    }
            	}
			}
            if = {
                limit = {
            		has_start_date > 1918.11.11
                }
            	SOV = {
					country_event = {
						id = SOV_political.1
						days = 1
					}
            	}
			}
        }
    }
}
