on_actions = {
    on_startup = {
        effect = {
            ### Election February 2nd 1919
            if = {
                limit = {
            		has_start_date > 1918.11.11
                }
                BAV = {
					country_event = {
						id = BAV_elections.1
						days = 82
					}
            	}
			}
			### Eisner Shot ###
			if = {
                limit = {
            		has_start_date > 1918.11.11
                }
                BAV = {
					country_event = {
						id = BAV_political.1
						days = 101
					}
            	}
            }
        }
    }
	on_capitulation_immediate = {
		effect = {
			if = {
				limit = {
					ROOT = {
						tag = BSR
					}
					FROM = {
						tag = BAV
					}
				}
				BAV = {
					set_country_flag = BAV_soviets_defeated
				}
			}
		}
	}
	on_capitulation_immediate = {
		effect = {
			if = {
				limit = {
					ROOT = {
						tag = BAV
					}
					FROM = {
						tag = BSR
					}
				}
				BSR = {
					set_country_flag = BSR_soviets_victorious
				}
			}
		}
	}
	on_capitulation_immediate = {
		effect = {
			if = {
				limit = {
					ROOT = {
						tag = GER
					}
					FROM = {
						tag = BSR
					}
				}
				BSR = {
					annex_country = {
						target = GER
					}
					country_event = {
						id = GER_CIVIL_WAR_political.1
						days = 10
						random_days = 2
					}
				}
			}
		}
	}
}
