on_actions = {
	on_startup = {
		effect = {
			if = {
				limit = {
					has_start_date < 1910.1.1
            		has_start_date > 1905.1.1
                }
                SER = {
                	country_event = { id = SER_political.0 days = 1 }
                }
			}
		}
	}
}
