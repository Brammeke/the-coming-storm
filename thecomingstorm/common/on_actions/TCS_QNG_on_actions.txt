on_actions = {
	on_startup = {
		effect = {
			if = {
				limit = {
					has_start_date < 1905.1.2
				}
				QNG = {
					country_event = {
						id = qing.1
						days = 91
					}
					country_event = {
						id = qing.8
						days = 92
					}
					country_event = {
						id = qing.9
						days = 102
					}
					country_event = {
						id = qing.10
						days = 124
					}
					country_event = {
						id = qing.11
						days = 231
					}
					country_event = {
						id = qing.12
						days = 511
					}
					country_event = {
						id = qing.19
						days = 751
					}
					country_event = {
						id = qing.20
						days = 1413
					}
				}
			}
			if = {
				limit = {
					has_start_date < 1914.1.1
					has_start_date > 1905.1.2
				}
				QNG = {
					activate_mission = TCS_XINHAI_wuchang_uprising
				}
			}
			if = {
				limit = {
					has_start_date < 1905.1.2
				}
				QNG = {
					country_event = TCS_XINHAI_debug.1
				}
			}
		}
	}
}