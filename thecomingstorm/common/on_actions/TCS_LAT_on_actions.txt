on_actions = {
	on_capitulation = {
		effect = {
			if = {
				limit = {
					LAT = {
						has_country_flag = latvian_struggle
					}
					original_tag = LSR
					has_war_with = LAT
				}
				LAT = { country_event = { id = latvia.9 days = 0 } }
			}
			if = {
				limit = {
					LAT = {
						has_country_flag = latvian_bermontists_attack
					}
					original_tag = WRC
					has_war_with = LAT
				}
				LAT = { country_event = { id = latvia.18 days = 0 } }
			}
			if = {
				limit = {
					LSR = {
						has_country_flag = latvian_struggle
					}
					original_tag = LAT
					has_war_with = LSR
				}
				LSR = { country_event = { id = latvianssr.9 days = 0 } }
			}
			if = {
				limit = {
					WRC = {
						has_country_flag = latvian_bermontists_attack
					}
					original_tag = LAT
					has_war_with = WRC
				}
				WRC = { country_event = { id = wrc.1 days = 0 } }
			}
			if = {
				limit = {
					WRC = {
						has_country_flag = WRC_invasion_estonia_flag
					}
					original_tag = EST
					has_war_with = WRC
					ESR = { exists = no }
				}
				WRC = { country_event = { id = wrc.3 days = 0 } }
			}
			if = {
				limit = {
					WRC = {
						has_country_flag = WRC_invasion_estonia_flag
					}
					original_tag = EST
					has_war_with = WRC
					ESR = { exists = yes }
				}
				WRC = { country_event = { id = wrc.11 days = 0 } }
			}
			if = {
				limit = {
					WRC = {
						has_country_flag = WRC_invasion_estonia_flag
					}
					original_tag = ESR
					has_war_with = WRC
					EST = { exists = no }
				}
				WRC = { country_event = { id = wrc.3 days = 0 } }
			}
			if = {
				limit = {
					WRC = {
						has_country_flag = WRC_invasion_estonia_flag
					}
					original_tag = ESR
					has_war_with = WRC
					EST = { exists = yes }
				}
				WRC = { country_event = { id = wrc.12 days = 0 } }
			}
			if = {
				limit = {
					WRC = {
						has_country_flag = WRC_invasion_lithuania_flag
					}
					original_tag = LIT
					has_war_with = WRC
				}
				WRC = { country_event = { id = wrc.4 days = 0 } }
			}
		}
	}
}