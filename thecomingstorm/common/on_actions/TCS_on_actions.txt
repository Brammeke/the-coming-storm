on_actions = {
    ### Starting Splash ###
    on_startup = {
        effect = {
            every_country = {
				limit = {
					is_ai = no
				}
				set_country_flag = show_welcome_splash
			}
        }
    }
    ### Tech ###
    on_startup = {
        effect = {
            every_country = {
				limit = {
					OR = {
                        AND = {
                            any_owned_state = {
                                is_capital = yes
                                is_on_continent = europe
                            }
                        }
                    }
				}
                set_technology = {
                    basic_train = 1
                    popup = no
                }
			}
        }
    }
    ### Coalitions & Oppositions ###
    on_startup = {
        effect = {
            HOL = {
                country_event = {
                    id = tcs_general.1
                }
            }
        }
    }
    on_daily = {
        effect = {
            TCS_calc_coalitions = yes
            TCS_calc_oppositions = yes
        }
    }
    ### Dutch Parliament ###
    on_daily = {
        effect = {
            HOL_recalc_parliament_icon_frames = yes
        }
    }
    ### Unit Limiter ###
    on_startup = {
        effect = {
            every_country = {
                calculate_unit_limit_full = yes
            }
        }
    }
    on_daily = {
        effect = {
            if = {
                limit = {
                    NOT = { has_global_flag = TCS_no_unit_limits }
                }
                calculate_unit_limit_full = yes
            }
        }
    }
}