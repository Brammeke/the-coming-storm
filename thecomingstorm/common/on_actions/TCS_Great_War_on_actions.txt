on_actions = {
	on_capitulation_immediate = {
		effect = {
			if = {
				limit = {
					tag = BEL
					has_war = yes
					has_war_with = GER
					#has_global_flag = BUL_second_balkan_war
				}
				ENG = {
					add_to_faction = BEL
				}
			}
			if = {
				limit = {
					tag = MNT
					has_war = yes
					has_war_with = AUS
					#has_global_flag = BUL_second_balkan_war
				}
				ENG = {
					add_to_faction = MNT
				}
			}
			# if = {
			# 	limit = {
			# 		tag = LUX
			# 		has_war = yes
			# 		#has_global_flag = BUL_second_balkan_war
			# 	}
			# 	ENG = {
			# 		add_to_faction = LUX
			# 	}
			# }
			if = {
				limit = {
					tag = SER
					has_war = yes
					has_war_with = BUL
				}
				BUL = {
					set_state_controller = 106
					set_state_controller = 828
					set_state_controller = 829
					set_state_controller = 830
					set_state_controller = 831
					set_state_controller = 108
				}
				AUS = {
					set_state_controller = 107
					set_state_controller = 832
					set_state_controller = 827
					set_state_controller = 824
				}
			}
			if = {
				limit = {
					tag = MNT
					has_war = yes
				}
				every_owned_state = {
					set_state_controller_to = AUS
				}
			}
		}
	}
	#ROOT is new controller #FROM is old controller #FROM.FROM is state ID
	on_daily_RUS = {
		effect = {
			if = { # Ukrainian Independence
				limit = {
					tag = RUS
					RUS = {
						has_war_with = GER
					}
					date > 1917.11.20
				}
				RUS = {
					country_event = {
						id = TCS_GW_RUS_event.3
						hours = 6
					}
				}
			}
			if = { # Provisional Government
				limit = {
					tag = RUS
					RUS = {
						has_war_with = GER
					}
					date > 1917.3.8
				}
				RUS = {
					country_event = {
						id = TCS_GW_RUS_event.1
						random_days = 8
					}
				}
			}
		}
	}
}