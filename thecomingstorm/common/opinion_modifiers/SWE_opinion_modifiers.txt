opinion_modifiers = {

	SWE_diplomatic_mission = { value = 20 }

	SWE_diplomatic_mission_neg = { value = -15 }

	SWE_diplomatic_mission_ENG = { value = 20 }

	SWE_diplomatic_mission_GER = { value = 20 }

	SWE_royal_marriage = { value = 10 }

	SWE_trade_deal = { value = 10 }

	SWE_arms_deal = { value = 5 }

	SWE_rejected_deal = { value = -20 }

	SWE_large_diaspora = { value = 30 }

}