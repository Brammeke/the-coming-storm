opinion_modifiers = {
	OTT_member_of_empire = { value = 50 }

	OTT_annexed_our_land = { value = -75 }

	OTT_claimed_our_lands = { value = -50 }

	OTT_caliph_opinion = { value = 30 }

	OTT_taba_loss_opinion = { value = -30 }

	OTT_arabian_friction = { 
		value = -15
		months = 3 
	}

	OTT_arabian_duel_win = { 
		value = -15
		months = 3 
		min_trust = -2
	}

	OTT_arabian_duel_loss = { 
		value = -40
		months = 6
		min_trust = -5
	}
}