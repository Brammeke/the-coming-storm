defined_text = {
	name = swe_partyslot
	text = {
		trigger = { check_variable = { v = 1 } }
		localization_key = SWE_libcon_1
	}
	text = {
		trigger = { check_variable = { v = 2 } }
		localization_key = SWE_soccon_1
	}
	text = {
		trigger = { check_variable = { v = 3 } }
		localization_key = SWE_socdem_1
	}
	text = {
		trigger = { check_variable = { v = 4 } }
		localization_key = SWE_autdem_1
	}
	text = {
		trigger = { check_variable = { v = 5 } }
		localization_key = SWE_revsoc_1
	}
	text = {
		trigger = { check_variable = { v = 6 } }
		localization_key = fitta
	}
}
defined_text = {
    name = TCS_SWE_support_icon_gfx
    text = {
        trigger = { 
			check_variable = { 
				var = v
				compare = greater_than_or_equals
				value = 1
			} 
		}
        localization_key = "GFX_checkbox_small_green"
    }
    text = {
        trigger = { check_variable = { v = 0 } }
        localization_key = "GFX_checkbox_small_red"
    }
}
defined_text = {
	name = TCS_SWE_ideology_icon_gfx
	text = {
		trigger = {
			has_government = leninism
		}
		localization_key = "GFX_ideology_leninism_group"
	}
	text = {
		trigger = {
			has_government = marxism
		}
		localization_key = "GFX_ideology_marxism_group"
	}
	text = {
		trigger = {
			has_government = rev_socialism
		}
		localization_key = "GFX_ideology_rev_socialism_group"
	}
	text = {
		trigger = {
			has_government = soc_democracy
		}
		localization_key = "GFX_ideology_soc_democracy_group"
	}
	text = {
		trigger = {
			has_government = soc_liberal
		}
		localization_key = "GFX_ideology_soc_liberal_group"
	}
	text = {
		trigger = {
			has_government = lib_conservatism
		}
		localization_key = "GFX_ideology_lib_conservatism_group"
	}
	text = {
		trigger = {
			has_government = soc_conservatism
		}
		localization_key = "GFX_ideology_soc_conservatism_group"
	}
	text = {
		trigger = {
			has_government = auth_democrat
		}
		localization_key = "GFX_ideology_auth_democrat_group"
	}
	text = {
		trigger = {
			has_government = pat_autocrat
		}
		localization_key = "GFX_ideology_pat_autocrat_group"
	}
	text = {
		trigger = {
			has_government = fascism
		}
		localization_key = "GFX_ideology_fascism_group"
	}
}
defined_text = {
	name = SWE_current_issue_bottom
	text = {
		trigger = {
			has_country_flag = reform_swe_nor_union_breakup
		}
		localization_key = SWE_reform_swe_nor_union_breakup
	}
	text = {
		trigger = {
			has_country_flag = issue_voting
		}
		localization_key = SWE_current_issue_1
	}
	text = {
		trigger = {
			has_country_flag = issue_safety_reforms
		}
		localization_key = SWE_current_issue_2
	}
	text = {
		trigger = {
			has_country_flag = issue_norwegian_model
		}
		localization_key = SWE_current_issue_3
	}
	text = {
		trigger = {
			has_country_flag = issue_people_not_lords
		}
		localization_key = SWE_current_issue_4
	}
	text = {
		trigger = {
			always = yes
		}
		localization_key = SWE_current_issue_none
	}
}