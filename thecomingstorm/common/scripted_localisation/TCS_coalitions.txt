
### Coalition ###

defined_text = {
    name = coalitionActive
    text = {
        trigger = {
            check_variable = { coalition_pop_total > 0 }
        }
        localization_key = TCS_TOPBAR_COALITION
    }
    text = {
        localization_key = TCS_TOPBAR_NO_COALITION
    }
}
defined_text = {
    name = coalitionOrPartyPop
    text = {
        trigger = {
            check_variable = { coalition_pop_total > 0 }
        }
        localization_key = TCS_COALITION_POP
    }
    text = {
        localization_key = TCS_PARTY_POP
    }
}
defined_text = {
    name = coalitionAnarchism
    text = {
        trigger = {
            has_country_flag = leninism_coalition
        }
        localization_key = COALITION_ANARCHISM
    }
}
defined_text = {
    name = coalitionmarxism
    text = {
        trigger = {
            has_country_flag = marxism_coalition
        }
        localization_key = COALITION_marxism
    }
}
defined_text = {
    name = coalitionRevSocialism
    text = {
        trigger = {
            has_country_flag = rev_socialism_coalition
        }
        localization_key = COALITION_REV_SOCIALISM
    }
}
defined_text = {
    name = coalitionSocDemocracy
    text = {
        trigger = {
            has_country_flag = soc_democracy_coalition
        }
        localization_key = COALITION_SOC_DEMOCRACY
    }
}
defined_text = {
    name = coalitionSocLiberal
    text = {
        trigger = {
            has_country_flag = soc_liberal_coalition
        }
        localization_key = COALITION_SOC_LIBERAL
    }
}
defined_text = {
    name = coalitionLibConservatism
    text = {
        trigger = {
            has_country_flag = lib_conservatism_coalition
        }
        localization_key = COALITION_LIB_CONSERVATISM
    }
}
defined_text = {
    name = coalitionSocConservatism
    text = {
        trigger = {
            has_country_flag = soc_conservatism_coalition
        }
        localization_key = COALITION_SOC_CONSERVATISM
    }
}
defined_text = {
    name = coalitionAuthDemocrat
    text = {
        trigger = {
            has_country_flag = auth_democrat_coalition
        }
        localization_key = COALITION_AUTH_DEMOCRAT
    }
}
defined_text = {
    name = coalitionPatAutocrat
    text = {
        trigger = {
            has_country_flag = pat_autocrat_coalition
        }
        localization_key = COALITION_PAT_AUTOCRAT
    }
}
defined_text = {
    name = coalitionFascism
    text = {
        trigger = {
            has_country_flag = fascism_coalition
        }
        localization_key = COALITION_FASCISM
    }
}

### Opposition ###

defined_text = {
    name = oppositionActive
    text = {
        trigger = {
            check_variable = { opposition_pop_total > 0 }
        }
        localization_key = TCS_TOPBAR_OPPOSITION
    }
    text = {
        localization_key = TCS_TOPBAR_NO_OPPOSITION
    }
}
defined_text = {
    name = oppositionAnarchism
    text = {
        trigger = {
            has_country_flag = leninism_opposition
        }
        localization_key = OPPOSITION_ANARCHISM
    }
}
defined_text = {
    name = oppositionmarxism
    text = {
        trigger = {
            has_country_flag = marxism_opposition
        }
        localization_key = OPPOSITION_marxism
    }
}
defined_text = {
    name = oppositionRevSocialism
    text = {
        trigger = {
            has_country_flag = rev_socialism_opposition
        }
        localization_key = OPPOSITION_REV_SOCIALISM
    }
}
defined_text = {
    name = oppositionSocDemocracy
    text = {
        trigger = {
            has_country_flag = soc_democracy_opposition
        }
        localization_key = OPPOSITION_SOC_DEMOCRACY
    }
}
defined_text = {
    name = oppositionSocLiberal
    text = {
        trigger = {
            has_country_flag = soc_liberal_opposition
        }
        localization_key = OPPOSITION_SOC_LIBERAL
    }
}
defined_text = {
    name = oppositionLibConservatism
    text = {
        trigger = {
            has_country_flag = lib_conservatism_opposition
        }
        localization_key = OPPOSITION_LIB_CONSERVATISM
    }
}
defined_text = {
    name = oppositionSocConservatism
    text = {
        trigger = {
            has_country_flag = soc_conservatism_opposition
        }
        localization_key = OPPOSITION_SOC_CONSERVATISM
    }
}
defined_text = {
    name = oppositionAuthDemocrat
    text = {
        trigger = {
            has_country_flag = auth_democrat_opposition
        }
        localization_key = OPPOSITION_AUTH_DEMOCRAT
    }
}
defined_text = {
    name = oppositionPatAutocrat
    text = {
        trigger = {
            has_country_flag = pat_autocrat_opposition
        }
        localization_key = OPPOSITION_PAT_AUTOCRAT
    }
}
defined_text = {
    name = oppositionFascism
    text = {
        trigger = {
            has_country_flag = fascism_opposition
        }
        localization_key = OPPOSITION_FASCISM
    }
}