ideas = {
	### Ideas ###
    country = {
        LSR_seized_bank_reserves = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_seized_bank_reserves" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = Money3Asset
            modifier = {
				consumer_goods_factor = -0.10
				stability_factor = 0.05
				political_power_factor = 0.10
            }
        }

        LSR_disrupted_food_supply = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_disrupted_food_supply" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = raj_risk_of_famine
            modifier = {
               MONTHLY_POPULATION = -0.20
               political_power_factor = -0.15
            }
        }

        LSR_disrupted_food_supply_b = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_disrupted_food_supply_b" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = raj_risk_of_famine
            modifier = {
               MONTHLY_POPULATION = -0.15
               political_power_factor = -0.10
            }
        }

        LSR_disrupted_food_supply_c = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_disrupted_food_supply_c" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = raj_risk_of_famine
            modifier = {
               MONTHLY_POPULATION = -0.10
               political_power_factor = -0.05
            }
        }

        LSR_disrupted_food_supply_d = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_disrupted_food_supply_d" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = raj_risk_of_famine
            modifier = {
               MONTHLY_POPULATION = -0.05
               political_power_factor = -0.05
            }
        }

        LSR_farmers_strike = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_farmers_strike" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = FRA_scw_intervention_republicans_focus
            modifier = {
               army_org_factor = -0.08
               stability_factor = -0.1
            }
        }

        LSR_farmers_strike_b = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_farmers_strike_b" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = FRA_scw_intervention_republicans_focus
            modifier = {
               army_org_factor = -0.05
               stability_factor = -0.05
            }
        }

        LSR_farmers_strike_c = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_farmers_strike_c" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = FRA_scw_intervention_republicans_focus
            modifier = {
               army_org_factor = -0.02
               stability_factor = -0.05
            }
        }

        LSR_flintenweiber = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_flintenweiber" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = ast_volunteer_defence_corps
            modifier = {
               conscription_factor = 0.05
               stability_factor = 0.05
            }
        }

        LSR_high_workers_support = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_high_workers_support" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = generic_production_bonus
            modifier = {
	       consumer_goods_factor = -0.10
               stability_factor = 0.05
            }
        }
		
        LSR_soviet_military_backing = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_soviet_military_backing" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = generic_communist_army
            modifier = {
                army_attack_factor = 0.05
                army_defence_factor = 0.05
		army_speed_factor = -0.20
                experience_loss_factor = 0.2
                planning_speed = -0.10
            }
        }
		
        LSR_soviet_military_backing_b = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_soviet_military_backing_b" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = generic_communist_army
            modifier = {
                army_attack_factor = 0.05
                army_defence_factor = 0.05
				army_speed_factor = -0.10
                experience_loss_factor = 0.1
                planning_speed = -0.05
            }
        }
		
        LSR_legacy_iskolat = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_legacy_iskolat" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = jap_the_unthinkable_option
            modifier = {
				stability_factor = 0.05
                political_power_factor = -0.10
            }
        }
		
        LSR_legacy_iskolat_b = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_legacy_iskolat_b" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = jap_the_unthinkable_option
            modifier = {
				stability_factor = 0.05
            }
        }
		
        LSR_legacy_iskolat_c = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_legacy_iskolat_c" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = jap_the_unthinkable_option
            modifier = {
				stability_factor = 0.08
                political_power_factor = 0.10
            }
        }
		
        LSR_fighting_latvia = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_fighting_latvia" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = generic_morale_bonus
            modifier = {
				surrender_limit = 1
            }
        }
		
        LSR_fighting_latvia_b = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LSR_fighting_latvia_b" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = generic_morale_bonus
            modifier = {
				surrender_limit = 1
            }
        }
    }
	### Ministers ###
	head_of_government = {
        LSR_peteris_stucka = {
            allowed = { original_tag = LSR }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea LSR_peteris_stucka"
			}
			picture = LSR_peteris_stucka
			traits = {
				leninist_m
				head_of_government_trait
			}
			#cancel_if_invalid = yes Default
		}
	}
    foreign_minister = {
        LSR_karl_peterson = {
            allowed = { original_tag = LSR }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea LSR_karl_peterson"
			}
			picture = LSR_karl_peterson
			traits = {
				leninist_m
				foreign_minister_trait
			}
			#cancel_if_invalid = yes Default
        }
	}
	economy_minister = {
        LSR_rudolf_endrups = {
            allowed = { original_tag = LSR }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea LSR_rudolf_endrups"
			}
			picture = LSR_rudolf_endrups
			traits = {
				leninist_m
				economy_minister_trait
			}
			#cancel_if_invalid = yes Default
		}
	}
    interior_minister = {
        LSR_janis_lencmanis = {
            allowed = { original_tag = LSR }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea LSR_janis_lencmanis"
			}
			picture = LSR_janis_lencmanis
			traits = {
				leninist_m
				interior_minister_trait
			}
			#cancel_if_invalid = yes Default
		}
	}
}
