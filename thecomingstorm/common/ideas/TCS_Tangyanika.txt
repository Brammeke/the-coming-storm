ideas = {
	### Ideas ###
    country = {
        TZN_tribal_army = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea TZN_tribal_army" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = ANA_tribal_factionalism
            modifier = {
                conscription_factor = 0.5
                army_attack_factor = -0.2
				army_speed_factor = 0.2
				army_org_factor = -0.15
				out_of_supply_factor = -1
            }
		}
        TZN_german_strategic_superiority_a = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea TZN_german_strategic_superiority_a" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = ger_revive_the_kaiserreich
			targeted_modifier = {
				tag = GER
				attack_bonus_against = -4.5
			}
		}
        TZN_german_strategic_superiority_b = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea TZN_german_strategic_superiority_b" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = ger_revive_the_kaiserreich
			targeted_modifier = {
				tag = GER
				attack_bonus_against = -3.0
			}
		}
        TZN_german_strategic_superiority_c = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea TZN_german_strategic_superiority_c" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = ger_revive_the_kaiserreich
			targeted_modifier = {
				tag = GER
				attack_bonus_against = -1.5
			}
		}
        TZN_fighting_home_soil = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea TZN_fighting_home_soil" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = generic_volunteer_expedition_bonus
            modifier = {
				surrender_limit = 1
				recon_factor = 0.25
				heat_attrition = -0.2
            }
		}
        TZN_guerrilla_tactics = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea TZN_fighting_home_soil" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = chi_war_of_resistance3
            modifier = {
				equipment_capture_factor = 0.65
				dig_in_speed_factor = 0.3
				org_loss_when_moving = -0.5
            }
		}
        TZN_fighting_liberation = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea TZN_fighting_liberation" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = air_support_focus
            modifier = {
				surrender_limit = 1
				army_defence_factor = 0.55
            }
		}
    }
	### Ministers ###
	head_of_government = {
	}
    foreign_minister = {
	}
	economy_minister = {
	}
    interior_minister = {
	}
}
