ideas = {

	### Ministers ###

	head_of_government = {
        AUS_Paul_Gautsch_von_Frankenthurn_hog = {
            allowed = { original_tag = AUS }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea AUS_Paul_Gautsch_von_Frankenthurn_hog"
			}
			picture = minister_AUS_Paul_Gautsch_von_Frankenthurn
			traits = {
				soc_conservative_m
				head_of_government_trait
			}
			#cancel_if_invalid = yes Default
        }
		
	}
	
	interior_minister = {
        AUS_Artur_von_Bylandt_Rheidt_int = {
            allowed = { original_tag = AUS }
			allowed_to_remove = {
				always = no
			}
            on_add = {
                log = "[GetDateText]: [Root.GetName]: add idea AUS_Artur_von_Bylandt_Rheidt_int"
            }
            picture = minister_AUS_Artur_Bylandt_Rheidt
            traits = {
                soc_conservative_m
                interior_minister_trait
            }
            #cancel_if_invalid = yes Default
        }
		
	}
	
	foreign_minister = {
		AUS_Agenor_Maria_Goluchowski_fm = {
            allowed = { original_tag = AUS }
			allowed_to_remove = {
				always = no
			}
            on_add = {
                log = "[GetDateText]: [Root.GetName]: add idea AUS_Agenor_Maria_Goluchowski_fm"
            }
            picture = minister_AUS_Agenor_Maria_Goluchowski
            traits = {
                soc_conservative_m
                foreign_minister_trait
            }
            #cancel_if_invalid = yes Default
        }
		
	}
	
	economy_minister = {
       AUS_Stephan_Burian_von_Rajecz_eco = {
            allowed = { original_tag = AUS }
			allowed_to_remove = {
				always = no
			}
            on_add = {
                log = "[GetDateText]: [Root.GetName]: add idea AUS_Stephan_Burian_von_Rajecz_eco"
            }
            picture = minister_AUS_Stephan_Burian_von_Rajecz
            traits = {
                soc_conservative_m
                economy_minister_trait
            }
            #cancel_if_invalid = yes Default 
        }
		
	}

	country = {
	    AUS_dual_monarchy = {
	        allowed = {
          	always = no
	        }
	        allowed_civil_war = {
	          always = no
	        }
	        removal_cost = -1
          picture = AUS_dual_monarchy.tga
	        modifier = {
	          stability_factor = +0.05
						political_power_factor = -0.10
	        }
	    }
			AUS_badeni_legacy = {
	        allowed = {
          	always = no
	        }
	        allowed_civil_war = {
	        	always = no
	        }
	        removal_cost = -1
          picture = AUS_badeni_legacy.tga
	        modifier = {
	        	stability_factor = -0.05
					political_power_factor = -0.20
	        }
	    }
			AUS_hungarian_army_question = {
	        allowed = {
          	always = no
	        }
	        allowed_civil_war = {
	        	always = no
	        }
	        removal_cost = -1
          picture = AUS_hungarian_army_question.tga
	        modifier = {
	        	political_power_factor = -0.15
						conscription = -0.01
						army_defence_factor = -0.1
						army_attack_factor = -0.1
	        }
	    }
			AUS_hungarian_army_question2 = {
	        allowed = {
          	always = no
	        }
	        allowed_civil_war = {
	        	always = no
	        }
	        removal_cost = -1
          picture = 1                # REPLACE THIS ICON #
	        modifier = {
						political_power_factor = -0.05
						conscription = -0.005
						army_defence_factor = -0.03
						army_attack_factor = -0.03
	        }
	    }
			AUS_hungarian_army_question3 = {
	        allowed = {
          	always = no
	        }
	        allowed_civil_war = {
	        	always = no
	        }
	        removal_cost = -1
          picture = 1                # REPLACE THIS ICON #
	        modifier = {
						political_power_factor = -0.05
						conscription = -0.1
						army_defence_factor = 0.1
						army_attack_factor = 0.1
	        }
	    }
			AUS_ethnic_tensions = {
	        allowed = {
          	always = no
	        }
	        allowed_civil_war = {
	        	always = no
	        }
	        removal_cost = -1
          picture = AUS_ethnic_tensions.tga
	        modifier = {
	        	stability_factor = -0.05
					political_power_factor = -0.05
	        }
	    }
			AUS_ethnic_tensions2 = {
	        allowed = {
          	always = no
	        }
	        allowed_civil_war = {
	        	always = no
	        }
	        removal_cost = -1
          picture = 1
	        modifier = {
	        	political_power_factor = -0.1
						war_support_factor = -0.5
						civilian_factory_use = 1
	        }
	    }
		
		AUS_Education_Act = {
	        allowed = {
          	always = no
	        }
	        allowed_civil_war = {
	        	always = no
	        }
	        removal_cost = -1
          picture = SWE_promote_reform
	        modifier = {
	        	research_speed_factor = 0.03
	        }
	    }
		
		AUS_Reverse_Noble_Magyarization = {
	        allowed = {
          	always = no
	        }
	        allowed_civil_war = {
	        	always = no
	        }
	        removal_cost = -1
          picture = ROM_king_carol_ii_hedonist
	        modifier = {
				political_power_factor = 0.05
	        	army_core_defence_factor = 0.03
	        }
	    }
	 }
}
