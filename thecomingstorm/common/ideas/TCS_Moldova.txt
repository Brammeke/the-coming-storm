
ideas = {

	country = {

		MOL_Sfatul_Tarii = {
		
		allowed = {
			original_tag = MOL
		}
		
		removal_cost = -1

		picture = MOL_Sfatul_Tarii
	
		modifier = {
				stability_factor = 0.15
				political_power_gain = 0.1
				army_core_defence_factor = 0.1
			}
		}

		MOL_Rural_Nation = {
		
			allowed = {
				original_tag = MOL
			}

			removal_cost = -1

			picture = MOL_Agarian_Nation
		
			modifier = {
				research_speed_factor = -0.05
				production_speed_buildings_factor = -0.15
				industrial_capacity_factory = -0.15
			}
		}

		MOL_Ukrainian_Minority = {
		
			allowed = {
				original_tag = MOL
			}

			removal_cost = -1

			picture = MOL_Ukranian_Minority
		
			modifier = {
				conscription_factor = -0.25
				political_power_gain = -0.25
			}
		}
	}

	head_of_government = {

		MOL_Pantelimon_Erhan = {
			allowed = {
				OR = {
					original_tag = MOL
				}
			}
			
			available = {
				always = yes
			}

			cancel_if_invalid = no
			removal_cost = -1
			picture = MOL_Pantelimon_Erhan

			traits = {
				head_of_government
			}
		}
	}

	foreign_minister = {

		MOL_Ion_Pelivan = {
			allowed = {
				OR = {
					original_tag = MOL
				}
			}
			
			available = {
				always = yes
			}

			cancel_if_invalid = no
			removal_cost = -1
			picture = MOL_Ion_Pelivan

			traits = {
				foreign_minister
			}
		}
	}

	economy_minister = {

		MOL_Teofil_Ioncu = {
			allowed = {
				OR = {
					original_tag = MOL
				}
			}
			
			available = {
				always = yes
			}

			cancel_if_invalid = no
			removal_cost = -1
			picture = MOL_Teofil_Ioncu

			traits = {
				economy_minister
			}
		}
	}

	interior_minister = {

		MOL_Gherman_Pantea = {
			allowed = {
				OR = {
					original_tag = ROM
				}
			}
			
			available = {
				always = yes
			}

			cancel_if_invalid = no
			removal_cost = -1
			picture = MOL_Gherman_Pantea

			traits = {
				security_minister
			}
		}
	}
}