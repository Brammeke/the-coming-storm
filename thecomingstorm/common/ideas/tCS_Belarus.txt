ideas = {
    ### Ideas ###
    # country = {
    # }
    ### Ministers ###
    head_of_government = {
        BLR_roman_skirmunt_hog = {
            allowed = { original_tag = BLR }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea BLR_roman_skirmunt_hog"
			}
			picture = BLR_roman_skirmunt
			traits = {
				auth_democrat_m
				head_of_government_trait
				#hog_political_protege
			}
			#cancel_if_invalid = yes Default
        }

        BLR_anton_luckievic_hog = {
            allowed = { original_tag = BLR }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea BLR_anton_luckievic_hog"
			}
			picture = BLR_anton_luckievic
			traits = {
				soc_democrat_m
				head_of_government_trait
				#hog_spiritual_leader
			}
			#cancel_if_invalid = yes Default
        }

		BLR_vaclau_lastouski_hog = {
            allowed = { original_tag = BLR }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea BLR_vaclau_lastouski_hog"
			}
			picture = BLR_vaclau_lastouski
			traits = {
				rev_socialist_m
				head_of_government_trait
				#hog_spiritual_leader
			}
			#cancel_if_invalid = yes Default
        }
    }
	foreign_minister = {
        BLR_anton_luckievic_for = {
            allowed = { original_tag = BLR }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea BLR_anton_luckievic_for"
			}
			picture = BLR_anton_luckievic
			traits = {
				soc_democrat_m
				foreign_minister_trait
				#for_world_patrician
			}
			#cancel_if_invalid = yes Default
        }
    }
    economy_minister = {
        BLR_vasil_zacharka = {
            allowed = { original_tag = BLR }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea BLR_vasil_zacharka"
			}
			picture = BLR_vasil_zacharka
			traits = {
				soc_liberal_m
				economy_minister_trait
				#eco_reformer
			}
			#cancel_if_invalid = yes Default
        }
		BLR_jafim_bialievic = {
            allowed = { original_tag = BLR }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea BLR_jafim_bialievic"
			}
			picture = BLR_jafim_bialievic
			traits = {
				soc_liberal_m
				economy_minister_trait
				#eco_reformer
			}
			#cancel_if_invalid = yes Default
        }
    }
    interior_minister = {
        BLR_jazep_varonka = {
            allowed = { original_tag = BLR }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea BLR_jazep_varonka"
			}
			picture = BLR_jazep_varonka
			traits = {
				soc_liberal_m
				interior_minister_trait
				#sec_man_of_the_people
			}
			#cancel_if_invalid = yes Default
        }

		BLR_kuzma_ciarescanka = {
            allowed = { original_tag = BLR }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea BLR_kuzma_ciarescanka"
			}
			picture = BLR_kuzma_ciarescanka
			traits = {
				rev_socialist_m
				interior_minister_trait
				#sec_man_of_the_people
			}
			#cancel_if_invalid = yes Default
        }
    }
}