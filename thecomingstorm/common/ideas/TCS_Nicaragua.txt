ideas = {
	country = {
		NIC_professional_military = {
			allowed = {
                always = no
            }
            allowed_civil_war = {
                #
            }
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea NIC_professional_military"
			}
            removal_cost = -1
            picture = NIC_professional_military
            modifier = {
				stability_factor = 0.05
				army_org_factor = 0.08
				planning_speed = 0.05
				defence = 0.05
				offence = 0.05
            }
		}
	}
}