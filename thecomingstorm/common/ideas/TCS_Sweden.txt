ideas = {
	### Ministers ###
	### Heads of Government ###
	head_of_government = {
		### Boström Government ###
		SWE_Erik_Bostrom = {
			allowed = { original_tag = SWE }
			available = {
				always = yes
			}
			allowed_to_remove = {
				always = no
			}
			visible = {
				NOT = { has_country_flag = Bostrom_gone }
			}
			picture = SWE_Erik_Bostrom
			traits = {
				lib_conservative_m
				head_of_government
			}
			#cancel_if_invalid = yes Default
		}
		### Ramstedt Government ###
		SWE_Johan_Ramstedt = {
			allowed = { original_tag = SWE }
			available = {
				always = yes
			}
			allowed_to_remove = {
				always = no
			}
			visible = {
				NOT = { has_country_flag = Johan_Ramstedt_dead }
			}
			picture = SWE_Johan_Ramstedt
			traits = {
				lib_conservative_m
				head_of_government
			}
			#cancel_if_invalid = yes Default
		}
		### Lundeberg Government ###
		SWE_Christian_Lundeberg = {
			allowed = { original_tag = SWE }
			available = {
				always = yes
			}
			allowed_to_remove = {
				always = no
			}
			visible = {
				NOT = { has_country_flag = Christian_Lundeberg_dead }
			}
			picture = SWE_Christian_Lundeberg
			traits = {
				soc_conservative_m
				head_of_government
			}
			#cancel_if_invalid = yes Default
		}
		### Staaff Government ###
		SWE_Karl_Staaff_hg = {
			allowed = { original_tag = SWE }
			available = {
				always = yes
			}
			allowed_to_remove = {
				always = no
			}
			visible = {
				NOT = { has_country_flag = Karl_Staaff_dead }
			}
			picture = SWE_Karl_Staaff
			traits = {
				soc_liberal_m
				head_of_government
			}
			#cancel_if_invalid = yes Default
		}
		### Lindman Government ###
		SWE_Arvid_Lindman = {
			allowed = { original_tag = SWE }
			available = {
				always = yes
			}
			allowed_to_remove = {
				always = no
			}
			visible = {
				NOT = { has_country_flag = Arvid_Lindman_dead }
			}
			picture = SWE_Arvid_Lindman
			traits = {
				soc_conservative_m
				head_of_government
				
			}
			#cancel_if_invalid = yes Default
		}
	}
	### Foreign Ministers ###
	foreign_minister = {
		### Ramstedt Government ###
	    SWE_August_Gyldenstolpe = {
		   allowed = { original_tag = SWE }
		   available = {
			   always = yes
		   }
		   allowed_to_remove = {
			always = no
			}
		   visible = {
			   NOT = { has_country_flag = August_Gyldenstolpe_dead }
		   }
		   picture = SWE_August_Gyldenstolpe
		   traits = {
			   soc_conservative_m
			   foreign_minister
		   }
		   #cancel_if_invalid = yes Default
	    }
	    ### Lundeberg Government ###
	    SWE_Fredrik_Wachtmeister = {
			allowed = { original_tag = SWE }
			available = {
				always = yes
			}
			allowed_to_remove = {
				always = no
			}
			visible = {
				NOT = { has_country_flag = Fredrik_Wachtmeister_dead }
			}
			picture = SWE_Fredrik_Wachtmeister
			traits = {
				soc_conservative_m
				foreign_minister
			}
			#cancel_if_invalid = yes Default
		}
		### Staaff Government ###
		SWE_Eric_Trolle = {
			allowed = { original_tag = SWE }
			available = {
				always = yes
			}
			allowed_to_remove = {
				always = no
			}
			visible = {
				NOT = { has_country_flag = Eric_Trolle_dead }
			}
			picture = SWE_Eric_Trolle
			traits = {
				soc_conservative_m
				foreign_minister
			}
			#cancel_if_invalid = yes Default
		}
	}
	### Economy ###
	economy_minister = {
		### Ramstedt Government ###
	    SWE_Ernst_Meyer = {
		   allowed = { original_tag = SWE }
		    available = {
			   always = yes
		    }
		   allowed_to_remove = {
				always = no
			}
		   visible = {
			   NOT = { has_country_flag = Ernst_Meyer_dead }
		   }
		   picture = SWE_Ernst_Meyer
		   traits = {
			soc_conservative_m
			economy_minister
		   }
		   #cancel_if_invalid = yes Default
	    }
	    ### Lundeberg & Staaff Government ###
	    SWE_Elof_Biesert = {
			allowed = { original_tag = SWE }
			available = {
				always = yes
			}
			allowed_to_remove = {
				always = no
			}
			visible = {
				NOT = { has_country_flag = Elof_Biesert_dead }
			}
			picture = SWE_Elof_Biesert
			traits = {
				soc_liberal_m
				economy_minister
			}
			#cancel_if_invalid = yes Default
		}
		### Staaff & Lindman Government ###
		SWE_Carl_Swartz = {
			allowed = { original_tag = SWE }
			available = {
				always = yes
			}
			allowed_to_remove = {
				always = no
			}
			visible = {
				NOT = { has_country_flag = Carl_Swartz_dead }
			}
			picture = SWE_Carl_Swartz
			traits = {
				soc_conservative_m
				economy_minister
			}
			#cancel_if_invalid = yes Default
		}
	}
	### Interior Ministers ###
	interior_minister = {
		### Ramstedt Government ###
	    SWE_Ossian_Berger = {
		   allowed = { original_tag = SWE }
		   available = {
			   always = yes
		    }
		   allowed_to_remove = {
			always = no
			}
		   visible = {
			   NOT = { has_country_flag = Ossian_Berger_dead }
		    }
		    picture = SWE_Ossian_Berger
		    traits = {
				lib_conservative_m
				interior_minister_trait
		    }
		    #cancel_if_invalid = yes Default
	    }
	    ### Lundeberg Government ###
	    SWE_Gustaf_Berg = {
			allowed = { original_tag = SWE }
			available = {
				always = yes
			}
			allowed_to_remove = {
				always = no
			}
			visible = {
				NOT = { has_country_flag = Gustaf_Berg_dead }
			}
			picture = SWE_Gustaf_Berg
			traits = {
				lib_conservative_m
				interior_minister_trait
			}
			#cancel_if_invalid = yes Default
		}
		### Staaff Government ###
		SWE_Karl_Staaff_sm = {
			allowed = { original_tag = SWE }
			available = {
				always = yes
			}
			allowed_to_remove = {
				always = no
			}
			visible = {
				NOT = { has_country_flag = Karl_Staaff_dead }
			}
			picture = SWE_Karl_Staaff
			traits = {
				soc_liberal_m
				interior_minister_trait
			}
			#cancel_if_invalid = yes Default
		}
		### Lindman Government ###
		SWE_Albert_Petersson = {
			allowed = { original_tag = SWE }
			available = {
				always = yes
			}
			allowed_to_remove = {
				always = no
			}
			visible = {
				NOT = { has_country_flag = Albert_Petersson_dead }
			}
			picture = SWE_Albert_Petersson
			traits = {
				soc_conservative_m
				interior_minister_trait
			}
			#cancel_if_invalid = yes Default
		}
	}


	### Companies ###
	weapons_manufacturer = {

		designer = yes

		SWE_bofors = {
			allowed = {
				original_tag = SWE
            }
            allowed_civil_war = {
                #
            }
            removal_cost = -1
			picture = SWE_bofors
			research_bonus = {
				artillery = 0.10
			}
			traits = {
				artillery_manufacturer
				infantry_equipment_manufacturer
			}
		}
		SWE_mauser = {
			allowed = {
				original_tag = SWE
            }
            allowed_civil_war = {
                #
            }
			cost = 100
			available = {
				has_completed_focus = SWE_invite_german_arms_manufacturer
				NOT = {
					has_war_with = GER
				}
			}
            removal_cost = -1
			picture = GER_mauser
			research_bonus = {
				#artillery = 0.10
				infantry_weapons = 0.10
			}
			traits = {
				#artillery_manufacturer
				infantry_equipment_manufacturer
			}
		}
	}
	industrial_concern = {
		SWE_skf = {
			allowed = {
				original_tag = SWE
			}
			available = {
				has_completed_focus = SWE_found_skf
			}
			picture = SWE_skf
			removal_cost = -1
			research_bonus = {
				industry = 0.10
				construction_tech = 0.05
			}
			traits = {
				industrial_manufacturer
			}
		}
	}



	country = {
		SWE_desire_for_sufferage = {
			picture = SWE_reform_desire

			allowed = {
				always = no
			}

			removal_cost = -1

			allowed_civil_war = {
				always = yes
			}

			modifier = {
				stability_factor = -0.2
				consumer_goods_factor = 0.15
			}
		}

		SWE_failing_union = {
			picture = SWE_failing_union

			allowed = {
				always = no
			}

			removal_cost = -1

			allowed_civil_war = {
				always = yes
			}

			modifier = {
				stability_factor = -0.3
				political_power_gain = -0.5
				army_org_factor = -0.15
			}
		}
		SWE_britain_trade_deal = {
			#picture = #cumcum

			allowed = {
				always = no
			}
			removal_cost = -1
			allowed_civil_war = {
				always = yes
			}
			targeted_modifier = {
				tag = ENG
				trade_opinion_factor = 0.1
				trade_cost_for_target_factor = -0.1
			}
			targeted_modifier = {
				tag = GER
				trade_cost_for_target_factor = 0.1
			}
		}
		SWE_germany_trade_deal ={ 
			#picture = #cumcum

			allowed = {
				always = no
			}
			removal_cost = -1
			allowed_civil_war = {
				always = yes
			}
			targeted_modifier = {
				tag = GER
				trade_opinion_factor = 0.1
				trade_cost_for_target_factor = -0.1
			}
			targeted_modifier = {
				tag = ENG
				trade_cost_for_target_factor = 0.1
			}
		}
		SWE_narvik_lease = { 
			#picture = #cumcum

			allowed = {
				always = no
			}
			removal_cost = -1
			allowed_civil_war = {
				always = yes
			}
			targeted_modifier = {
				tag = NOR
				trade_cost_for_target_factor = -0.2
			}
			modifier = {
				local_resources_factor = 0.10
			}
		}
		SWE_iron_diplomacy = {
			#picture = #cumcum
			allowed = {
				always = no
			}
			removal_cost = -1
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				trade_opinion_factor = 0.2
				local_resources_factor = 0.1
			}
		}
		SWE_railway_reform = {
			allowed = {
				always = no
			}
			removal_cost = -1
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				production_speed_infrastructure_factor = 0.1
			}
		}
		SWE_german_investment = {
			allowed = {
				GER = {
					has_opinion = {
						target = THIS
						value > 50
					}
				}
			}
			modifier = {
				production_speed_buildings_factor = 0.1
				consumer_goods_factor = -0.03
			}
		}
		SWE_german_influence = {
			allowed = {
				NOT = {
					has_war_with = GER
				}
			}
			targeted_modifier = {
				tag = GER
				trade_opinion_factor = 1
			}
			modifier = {
				consumer_goods_factor = -0.02
			}
		}



		### Monarch Power ###
		SWE_monarch_power = {
			picture = SWE_monarch_power_3
			
			allowed = {
				always = no
			}

			traits = {
				monarch_power_trait
			}
			
			removal_cost = -1

			allowed_civil_war = {
				always = yes
			}
			
			modifier = {
			}
		}
	}
}