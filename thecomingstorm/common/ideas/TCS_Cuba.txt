ideas = {
	country = {
		## Starting Ideas
		CUB_nation_wide_drought = {
			allowed = {
                always = no
            }
            allowed_civil_war = {
                #
            }
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea CUB_nation_wide_drought"
			}
            removal_cost = -1
            picture = CUB_nation_wide_drought
            modifier = {
				stability_factor = -0.05
				consumer_goods_factor = 0.1
            }
		}
		CUB_platt_agreement = {
			allowed = {
                always = no
            }
            allowed_civil_war = {
                #
            }
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea CUB_platt_agreement"
			}
            removal_cost = -1
            picture = CUB_platt_agreement
            modifier = {
				stability_factor = -0.05
				political_power_gain = -1
            }
			targeted_modifier = {
				tag = USA
				consumer_goods_factor = 0.05
				cic_to_target_factor = 0.05
			}
		}
		## Event given Ideas
		CUB_american_occupation = {
			allowed = {
                always = no
            }
            allowed_civil_war = {
                #
            }
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea CUB_american_occupation"
			}
            removal_cost = -1
            picture = CUB_american_occupation
            modifier = {
				stability_factor = -0.10
				political_power_gain = -0.5
            }
		}
		CUB_little_war = {
			allowed = {
                always = no
            }
            allowed_civil_war = {
                #
            }
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea CUB_little_war"
			}
            removal_cost = -1
            picture = CUB_little_war
            modifier = {
				stability_factor = -0.50
				political_power_gain = -1
            }
		}
	}
}