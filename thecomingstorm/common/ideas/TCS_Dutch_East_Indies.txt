ideas = {
    ### Ideas ###
    # country = {
	# 	#
    # }
    ### Ministers ###
    head_of_government = {
		HOL_pieter_merkus_lambertus_de_bruyn_prince = {
            allowed = { original_tag = DEI }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea HOL_pieter_merkus_lambertus_de_bruyn_prince"
			}
			picture = HOL_pieter_merkus_lambertus_de_bruyn_prince
			traits = {
				lib_conservative_m
				head_of_government_trait
				#hog_spiritual_leader
			}
			#cancel_if_invalid = yes Default
		}
        HOL_colonial_control_hos = {
            allowed = { original_tag = DEI }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea HOL_colonial_control_hos"
			}
			picture = HOL_colonial_control
			traits = {
				pat_autocrat_m
				head_of_government_trait
				#hog_spiritual_leader
			}
			#cancel_if_invalid = yes Default
		}
    }
    foreign_minister = {
		HOL_colonial_control_fom = {
            allowed = { original_tag = DEI }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea HOL_colonial_control_fom"
			}
			picture = HOL_colonial_control
			traits = {
				pat_autocrat_m
				foreign_minister_trait
				#hog_spiritual_leader
			}
			#cancel_if_invalid = yes Default
		}
    }
    economy_minister = {
		HOL_colonial_control_fim = {
            allowed = { original_tag = DEI }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea HOL_colonial_control_fim"
			}
			picture = HOL_colonial_control
			traits = {
				pat_autocrat_m
				economy_minister_trait
				#hog_spiritual_leader
			}
			#cancel_if_invalid = yes Default
		}
    }
    interior_minister = {
		HOL_colonial_control_im = {
            allowed = { original_tag = DEI }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea HOL_colonial_control_im"
			}
			picture = HOL_colonial_control
			traits = {
				pat_autocrat_m
				interior_minister_trait
				#hog_spiritual_leader
			}
			#cancel_if_invalid = yes Default
		}
	}
	### Companies ###
}