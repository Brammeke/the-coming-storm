ideas = {
	country = {
		MAK_bandit_army = {
			allowed = {
                always = no
            }
            allowed_civil_war = {
                #
            }
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea MAK_bandit_army"
			}
            removal_cost = -1
            picture = MAK_bandit_army
            modifier = {
				surrender_limit = 1
				out_of_supply_factor = -0.5
				defence = 0.1
				army_org_factor = -0.25
            }
		}
		MAK_peasant_force = {
			allowed = {
                always = no
            }
            allowed_civil_war = {
                #
            }
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea MAK_peasant_force"
			}
            removal_cost = -1
            picture = MAK_peasant_force
            modifier = {
				non_core_manpower = 0.10
            }
		}
		MAK_mass_assembly_based_governance = {
			allowed = {
                always = no
            }
            allowed_civil_war = {
                #
            }
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea MAK_mass_assembly_based_governance"
			}
            removal_cost = -1
            picture = MAK_mass_assembly_based_governance
            modifier = {
				stability_factor = -0.1
				political_power_gain = 1.5
            }
		}
	}
}