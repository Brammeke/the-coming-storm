ideas = {

	country = {

		LUX_economic_union_with_germany = {

			picture = LUX_economic_union_with_germany


			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				political_power_factor = 0.03
				production_factory_max_efficiency_factor = 0.03
			}
			targeted_modifier = {
				tag = GER
				trade_cost_for_target_factor = -0.25
			}
			targeted_modifier = {
				tag = FRA
				trade_cost_for_target_factor = 0.25
			}
		}

		LUX_oligarchical_politics = {

			picture = generic_disjointed_gov


			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				war_support_factor = -0.10
				stability_factor = -0.05
				political_power_factor = 0.05
			}
		}

		LUX_schoulkampf_start = {

			picture = LUX_schoulkampf
			
			name = LUX_schoulkampf

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.08
				political_power_factor = -0.07
				research_speed_factor = -0.02
			}
		}

		LUX_schoulkampf = {

			picture = LUX_schoulkampf


			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.10
				political_power_factor = -0.09
				research_speed_factor = -0.04
			}
		}

		LUX_schoulkampf_1 = {

			picture = LUX_schoulkampf

			name = LUX_schoulkampf

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.05
				political_power_factor = -0.09
				research_speed_factor = -0.04
				production_factory_max_efficiency_factor = -0.05
			}
		}
		LUX_schoulkampf_2 = {

			picture = LUX_schoulkampf

			name = LUX_schoulkampf

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.03
				political_power_factor = -0.07
				research_speed_factor = -0.02
				production_factory_max_efficiency_factor = -0.03
			}
		}
		LUX_schoulkampf_3 = {

			picture = LUX_schoulkampf

			name = LUX_schoulkampf

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.01
				political_power_factor = -0.05
				research_speed_factor = -0.00
				production_factory_max_efficiency_factor = -0.01
			}
		}
		LUX_schoulkampf_4 = {

			picture = LUX_schoulkampf

			name = LUX_schoulkampf

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.00
				political_power_factor = -0.03
				research_speed_factor = -0.00
				production_factory_max_efficiency_factor = -0.00
			}
		}
		LUX_schoulkampf_5 = {

			picture = LUX_schoulkampf

			name = LUX_schoulkampf

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.00
				political_power_factor = -0.01
				research_speed_factor = -0.00
				production_factory_max_efficiency_factor = -0.00
			}
		}
		LUX_schoulkampf_1_league = {

			picture = LUX_schoulkampf

			name = LUX_schoulkampf

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.15
				political_power_factor = -0.09
				research_speed_factor = -0.04
				production_factory_max_efficiency_factor = 0.05
			}
		}
		LUX_schoulkampf_2_league = {

			picture = LUX_schoulkampf

			name = LUX_schoulkampf

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.14
				political_power_factor = -0.08
				research_speed_factor = -0.03
				production_factory_max_efficiency_factor = 0.05
			}
		}
		LUX_schoulkampf_3_league = {

			picture = LUX_schoulkampf

			name = LUX_schoulkampf

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.13
				political_power_factor = -0.07
				research_speed_factor = -0.02
				production_factory_max_efficiency_factor = 0.05
			}
		}
		LUX_schoulkampf_4_league = {

			picture = LUX_schoulkampf

			name = LUX_schoulkampf

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.12
				political_power_factor = -0.06
				research_speed_factor = -0.01
				production_factory_max_efficiency_factor = 0.05
			}
		}
		LUX_schoulkampf_5_league = {

			picture = LUX_schoulkampf

			name = LUX_schoulkampf

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.11
				political_power_factor = -0.06
				research_speed_factor = -0.00
				production_factory_max_efficiency_factor = 0.05
			}
		}

		LUX_secularized_schools = {

			#picture = LUX_schoulkampf


			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.03
				research_speed_factor = 0.03
			}
		}

		LUX_liberalized_schools = {

			#picture = LUX_schoulkampf


			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.02
				research_speed_factor = 0.01
			}
		}
	}

	head_of_government = {
		LUX_paul_eyschen = {
			allowed = { original_tag = LUX }
			allowed_to_remove = {
				always = no
			}

			picture = theodoros_diligiannis
			traits = {
				lib_conservative_m
				head_of_government_trait
			}
			#cancel_if_invalid = yes Default
		}
	}

	economy_minister = {
		LUX_mathias_mongenast = {
			allowed = { original_tag = LUX }
			allowed_to_remove = {
				always = no
			}
			picture = nikolaos_gounarakis
			traits = {
				lib_conservative_m
				economy_minister_trait
			}
			#cancel_if_invalid = yes Default
		}
	}

	foreign_minister = {
		LUX_paul_eyschen_foreign = {
            name = LUX_paul_eyschen
			allowed = { original_tag = LUX }
		   allowed_to_remove = {
			   always = no
		   }
		   picture = alexandros_skouzes
	   
		   traits = {
			   lib_conservative_m
			   foreign_minister_trait
		   }
		}
	}

	interior_minister = {
		LUX_auguste_laval = {
			allowed = { original_tag = LUX }
			allowed_to_remove = {
				always = no
			}
	   
			picture = theodoros_diligiannis
			traits = {
			   lib_conservative_m
			   interior_minister_trait
			}
			#cancel_if_invalid = yes Default
		}
	}
}