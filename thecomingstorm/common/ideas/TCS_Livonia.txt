ideas = {
	### Ideas ###
    country = {
        LIV_nation_on_last_legs = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea LIV_nation_on_last_legs" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = LIV_nation_on_last_legs
            modifier = {
				political_power_factor = -0.20
				stability_factor = -0.10
				war_support_factor = -0.10
				conscription_factor = -0.90
            }
		}
    }
	### Ministers ###
	head_of_government = {
	}
    foreign_minister = {
	}
	economy_minister = {
	}
    interior_minister = {
	}
}
