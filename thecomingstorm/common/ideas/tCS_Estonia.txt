ideas = {
    ### Ideas ###
    # country = {
    # }
    ### Ministers ###
    head_of_government = {
        EST_konstantin_pats_hog = {
            allowed = { original_tag = EST }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea EST_konstantin_pats_hog"
			}
			picture = EST_konstantin_pats
			traits = {
				auth_democrat_m
				head_of_government_trait
				#hog_spiritual_leader
			}
			#cancel_if_invalid = yes Default
        }
    }

    interior_minister = {
        EST_konstantin_pats_int = {
            allowed = { original_tag = EST }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea EST_konstantin_pats_int"
			}
			picture = EST_konstantin_pats
			traits = {
				auth_democrat_m
				interior_minister_trait
				#sec_man_of_the_people
			}
			#cancel_if_invalid = yes Default
        }
    }

	# chief_of_staff = {
    #     EST_jaan_soots = {
    #         allowed = { original_tag = EST }
	# 		allowed_to_remove = {
	# 			always = no
	# 		}
	# 		on_add = {
	# 			log = "[GetDateText]: [Root.GetName]: add idea EST_jaan_soots"
	# 		}
	# 		picture = EST_jaan_soots
	# 		traits = {
	# 			chief_of_staff
	# 			cos_school_of_fire_support
	# 		}
	# 		##cancel_if_invalid = yes Default
    #     }
    # }

    # army_chief = {
	# 	EST_johan_laidoner = {
	# 		allowed = { original_tag = EST }
	# 		# allowed_to_remove = {
	# 		# 	always = no
	# 		# }
	# 		on_add = {
	# 			log = "[GetDateText]: [Root.GetName]: add idea EST_johan_laidoner"
	# 		}
	# 		picture = EST_johan_laidoner
	# 		traits = {
	# 			army_chief
	# 			carm_doctrine_of_autonomy
	# 		}
	# 		##cancel_if_invalid = yes Default
	# 	}
	# }
}