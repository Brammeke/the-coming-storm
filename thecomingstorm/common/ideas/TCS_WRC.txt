ideas = {
	### Ideas ###
    country = {
        WRC_surprise_attack = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea WRC_surprise_attack" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = FRA_scw_intervention_republicans_focus
            modifier = {
                army_attack_factor = 0.10
				army_speed_factor = 0.10				
            }
		}
        WRC_mobile_army = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea WRC_mobile_army" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = prc_the_long_march2
            modifier = {
				out_of_supply_factor = -0.6
				org_loss_when_moving = -0.1			
            }
		}
        WRC_public_unrest = {
            on_add = { log = "[GetDateText]: [Root.GetName]: add idea WRC_public_unrest" }
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = no
            }
            removal_cost = -1
            picture = generic_disjointed_gov
            modifier = {
				political_power_cost = 0.20
				stability_weekly = -0.05
				war_support_weekly = -0.05
				conscription_factor = -0.50
            }
		}
    }
	### Ministers ###
	head_of_government = {
        WRC_ludwig_knorings = {
            allowed = { original_tag = WRC }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea WRC_ludwig_knorings"
			}
			picture = WRC_ludvigs_knorings
			traits = {
                pat_autocrat_m
				head_of_government_trait
			}
			#cancel_if_invalid = yes Default
		}
	}
    foreign_minister = {
        WRC_pavels_bermonts = {
            allowed = { original_tag = WRC }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea WRC_pavels_bermonts"
			}
			picture = WRC_pavel_bermondt
			traits = {
                pat_autocrat_m
				foreign_minister_trait
			}
			#cancel_if_invalid = yes Default
		}
	}
	economy_minister = {
        WRC_rudolf_engelhardt = {
            allowed = { original_tag = WRC }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea WRC_rudolf_engelhardt"
			}
			picture = WRC_rudolf_von_engelhardt
			traits = {
                pat_autocrat_m
				economy_minister_trait
			}
			#cancel_if_invalid = yes Default
		}
	}
    interior_minister = {
        WRC_aleksandr_korsakov = {
            allowed = { original_tag = WRC }
			allowed_to_remove = {
				always = no
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea WRC_aleksandr_korsakov"
			}
			picture = WRC_aleksandr_korsakov
			traits = {
                pat_autocrat_m
				interior_minister_trait
			}
			#cancel_if_invalid = yes Default
		}
	}
}