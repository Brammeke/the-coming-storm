# ###MILITARY AI###

division_limiter = {
	enable = {
		has_reached_maximum_divisions = yes
	}
	
	abort_when_not_enabled = yes

	ai_strategy = {
		type = build_army
		id = paratroopers
		value = -1000
	}

	ai_strategy = {
		type = build_army
		id = anti_tank
		value = -1000
	}

	ai_strategy = {
		type = build_army
		id = marine
		value = -1000
	}

	ai_strategy = {
		type = build_army
		id = mobile
		value = -1000
	}

	ai_strategy = {
		type = build_army
		id = armor
		value = -1000
	}

	ai_strategy = {
		type = build_army
		id = mountaineer
		value = -1000
	}

	ai_strategy = {
		type = build_army
		id = bicycle
		value = -1000
	}

	ai_strategy = {
		type = build_army
		id = garrison
		value = -1000
	}

	ai_strategy = {
		type = build_army
		id = militia
		value = -1000
	}

	ai_strategy = {
		type = build_army
		id = cavalry
		value = -1000
	}

	ai_strategy = {
		type = build_army
		id = infantry
		value = -1000
	}
}

# default_unit_production = {
# 	enable = {
# 		always = yes
# 	}

# 	ai_strategy = {
# 		type = role_ratio
# 		id = paratroopers
# 		value = 0
# 	}

# 	ai_strategy = {
# 		type = role_ratio
# 		id = anti_tank
# 		value = 0
# 	}

# 	ai_strategy = {
# 		type = role_ratio
# 		id = marine
# 		value = 0
# 	}

# 	ai_strategy = {
# 		type = role_ratio
# 		id = mobile
# 		value = 0
# 	}

# 	ai_strategy = {
# 		type = role_ratio
# 		id = armor
# 		value = 0
# 	}

# 	ai_strategy = {
# 		type = role_ratio
# 		id = mountaineer
# 		value = 0
# 	}

# 	ai_strategy = {
# 		type = role_ratio
# 		id = bicycle
# 		value = 0
# 	}

# 	ai_strategy = {
# 		type = role_ratio
# 		id = garrison
# 		value = 0
# 	}

# 	ai_strategy = {
# 		type = role_ratio
# 		id = militia
# 		value = 10
# 	}

# 	ai_strategy = {
# 		type = role_ratio
# 		id = cavalry
# 		value = 10
# 	}

# 	ai_strategy = {
# 		type = role_ratio
# 		id = infantry
# 		value = 80
# 	}

# 	#Air unit factors
# 	ai_strategy = {
# 		type = unit_ratio
# 		id = fighter
# 		value = 75
# 	}

# 	ai_strategy = {
# 		type = unit_ratio
# 		id = cas
# 		value = 10
# 	}

# 	ai_strategy = {
# 		type = unit_ratio
# 		id = tactical_bomber
# 		value = 10
# 	}

# 	ai_strategy = {
# 		type = unit_ratio
# 		id = strategic_bomber
# 		value = 0
# 	}

# 	ai_strategy = {
# 		type = unit_ratio
# 		id = naval_bomber
# 		value = 5
# 	}

# 	ai_strategy = {
# 		type = equipment_production_factor
# 		id = fighter
# 		value = 35
# 	}

# 	ai_strategy = {
# 		type = equipment_production_factor
# 		id = infantry
# 		value = 40
# 	}

# 	ai_strategy = {
# 		type = equipment_production_factor
# 		id = artillery
# 		value = 25
# 	}
# }