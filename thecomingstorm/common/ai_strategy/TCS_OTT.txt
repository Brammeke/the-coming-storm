# may be revised if balance issues arise
TCS_ai_ottoman_garrisons = {
	enable = {
		original_tag = OTT
		has_global_flag = OTT_first_balkan_war
	}
	abort_when_not_enabled = yes

	ai_strategy = {
		type = put_unit_buffers

		ratio = 0.4

		area = middle_east

		subtract_fronts_from_need = yes
	}

	ai_strategy = {
		type = front_unit_request

		area = europe

		value = -25
	}

	# ignore tripoli bruh
	ai_strategy = {
		type = front_unit_request
		area = africa
		value = -200
	}

	ai_strategy = {
  		type = garrison
   		value = 50
   	}
}

OTT_WW1_russia = {
	enable = {
		tag = OTT
		date > 1914.1.1
		has_war_with = RUS
	}

	abort_when_not_enabled = yes

	ai_strategy = {
        type = front_control
		
		tag = RUS

		priority = 1000 # default 0, higher prio strats will override lower
		ordertype = front # front or invasion. if set this strategy will only apply to that specific order type
		execution_type = careful # one of careful balanced rush rush_weak. if set will override the execution type of front (only for front orders)
		execute_order = yes # yes or no. if set will override execute or not decision of front
		manual_attack = yes # default yes. if no ai will not do manual pokes at enemy (only for front orders)
    }
}

OTT_WW1_england = {
	enable = {
		tag = OTT
		date > 1914.1.1
		has_war_with = ENG
	}

	abort_when_not_enabled = yes

	ai_strategy = {
        type = front_control
		
		tag = ENG

		priority = 1000 # default 0, higher prio strats will override lower
		ordertype = front # front or invasion. if set this strategy will only apply to that specific order type
		execution_type = careful # one of careful balanced rush rush_weak. if set will override the execution type of front (only for front orders)
		execute_order = yes # yes or no. if set will override execute or not decision of front
		manual_attack = yes # default yes. if no ai will not do manual pokes at enemy (only for front orders)
    }

	ai_strategy = {
        type = front_control
		
		tag = EGY

		priority = 1000 # default 0, higher prio strats will override lower
		ordertype = front # front or invasion. if set this strategy will only apply to that specific order type
		execution_type = careful # one of careful balanced rush rush_weak. if set will override the execution type of front (only for front orders)
		execute_order = yes # yes or no. if set will override execute or not decision of front
		manual_attack = yes # default yes. if no ai will not do manual pokes at enemy (only for front orders)
    }
}