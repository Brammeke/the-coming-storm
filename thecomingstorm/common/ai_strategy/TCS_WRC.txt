TCS_ai_wrc_latvian_war = {
	allowed = {
		original_tag = WRC
	}

	enable = {
		has_war_with = LAT
	}
	abort_when_not_enabled = yes
	
	ai_strategy = {
        type = front_control
		
        state = 890
		
		priority = 100
		ordertype = front
		execution_type = rush
		execute_order = yes
    }
	
	ai_strategy = {
        type = front_control
		
        state = 884
		
		priority = 100
		ordertype = front
		execution_type = rush
		execute_order = yes
    }
	
	ai_strategy = {
		type = front_unit_request

		area = europe
		
		value = 1000
	}
}