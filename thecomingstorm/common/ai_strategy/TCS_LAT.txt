TCS_ai_latvian_war_independence = {
	allowed = {
		original_tag = LAT
	}

	enable = {
		NOT = { has_completed_focus = LAT_Offensive_Option }
		has_war_with = LSR 
	}
	abort_when_not_enabled = yes
	
	ai_strategy = {
        type = front_control
		
		area = europe
		
		priority = 100
		ordertype = front
		execute_order = no
		manual_attack = no
    }
	
	ai_strategy = {
		type = front_unit_request

		area = europe
		
		value = 1000
	}
}

TCS_ai_latvian_war_independence_push_back = {
	allowed = {
		original_tag = LAT
	}

	enable = {
		has_completed_focus = LAT_Offensive_Option
		has_war_with = LSR 
	}
	abort_when_not_enabled = yes
	
	ai_strategy = {
        type = front_control
		
        state = 890
		
		priority = 100
		ordertype = front
		execution_type = rush
		execute_order = yes
    }
	
	ai_strategy = {
        type = front_control
		
        state = 190
		
		priority = 100
		ordertype = front
		execution_type = rush
		execute_order = yes
    }
	
	ai_strategy = {
        type = front_control
		
        state = 884
		
		priority = 100
		ordertype = front
		execution_type = rush
		execute_order = yes
    }
	
	ai_strategy = {
        type = front_control
		
        state = 12
		
		priority = 100
		ordertype = front
		execution_type = rush
		execute_order = yes
    }
	
	ai_strategy = {
		type = front_unit_request

		area = europe
		
		value = 2000
	}
}

TCS_ai_latvian_war_independence_wrc = {
	allowed = {
		original_tag = LAT
	}

	enable = {
		has_war_with = WRC
	}
	abort_when_not_enabled = yes
	
	ai_strategy = {
        type = front_control
		
        state = 190
		
		priority = 100
		ordertype = front
		execution_type = rush
		execute_order = yes
    }
	
	ai_strategy = {
        type = front_control
		
        state = 884
		
		priority = 100
		ordertype = front
		execution_type = rush
		execute_order = yes
    }
	
	ai_strategy = {
		type = front_unit_request

		area = europe
		
		value = 1000
	}
}