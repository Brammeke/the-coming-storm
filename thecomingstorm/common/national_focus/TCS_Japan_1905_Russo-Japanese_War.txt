##############################################
### tCS Japan 1905 Russo-Japanese War Tree ###
##############################################

#continuous_focus_position = { x = 50 y = 2000 }

shared_focus = {
	id = JAP_from_the_ashes_of_ryojun
	icon = GFX_goal_unknown
	x = 7
	y = 0
	cost = 0
	ai_will_do = { factor = 1 }
	available = {
		always = no
	}
	bypass = {
		controls_state = 745
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_borrow_more_money_from_britain
	icon = GFX_JAP_Borrow_Money_from_the_British
	prerequisite = { 
		focus = JAP_from_the_ashes_of_ryojun
	}
	x = 5
	y = 1
	cost = 2
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_yuki_no_shingun
	icon = GFX_JAP_Yuki_no_Shingun
	prerequisite = { 
		focus = JAP_from_the_ashes_of_ryojun
	}
	x = 7
	y = 1
	cost = 2
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_further_civilian_requisition
	icon = GFX_goal_unknown
	prerequisite = { 
		focus = JAP_from_the_ashes_of_ryojun
	}
	x = 9
	y = 1
	cost = 2
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_prepare_for_the_sekigahara
	icon = GFX_goal_unknown
	prerequisite = { 
		focus = JAP_borrow_more_money_from_britain
	}
	prerequisite = {
		focus = JAP_yuki_no_shingun
	}
	prerequisite = {
		focus = JAP_further_civilian_requisition
	}	
	x = 7
	y = 2
	cost = 2
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_pan_asian_propaganda
	icon = GFX_JAP_Pan-Asian_Propaganda
	prerequisite = { 
		focus = JAP_prepare_for_the_sekigahara
	}
	x = 4
	y = 3
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_diplomatic_support_from_the_west
	icon = GFX_JAP_Diplomatic_Support_from_the_West
	prerequisite = { 
		focus = JAP_prepare_for_the_sekigahara
	}
	x = 6
	y = 3
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_request_more_american_loan
	icon = GFX_JAP_American_Loans
	prerequisite = { 
		focus = JAP_prepare_for_the_sekigahara
	}
	x = 8
	y = 3
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_continue_the_emergency_taxation
	icon = GFX_goal_unknown
	prerequisite = { 
		focus = JAP_prepare_for_the_sekigahara
	}
	x = 10
	y = 3
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_nihonkai_kaisen
	icon = GFX_goal_unknown
	prerequisite = { 
		focus = JAP_pan_asian_propaganda
	}
	prerequisite = {
		focus = JAP_diplomatic_support_from_the_west
	}
	prerequisite = {
		focus = JAP_request_more_american_loan
	}
	prerequisite = {
		focus = JAP_continue_the_emergency_taxation
	}	
	x = 7
	y = 4
	cost = 2
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}

####################################################
### Russo-Japanese War - Japanese Victory Branch ###
####################################################

shared_focus = {
	id = JAP_get_ready_for_the_peace_conference
	icon = GFX_JAP_Get_Ready_for_the_Peace_Conference
	prerequisite = { 
		focus = JAP_nihonkai_kaisen
	}
	x = 3
	y = 5
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_victory_is_near
	icon = GFX_goal_unknown
	prerequisite = { 
		focus = JAP_nihonkai_kaisen
	}
	x = 5
	y = 5
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_taft_katsura_agreement
	icon = GFX_JAP_Taft-Katsura_Agreement
	prerequisite = { 
		focus = JAP_get_ready_for_the_peace_conference
	}
	prerequisite = {
		focus = JAP_victory_is_near
	}	
	x = 2
	y = 6
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_battle_for_karafuto
	icon = GFX_JAP_Battle_for_Karafuto
	prerequisite = { 
		focus = JAP_get_ready_for_the_peace_conference
	}
	prerequisite = {
		focus = JAP_victory_is_near
	}	
	x = 4
	y = 6
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_amend_the_anglo_japanese_alliance
	icon = GFX_JAP_Amend_the_Anglo-Japanese_Alliance
	prerequisite = { 
		focus = JAP_get_ready_for_the_peace_conference
	}
	prerequisite = {
		focus = JAP_victory_is_near
	}	
	x = 6
	y = 6
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_the_treaty_of_portsmouth
	icon = GFX_JAP_Treaty_of_Portsmouth
	prerequisite = { 
		focus = JAP_taft_katsura_agreement
	}
	prerequisite = {
		focus = JAP_battle_for_karafuto
	}
	prerequisite = {
		focus = JAP_amend_the_anglo_japanese_alliance
	}	
	x = 4
	y = 7
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		NOT = {
			has_war_with = RUS
		}	
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_sign_the_korea_protection_treaty
	icon = GFX_JAP_Sign_the_Korea_Protection_Treaty
	prerequisite = { 
		focus = JAP_the_treaty_of_portsmouth
	}
	x = 3
	y = 8
	cost = 8
	ai_will_do = { factor = 1 }
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_disband_daihonei
	icon = GFX_JAP_Disband_Daihonei
	prerequisite = { 
		focus = JAP_the_treaty_of_portsmouth
	}
	x = 5
	y = 8
	cost = 8
	ai_will_do = { factor = 1 }
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}

###################################################
### Russo-Japanese War - Russian Victory Branch ###
###################################################

shared_focus = {
	id = JAP_declare_martial_law
	icon = GFX_JAP_Declare_Martial_Law
	prerequisite = { 
		focus = JAP_nihonkai_kaisen
	}
	x = 9
	y = 5
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_pacify_the_citizens
	icon = GFX_JAP_Pacify_the_Citizens
	prerequisite = { 
		focus = JAP_nihonkai_kaisen
	}
	x = 11
	y = 5
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_leash_made_of_debt
	icon = GFX_JAP_Japan_under_Leash
	prerequisite = { 
		focus = JAP_declare_martial_law
	}
	prerequisite = {
		focus = JAP_pacify_the_citizens
	}
	x = 8
	y = 6
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_beg_for_the_peace_conference
	icon = GFX_JAP_Beg_for_the_Peace_Conference
	prerequisite = { 
		focus = JAP_declare_martial_law
	}
	prerequisite = {
		focus = JAP_pacify_the_citizens
	}
	x = 10
	y = 6
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_the_final_attempt
	icon = GFX_JAP_Battle_for_Karafuto
	prerequisite = { 
		focus = JAP_declare_martial_law
	}
	prerequisite = {
		focus = JAP_pacify_the_citizens
	}
	x = 12
	y = 6
	cost = 3
	ai_will_do = { factor = 1 }
	available = { 
		has_war_with = RUS
	}		
	bypass = {
		NOT = {
			has_war_with = RUS
		}
	}
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_the_treaty_of_paris
	icon = GFX_JAP_Treaty_of_Paris
	prerequisite = { 
		focus = JAP_leash_made_of_debt
	}
	prerequisite = {
		focus = JAP_beg_for_the_peace_conference
	}
	prerequisite = {
		focus = JAP_the_final_attempt
	}	
	x = 10
	y = 7
	cost = 2
	ai_will_do = { factor = 1 }
	available = {
		NOT = {
			has_war_with = RUS
		}	
	}		
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_maintain_the_military_control
	icon = GFX_goal_unknown
	prerequisite = { 
		focus = JAP_the_treaty_of_paris
	}
	x = 9
	y = 8
	cost = 8
	ai_will_do = { factor = 1 }		
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}
shared_focus = {
	id = JAP_the_korean_problem
	icon = GFX_JAP_The_Korean_Problem
	prerequisite = { 
		focus = JAP_the_treaty_of_paris
	}
	x = 11
	y = 8
	cost = 8
	ai_will_do = { factor = 1 }		
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}

shared_focus = {
	id = JAP_open_the_keien_jidai
	icon = GFX_JAP_Open_the_Keien_Jidai
	prerequisite = {
		focus = JAP_sign_the_korea_protection_treaty
		focus = JAP_the_korean_problem
	}
	prerequisite = {
		focus = JAP_disband_daihonei
		focus = JAP_maintain_the_military_control
	}	
	x = 7
	y = 9
	cost = 1
	ai_will_do = { factor = 1 }		
	cancel_if_invalid = yes
	continue_if_invalid = no
	available_if_capitulated = no
}