focus_tree = {
	id = TCS_Netherlands_1905_Start
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = HOL
		}
	}
	default = no
	shared_focus = HOL_luchtvaart_commissie
	shared_focus = HOL_koloniale_rijk
	focus = {
		id = HOL_kabinet_kuyper
		icon = GFX_kuypertime
		cost = 2
		x = 3
		y = 0
		completion_reward = {
			custom_effect_tooltip = HOL_gain_10_pp_tt
			hidden_effect = {
				add_political_power = 10
			}
		}
	}
	focus = {
		id = HOL_onderwijswetten
		icon = GFX_onderwijswetten
		cost = 2
		prerequisite = {
			focus = HOL_kabinet_kuyper
		}
		relative_position_id = HOL_kabinet_kuyper
		available = {
			custom_trigger_tooltip = {
				tooltip = HOL_enough_support_ek_tt
				check_variable = {
					var = eerste_kamer_support
					value = 25
					compare = greater_than
				}
			}
			custom_trigger_tooltip = {
				tooltip = HOL_enough_support_tk_tt
				check_variable = {
					var = tweede_kamer_support
					value = 50
					compare = greater_than
				}
			}
		}
		completion_reward = {
			custom_effect_tooltip = HOL_onderwijswetten_tt
			hidden_effect = {
				add_political_power = -20
				set_country_flag = HOL_onderwijswetten_subsidies
				set_country_flag = HOL_onderwijswetten_salaries
				set_country_flag = HOL_onderwijswetten_christian_primary_schools
				set_country_flag = HOL_onderwijswetten_christian_gymnasia
			}
		}
		x = -2
		y = 1

	}
	focus = {
		id = HOL_increased_subsidies
		icon = GFX_school_subsidies
		cost = 2
		prerequisite = {
			focus = HOL_onderwijswetten
		}
		relative_position_id = HOL_kabinet_kuyper
		x = -3
		y = 2
		available = {
			custom_trigger_tooltip = {
				tooltip = HOL_bypass_when_law_tt
				always = no
			}
		}
		completion_reward = {
			custom_effect_tooltip = HOL_gain_5_pp_tt
			hidden_effect = {
				add_political_power = 5
			}
		}
	}
	focus = {
		id = HOL_increased_salaries
		icon = GFX_higher_salary
		cost = 2
		prerequisite = {
			focus = HOL_onderwijswetten
		}
		relative_position_id = HOL_kabinet_kuyper
		x = -1
		y = 2
		available = {
			custom_trigger_tooltip = {
				tooltip = HOL_bypass_when_law_tt
				always = no
			}
		}
		completion_reward = {
			custom_effect_tooltip = HOL_gain_5_pp_tt
			hidden_effect = {
				add_political_power = 5
			}
		}
	}
	focus = {
		id = HOL_christ_basisschool
		icon = GFX_christian_school
		cost = 2
		prerequisite = {
			focus = HOL_increased_salaries
		}
		prerequisite = {
			focus = HOL_increased_subsidies
		}
		relative_position_id = HOL_kabinet_kuyper
		x = -2
		y = 3
		available = {
			custom_trigger_tooltip = {
				tooltip = HOL_bypass_when_law_tt
				always = no
			}
		}
		completion_reward = {
			custom_effect_tooltip = HOL_gain_5_pp_tt
			hidden_effect = {
				add_political_power = 5
			}
		}
	}
	focus = {
		id = HOL_christ_gymnasia
		icon = GFX_christian_gymnasium
		cost = 2
		prerequisite = {
			focus = HOL_christ_basisschool
		}
		relative_position_id = HOL_kabinet_kuyper
		x = -2
		y = 4
		available = {
			custom_trigger_tooltip = {
				tooltip = HOL_bypass_when_law_tt
				always = no
			}
		}
		completion_reward = {
			custom_effect_tooltip = HOL_gain_5_pp_tt
			hidden_effect = {
				add_political_power = 5
			}
		}
	}
	focus = {
		id = HOL_rijtuigenwet
		icon = GFX_motor_rijtuigenwet
		cost = 2
		prerequisite = {
			focus = HOL_kabinet_kuyper
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = HOL_enough_support_ek_tt
				check_variable = {
					var = eerste_kamer_support
					value = 25
					compare = greater_than
				}
			}
			custom_trigger_tooltip = {
				tooltip = HOL_enough_support_tk_tt
				check_variable = {
					var = tweede_kamer_support
					value = 50
					compare = greater_than
				}
			}
		}
		completion_reward = {
			custom_effect_tooltip = HOL_rijtuigenwet_tt
			hidden_effect = {
				add_political_power = -20
				set_country_flag = HOL_rijtuigenwet_maximum_speed
				set_country_flag = HOL_rijtuigenwet_driving_age
				set_country_flag = HOL_rijtuigenwet_traffic_signs
				set_country_flag = HOL_rijtuigenwet_traffic_fines
				set_country_flag = HOL_rijtuigenwet_streetraces
			}
		}
		relative_position_id = HOL_kabinet_kuyper
		x = 2
		y = 1

	}
	focus = {
		id = HOL_maximum_speed
		icon = GFX_maximum_speed
		cost = 2
		prerequisite = {
			focus = HOL_rijtuigenwet
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = HOL_bypass_when_law_tt
				always = no
			}
		}
		completion_reward = {
			custom_effect_tooltip = HOL_gain_5_pp_tt
			hidden_effect = {
				add_political_power = 5
			}
		}
		relative_position_id = HOL_kabinet_kuyper
		x = 1
		y = 2

	}
	focus = {
		id = HOL_driving_age
		icon = GFX_driving_age
		cost = 2
		prerequisite = {
			focus = HOL_maximum_speed
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = HOL_bypass_when_law_tt
				always = no
			}
		}
		completion_reward = {
			custom_effect_tooltip = HOL_gain_5_pp_tt
			hidden_effect = {
				add_political_power = 5
			}
		}
		relative_position_id = HOL_kabinet_kuyper
		x = 1
		y = 3

	}
	focus = {
		id = HOL_traffic_signs
		icon = GFX_verkeersborden
		cost = 2
		prerequisite = {
			focus = HOL_rijtuigenwet
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = HOL_bypass_when_law_tt
				always = no
			}
		}
		completion_reward = {
			custom_effect_tooltip = HOL_gain_5_pp_tt
			hidden_effect = {
				add_political_power = 5
			}
		}
		relative_position_id = HOL_kabinet_kuyper
		x = 3
		y = 2

	}
	focus = {
		id = HOL_traffic_fines
		icon = GFX_traffic_fines
		cost = 2
		prerequisite = {
			focus = HOL_traffic_signs
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = HOL_bypass_when_law_tt
				always = no
			}
		}
		completion_reward = {
			custom_effect_tooltip = HOL_gain_5_pp_tt
			hidden_effect = {
				add_political_power = 5
			}
		}
		relative_position_id = HOL_kabinet_kuyper
		x = 3
		y = 3

	}
	focus = {
		id = HOL_streetraces
		icon = GFX_ban_streetraces
		cost = 2
		prerequisite = {
			focus = HOL_driving_age
		}
		prerequisite = {
			focus = HOL_traffic_fines
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = HOL_bypass_when_law_tt
				always = no
			}
		}
		completion_reward = {
			custom_effect_tooltip = HOL_gain_5_pp_tt
			hidden_effect = {
				add_political_power = 5
			}
		}
		relative_position_id = HOL_kabinet_kuyper
		x = 2
		y = 4

	}
	focus = {
		id = HOL_loterijwet
		icon = GFX_loterijwet
		cost = 2
		prerequisite = {
			focus = HOL_streetraces
		}
		prerequisite = {
			focus = HOL_christ_gymnasia
		}
		relative_position_id = HOL_kabinet_kuyper
		x = 0
		y = 5
		available = {
			custom_trigger_tooltip = {
				tooltip = HOL_enough_support_ek_tt
				check_variable = {
					var = eerste_kamer_support
					value = 25
					compare = greater_than
				}
			}
			custom_trigger_tooltip = {
				tooltip = HOL_enough_support_tk_tt
				check_variable = {
					var = tweede_kamer_support
					value = 50
					compare = greater_than
				}
			}
		}
		completion_reward = {
			custom_effect_tooltip = HOL_loterijwet_tt
			hidden_effect = {
				add_political_power = -20
			}
		}
	}
}
