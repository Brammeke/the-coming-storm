### search_filters = {FOCUS_FILTER_POLITICAL}
### search_filters = {FOCUS_FILTER_RESEARCH}
### search_filters = {FOCUS_FILTER_INDUSTRY}
### search_filters = {FOCUS_FILTER_STABILITY}
### search_filters = {FOCUS_FILTER_WAR_SUPPORT}
### search_filters = {FOCUS_FILTER_MANPOWER}
### search_filters = {FOCUS_FILTER_ANNEXATION}
### search_filters = {FOCUS_FILTER_INTERNAL_AFFAIRS}

#!gfx:interface\KR_goals.gfx

## Political Russian Foci ## Brammeke ##

shared_focus = {
	id = RUS_first_russian_duma
	icon = GFX_RUS_russian_duma
	cost = 0
	x = 4
	y = 0
	available = {
		has_country_flag = RUS_october_manifesto
	}
	completion_reward = { }
	bypass = {
		has_country_flag = RUS_october_manifesto
	}
}

shared_focus = {
	id = RUS_fundamental_laws
	icon = GFX_GENERIC_law
	cost = 2.9
	x = 0
	y = 1
	prerequisite = {
		focus = RUS_first_russian_duma
	}
	completion_reward = {
		add_political_power = 20
		add_popularity = {
			ideology = pat_autocrat
			popularity = 0.05
		}
	}
	relative_position_id = RUS_first_russian_duma
}

shared_focus = {
	id = RUS_prisoner_amnesty
	icon = GFX_focus_generic_concessions
	cost = 2.9
	x = -1
	y = 2
	prerequisite = {
		focus = RUS_fundamental_laws
	}
	completion_reward = {
		country_event = { # Event for granting prisoner amnesty
			id = RUS_focus.1
			days = 1
			random_days = 2
		}
	}
	relative_position_id = RUS_first_russian_duma
}

shared_focus = {
	id = RUS_abolish_death_penalty
	icon = GFX_goal_generic_dangerous_deal
	cost = 2.9
	x = 1
	y = 2
	prerequisite = {
		focus = RUS_fundamental_laws
	}
	completion_reward = {
		country_event = { # Event for abolishing the death penalty
			id = RUS_focus.2
			days = 1
			random_days = 2
		}
	}
	relative_position_id = RUS_first_russian_duma
}

shared_focus = {
	id = RUS_land_for_peasantry
	icon = GFX_goal_generic_improve_relations
	cost = 2.9
	x = 0
	y = 3
	prerequisite = {
		focus = RUS_abolish_death_penalty
	}
	prerequisite = {
		focus = RUS_prisoner_amnesty
	}
	completion_reward = {
		country_event = { # Event for choosing if to grant Peasantry land
			id = RUS_focus.3
			days = 1
			random_days = 2
		}
	}
	relative_position_id = RUS_first_russian_duma
}

shared_focus = {
	id = RUS_dissolution_by_ukase
	icon = GFX_goal_generic_political_pressure
	cost = 0
	x = -3
	y = 1
	available = {
		has_country_flag = RUS_dissolved_duma
	}
	bypass = {
		has_country_flag = RUS_dissolved_duma
	}
	completion_reward = {
		#
	}
	relative_position_id = RUS_first_russian_duma
}

shared_focus = {
	id = RUS_trust_in_the_duma
	icon = GFX_GENERIC_parliament
	cost = 0
	x = 3
	y = 1
	available = {
		has_country_flag = RUS_trusted_duma
	}
	bypass = {
		has_country_flag = RUS_trusted_duma
	}
	completion_reward = {
		#
	}
	relative_position_id = RUS_first_russian_duma
}

shared_focus = {
	id = RUS_glory_to_the_tsar
	icon = GFX_focus_SOV_zemsky_sobor
	cost = 1
	x = 0
	y = 1
	prerequisite = {
		focus = RUS_dissolution_by_ukase
	}
	completion_reward = {
		add_political_power = 50
		add_popularity = {
			ideology = pat_autocrat
			popularity = 0.05
		}
	}
	relative_position_id = RUS_dissolution_by_ukase
}

shared_focus = {
	id = RUS_police_the_universities
	icon = GFX_focus_eng_concessions_to_the_trade_unions
	cost = 1
	x = -1
	y = 2
	prerequisite = {
		focus = RUS_glory_to_the_tsar
	}
	completion_reward = {
		add_stability = 0.1
		add_timed_idea = {
			idea = RUS_policed_universities
			days = 200
		}
	}
	relative_position_id = RUS_dissolution_by_ukase
}

shared_focus = {
	id = RUS_field_trials_for_terrorists
	icon = GFX_focus_generic_strike_at_democracy1
	cost = 1
	x = 1
	y = 2
	prerequisite = {
		focus = RUS_glory_to_the_tsar
	}
	completion_reward = {
		add_political_power = -50
		add_timed_idea = {
			idea = RUS_anti_terror_trials
			days = 200
		}
	}
	relative_position_id = RUS_dissolution_by_ukase
}

shared_focus = {
	id = RUS_arrest_vyborg_conspiritors
	icon = GFX_goal_generic_secret_weapon
	cost = 1
	x = 0
	y = 3
	prerequisite = {
		focus = RUS_field_trials_for_terrorists
	}
	prerequisite = {
		focus = RUS_police_the_universities
	}
	completion_reward = {
		# country_event = { # Event for new Elections (historical, Kadets banned)
		# 	id = RUS_political.?
		# 	days = 1
		# 	random_days = 2
		# }
	}
	relative_position_id = RUS_dissolution_by_ukase
}

shared_focus = {
	id = RUS_limit_the_tsar
	icon = GFX_focus_rom_abdicate
	cost = 1
	x = 0
	y = 1
	prerequisite = {
		focus = RUS_trust_in_the_duma
	}
	completion_reward = {
		add_popularity = {
			ideology = pat_autocrat
			popularity = -1
		}
		add_popularity = {
			ideology = soc_conservatism
			popularity = 0.15
		}
		set_politics = {
			ruling_party = soc_conservatism
		}
		hidden_effect = {
			add_popularity = {
				ideology = pat_autocrat
				popularity = 0.05
			}
		}
	}
	relative_position_id = RUS_trust_in_the_duma
}

shared_focus = {
	id = RUS_provisions_for_peasantry
	icon = GFX_goal_generic_national_unity
	cost = 1
	x = -1
	y = 2
	prerequisite = {
		focus = RUS_limit_the_tsar
	}
	completion_reward = {
		add_political_power = -10
		add_timed_idea = {
			idea = RUS_provisions_for_peasantry
			days = 200
		}
	}
	relative_position_id = RUS_trust_in_the_duma
}

shared_focus = {
	id = RUS_religious_and_political_freedom
	icon = GFX_focus_GRE_an_orthodox_state
	cost = 1
	x = 1
	y = 2
	prerequisite = {
		focus = RUS_limit_the_tsar
	}
	completion_reward = {
		country_event = { # The Church Intervenes
			id = RUS_focus.4
			days = 1
			random_days = 2
		}
	}
	relative_position_id = RUS_trust_in_the_duma
}

shared_focus = {
	id = RUS_peasants_in_the_upper_house
	icon = GFX_GENERIC_parliament
	cost = 1
	x = 0
	y = 3
	prerequisite = {
		focus = RUS_provisions_for_peasantry
	}
	prerequisite = {
		focus = RUS_religious_and_political_freedom
	}
	completion_reward = {
		# country_event = { # Event for new Elections (a-historical)
		# 	id = RUS_political.?
		# 	days = 1
		# 	random_days = 2
		# }
	}
	relative_position_id = RUS_trust_in_the_duma
}