focus_tree = {
	id = TCS_Montenegro_TPP_Kingdom
	country = {
		factor = 0
	}
	
	shared_focus = MNT_royal_conference_on_foreign_policy
	shared_focus = MNT_cultivate_clan_leader_loyalty
	shared_focus = MNT_connect_niksic_to_antivari
	shared_focus = MNT_army_systemized_regulations
}