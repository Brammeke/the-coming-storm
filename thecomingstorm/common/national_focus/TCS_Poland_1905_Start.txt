### search_filters = {FOCUS_FILTER_POLITICAL}
### search_filters = {FOCUS_FILTER_RESEARCH}
### search_filters = {FOCUS_FILTER_INDUSTRY}
### search_filters = {FOCUS_FILTER_STABILITY}
### search_filters = {FOCUS_FILTER_WAR_SUPPORT}
### search_filters = {FOCUS_FILTER_MANPOWER}

focus_tree = {
	id = TCS_Poland_1918_Start
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = POL
			date > 1918.11.11
			NOT = {
				has_cosmetic_tag = 1905_revolt_poland
			}
		}
	}
	default = no
	shared_focus = POL_decree_of_pko
	shared_focus = POL_decree_military_service
	continuous_focus_position = { x = 175 y = 1475 }

	focus = {
		id = POL_rebirth_of_poland
		icon = GFX_POL_rebirth_of_poland
		available = {
		    date > 1918.11.11
			has_country_leader = {
				character = POL_jozef_pilsudski 
				ruling_only = yes 
			}
	    }
		search_filters = {FOCUS_FILTER_POLITICAL}
		completion_reward = {
			add_political_power = 85
			POL = { country_event = { id = poland_1918.2 } }
		}

		cost = 1
		
		x = 5
		y = 0

	}

	focus = {
		id = POL_land_reform_and_partial_nationalisation_manifesto
		icon = GFX_POL_land_reform_and_partial_nationalisation_manifesto
		cost = 1.5
		prerequisite = {
			focus = POL_rebirth_of_poland
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 2
				is_historical_focus_on = yes
				has_completed_focus = POL_rebirth_of_poland
			}
		}
		search_filters = {FOCUS_FILTER_POLITICAL}
		completion_reward = {
			add_stability = 0.10
			unlock_decision_tooltip = POL_8_Hours_Work_Decree
		}
		x = 4
		y = 1

	}

	focus = {
		id = POL_Establishing_Chief_of_State_Office
		icon = GFX_POL_Establishing_Chief_of_State_Office
		cost = 1.5
		prerequisite = {
			focus = POL_rebirth_of_poland
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 2
				is_historical_focus_on = yes
				has_completed_focus = POL_land_reform_and_partial_nationalisation_manifesto
			}
		}
		search_filters = {FOCUS_FILTER_POLITICAL}
		completion_reward = {
			add_political_power = 80
			unlock_decision_tooltip = POL_Decree_on_elections_to_the_Sejm
		}
		x = 6
		y = 1

	}

	focus = {
		id = POL_Strengthening_State_administration
		icon = GFX_POL_Strengthening_State_administration
		cost = 1.6
		prerequisite = {
			focus = POL_Establishing_Chief_of_State_Office
		}
		prerequisite = {
			focus = POL_land_reform_and_partial_nationalisation_manifesto
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 2
				is_historical_focus_on = yes
				has_completed_focus = POL_Establishing_Chief_of_State_Office
			}
		}
		search_filters = {FOCUS_FILTER_POLITICAL}

		completion_reward = {
			add_stability = 0.02
			add_political_power = 50
			unlock_decision_tooltip = POL_Decree_on_mandatory_state_administration
			unlock_decision_tooltip = POL_Decree_on_temporary_housing_for_the_unemployed
		}
		x = 5
		y = 2

	}

	focus = {
		id = POL_Compromise_with_Opposition
		icon = GFX_focus_POL_Compromise_with_Opposition
		cost = 1.7
		prerequisite = {
			focus = POL_Strengthening_State_administration
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 2
				is_historical_focus_on = yes
				has_completed_focus = POL_Strengthening_State_administration
			}
		}
		search_filters = {FOCUS_FILTER_POLITICAL}
		
		completion_reward = {
			add_political_power = 25
			hidden_effect = {
                set_party_name = { ideology = leninism long_name = KPRP_POL_leninism_party_long name = KPRP_POL_leninism_party }
                set_party_name = { ideology = marxism long_name = KPRP_POL_marxism_party_long name = KPRP_POL_marxism_party }
                set_party_name = { ideology = rev_socialism long_name = PSLL_POL_rev_socialism_party_long name = PSLL_POL_rev_socialism_party }
				create_country_leader = {
	                name = "Jan Stapiński"
	                desc = "POLITICS_JAN_STAPINSKI_DESC"
	                picture = "Portrait_Poland_Jan_Stapinski.tga"
	                expire = "1965.1.1"
	                ideology = rev_socialism_subtype
	                traits = {}
	            }
            }
			load_focus_tree = {
                tree = POL_1918_Paderewski_Base
                keep_completed = yes
            }
		}
		x = 5
		y = 3

	}

}
