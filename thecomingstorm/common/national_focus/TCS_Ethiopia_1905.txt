# Ethiopia, 1905 starting tree

shared_focus = {
		id = ETH_request_accounting_nobles
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 5
		y = 0
		
	completion_reward = {
		add_political_power = 15
		country_event = {
			id = TCS_ethiopia.1
		}
    }
}

# Reorg. ter. bound.

shared_focus = {
		id = ETH_reorg_ter_bound
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 3
		y = 1
		
	completion_reward = {
		add_political_power = 10
		add_popularity = {
			ideology = auth_democrat
			popularity = 0.01
		}
    }
	prerequisite = { focus = ETH_request_accounting_nobles }
}

shared_focus = {
		id = ETH_1906_agreement
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 2
		y = 2
		
	completion_reward = {
		country_event = {
			id = TCS_ethiopia.2
		}
    }
	prerequisite = { focus = ETH_reorg_ter_bound }
}

shared_focus = {
		id = ETH_finalise_borders_france
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 2
		y = 3
		
	completion_reward = {
		country_event = {
			id = TCS_ethiopia.3
		}
    }
	prerequisite = { 
	focus = ETH_1906_agreement 
	}
	prerequisite = { 
	focus = ETH_incorporation_of_new_land 
	}
}

shared_focus = {
		id = ETH_border_treaty_ITA_maybe
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 2
		y = 4
		
	completion_reward = {
		country_event = {
			id = TCS_ethiopia.5
		}
    }
	prerequisite = { 
	focus = ETH_finalise_borders_france 
	}
	prerequisite = { 
	focus = ETH_finalise_borders_britain 
	}
}
#################################################################################################################################################
shared_focus = {
		id = ETH_incorporation_of_new_land
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 4
		y = 2
		
	completion_reward = {
		add_stability = 0.01
		add_manpower = 500
		add_political_power = 10
    }
	prerequisite = { focus = ETH_reorg_ter_bound }
}

shared_focus = {
		id =  ETH_finalise_borders_britain
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 4
		y = 3
		
	completion_reward = {
		country_event = {
			id = TCS_ethiopia.4
		}
    }
	prerequisite = { focus = ETH_incorporation_of_new_land }
	prerequisite = { focus = ETH_1906_agreement }
}

shared_focus = {
		id =  ETH_campaign_against_somalis
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 4
		y = 4
		
	completion_reward = {
		country_event = {
			id = TCS_ethiopia.8
		}
    }
	prerequisite = { focus = ETH_finalise_borders_france }
	prerequisite = { focus = ETH_finalise_borders_britain }
}

shared_focus = {
		id =  ETH_regular_border_patrols_crown_friendly_militia
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 3
		y = 5
		
	completion_reward = {
		add_political_power = 10
    }
	prerequisite = { focus = ETH_campaign_against_somalis }
	prerequisite = { focus = ETH_border_treaty_ITA_maybe }
}

shared_focus = {
		id =  ETH_lookout_posts_italian_border
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 2
		y = 6
		
	completion_reward = {
		271 = {
			add_building_construction = {
				type = bunker
				level = 1
				instant_build = yes
				province = {
					id = 4995
				}
			}
		}
    }
	prerequisite = { focus = ETH_regular_border_patrols_crown_friendly_militia }
	mutually_exclusive = { focus = ETH_form_customs_administration_eritrean_border }
}

shared_focus = {
		id =  ETH_form_customs_administration_eritrean_border
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 4
		y = 6
		
	completion_reward = {
		add_opinion_modifier = {
			target = FRA
			modifier = ETH_trade_relations_fra_eng
        }
		add_opinion_modifier = {
			target = ENG
			modifier = ETH_trade_relations_fra_eng
        }
    }
	prerequisite = { focus = ETH_regular_border_patrols_crown_friendly_militia }
	mutually_exclusive = { focus = ETH_lookout_posts_italian_border }
}

shared_focus = {
		id =  ETH_promote_territorial_sovreignty_among_aristocracy
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 3
		y = 7
		
	completion_reward = {
		ETH_feudalism_betterment = yes
    }
	prerequisite = {
	focus = ETH_lookout_posts_italian_border
	focus = ETH_form_customs_administration_eritrean_border
	}
}

# Convene The Royal Court #####################################################################################################

shared_focus = {
		id = ETH_convene_the_royal_court
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 8
		y = 1
		
	completion_reward = {
		add_political_power = 15
		add_popularity = {
			ideology = soc_conservatism
			popularity = 0.02
		}
		add_popularity = {
			ideology = pat_autocrat
			popularity = 0.02
		}
    }
	prerequisite = { focus = ETH_request_accounting_nobles }
}

shared_focus = {
		id = ETH_establishment_of_the_minister_cabinet
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 7
		y = 2
		
	completion_reward = {
		country_event = {
			id = TCS_ethiopia.11
		}
    }
	prerequisite = { focus = ETH_convene_the_royal_court }
}

shared_focus = {
		id = ETH_enforce_royal_control_gondar_trade
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 6
		y = 3
		
	completion_reward = {
		add_ideas = ETH_royal_control_of_the_gondar_trade
    }
	prerequisite = { focus = ETH_establishment_of_the_minister_cabinet }
}

shared_focus = {
		id = ETH_undercut_local_minor_nobility
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 6
		y = 4
		
	completion_reward = {
		ETH_meneliks_reign_change = yes
    }
	prerequisite = { focus = ETH_enforce_royal_control_gondar_trade }
}

shared_focus = {
		id = ETH_found_ethiopian_royal_postal_system
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 6
		y = 5
		
	completion_reward = {
		add_stability = 0.03
		add_political_power = 20
    }
	prerequisite = { focus = ETH_undercut_local_minor_nobility }
}
##
shared_focus = {
		id = ETH_judicial_reform
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 9
		y = 2
		
	completion_reward = {
		add_stability = 0.05
    }
	prerequisite = { focus = ETH_convene_the_royal_court }
}

shared_focus = {
		id = ETH_begin_supression_slave_trade
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 8
		y = 3
		
	completion_reward = {
		add_ideas = ETH_slave_trade_supressed
		add_stability = -0.02
    }
	prerequisite = { focus = ETH_judicial_reform }
	prerequisite = { focus = ETH_establishment_of_the_minister_cabinet }
}

shared_focus = {
		id = ETH_mil_campaigns_against_human_traffickers
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 8
		y = 4
		
	completion_reward = {
		ETH_feudalism_betterment = yes
    }
	prerequisite = { focus = ETH_begin_supression_slave_trade }
}

shared_focus = {
		id = ETH_allow_continued_corvee_labour
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 8
		y = 5
		
	completion_reward = {
		add_ideas = ETH_corvee_labour
    }
	prerequisite = { focus = ETH_mil_campaigns_against_human_traffickers }
}

shared_focus = {
		id = ETH_subordination_to_shewa
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 10
		y = 3
		
	completion_reward = {
		ETH_meneliks_reign_change = yes
		country_event = {
			id = TCS_ethiopia.12
		}
    }
	prerequisite = { focus = ETH_taxes_for_the_unruly }
	prerequisite = { focus = ETH_judicial_reform }
}

shared_focus = {
		id = ETH_create_transnational_royal_bureaucracy
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 10
		y = 4
		
	completion_reward = {
		add_popularity = {
			ideology = soc_conservatism
			popularity = 0.02
		}
		ETH_feudalism_betterment = yes
    }
	prerequisite = { focus = ETH_subordination_to_shewa }
}

shared_focus = {
		id = ETH_mollify_the_aristocracy
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 10
		y = 5
		
	completion_reward = {
		add_popularity = {
			ideology = pat_autocrat
			popularity = 0.02
		}
    }
	prerequisite = { focus = ETH_create_transnational_royal_bureaucracy }
}

shared_focus = {
		id = ETH_the_question_of_succession
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 8
		y = 6
		
	completion_reward = {
		country_event = {
			id = TCS_ethiopia.13
		}
    }
	prerequisite = { focus = ETH_mollify_the_aristocracy }
	prerequisite = { focus = ETH_found_ethiopian_royal_postal_system }
	prerequisite = { focus = ETH_allow_continued_corvee_labour }
}

################### Economic Reformation Advisory Board ###################

shared_focus = {
		id = ETH_economic_reformation_advisory_board
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 12
		y = 1
		
	completion_reward = {
		ETH_meneliks_reign_change = yes
    }
	prerequisite = { focus = ETH_request_accounting_nobles }
}

shared_focus = {
		id = ETH_taxes_for_the_unruly
		icon = GFX_grab_money_ottomans
		cost = 5
		x = 11
		y = 2
		
	completion_reward = {
		add_stability = 0.01
		add_popularity = {
			ideology = pat_autocrat
			popularity = 0.01
		}
    }
	prerequisite = { focus = ETH_economic_reformation_advisory_board }
}

shared_focus = {
		id = ETH_bank_of_abyssinia
		icon = GFX_goal_FRA_exceptional_circumstances_act
		cost = 5
		x = 14
		y = 2
		
	completion_reward = {
		country_event = {
			id = TCS_ethiopia.14
		}
    }
	prerequisite = { focus = ETH_economic_reformation_advisory_board }
}

shared_focus = {
		id = ETH_national_banknotes
		icon = GFX_new_taxes
		cost = 5
		x = 14
		y = 3
		
	completion_reward = {
		ETH_bank_of_abyssinia_change = yes
		add_political_power = 30
    }
	prerequisite = { focus = ETH_bank_of_abyssinia }
}

shared_focus = {
		id = ETH_partner_with_a_credible_lender
		icon = GFX_promote_clientilism
		cost = 5
		x = 13
		y = 4
		
	completion_reward = {
		country_event = {
			id = TCS_ethiopia.15
		}
    }
	prerequisite = { focus = ETH_national_banknotes }
}

shared_focus = {
		id = ETH_import_substitution_rights
		icon = GFX_limit_the_tax_burden
		cost = 5
		x = 15
		y = 4
		
	completion_reward = {
		add_ideas = ETH_import_substitution_rights
    }
	prerequisite = { focus = ETH_national_banknotes }
}

shared_focus = {
		id = ETH_credit_network_for_local_entreprenuers
		icon = GFX_goal_generic_free_trade
		cost = 5
		x = 14
		y = 5
		
	completion_reward = {
		ETH_bank_of_abyssinia_change = yes
    }
	prerequisite = { focus = ETH_import_substitution_rights }
	prerequisite = { focus = ETH_partner_with_a_credible_lender }
}

shared_focus = {
		id = ETH_american_rubber_tree_farming_companies
		icon = GFX_GER_rubber
		cost = 5
		x = 11
		y = 6
		
	completion_reward = {
		add_resource = {
			type = rubber
			amount = 1
			state = 271
		}
    }
	prerequisite = { focus = ETH_credit_network_for_local_entreprenuers }
}

shared_focus = {
		id = ETH_german_ethiopian_imperial_copper_mining_company
		icon = GFX_focus_generic_steel
		cost = 5
		x = 13
		y = 6
		
	completion_reward = {
		add_tech_bonus = {
			bonus = 0.2
			uses = 1
			category = industry
		}
    }
	prerequisite = { focus = ETH_credit_network_for_local_entreprenuers }
}

shared_focus = {
		id = ETH_ivory_trade_with_england_and_france
		icon = GFX_generic_ivory_trade
		cost = 5
		x = 15
		y = 6
		
	completion_reward = {
		ETH_bank_of_abyssinia_change = yes
    }
	prerequisite = { focus = ETH_credit_network_for_local_entreprenuers }
}

shared_focus = {
		id = ETH_russian_comercial_gold_mining_investments
		icon = GFX_Gold
		cost = 5
		x = 17
		y = 6
		
	completion_reward = {
		add_tech_bonus = {
			bonus = 0.2
			uses = 1
			category = electronics
		}
    }
	prerequisite = { focus = ETH_credit_network_for_local_entreprenuers }
}

shared_focus = {
		id = ETH_create_european_resources_trade_board
		icon = GFX_generic_european_resources
		cost = 5
		x = 12
		y = 7
		
	completion_reward = {
		add_resource = {
			type = rubber
			amount = 1
			state = 271
		}
    }
	prerequisite = { focus = ETH_american_rubber_tree_farming_companies }
	prerequisite = { focus = ETH_german_ethiopian_imperial_copper_mining_company }
	prerequisite = { focus = ETH_ivory_trade_with_england_and_france }
	prerequisite = { focus = ETH_russian_comercial_gold_mining_investments }
	
	mutually_exclusive = { focus = ETH_reinvest_gains_in_local_goods_and_workshops }
}

shared_focus = {
		id = ETH_reinvest_gains_in_local_goods_and_workshops
		icon = GFX_generic_reinvest_goods
		cost = 5
		x = 16
		y = 7
		
	completion_reward = {
		ETH_bank_of_abyssinia_change = yes
    }
	prerequisite = { focus = ETH_american_rubber_tree_farming_companies }
	prerequisite = { focus = ETH_german_ethiopian_imperial_copper_mining_company }
	prerequisite = { focus = ETH_ivory_trade_with_england_and_france }
	prerequisite = { focus = ETH_russian_comercial_gold_mining_investments }
	
	mutually_exclusive = { focus = ETH_create_european_resources_trade_board }
}

shared_focus = {
		id = ETH_fund_royal_tea_plantations
		icon = GFX_generic_tea
		cost = 5
		x = 14
		y = 8
		
	completion_reward = {
		add_opinion_modifier = { target = ENG modifier = ETH_lender_help_trade }
		add_opinion_modifier = { target = RUS modifier = ETH_lender_help_trade }
		add_opinion_modifier = { target = GER modifier = ETH_lender_help_trade }
    }
	
	prerequisite = { 
	focus = ETH_create_european_resources_trade_board 
	focus = ETH_reinvest_gains_in_local_goods_and_workshops 
	}
}

shared_focus = {
		id = ETH_first_step_towards_domestic_cottage_industry
		icon = GFX_focus_generic_treaty
		cost = 5
		x = 14
		y = 9
		
	completion_reward = {
		random_owned_state = {
			add_building_construction = {
				type = industrial_complex
				level = 2
				instant_build = yes
			}
		}
    }
	
	prerequisite = { focus = ETH_fund_royal_tea_plantations }
}

########## ETH_begin_famine_recovery_program ##########

shared_focus = {
		id = ETH_begin_famine_recovery_program
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 17
		y = 1
		
	completion_reward = {
		modify_timed_idea = {
			idea = ETH_great_famine
			days = -250
		}
    }
	prerequisite = { focus = ETH_request_accounting_nobles }
}

shared_focus = {
		id = ETH_fund_grain_dole_in_addis_ababa
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 16
		y = 2
		
	completion_reward = {
		modify_timed_idea = {
			idea = ETH_great_famine
			days = -250
		}
    }
	prerequisite = { focus = ETH_begin_famine_recovery_program }
}

shared_focus = {
		id = ETH_disctribute_plow_oxen_to_village_councils
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 18
		y = 2
		
	completion_reward = {
		modify_timed_idea = {
			idea = ETH_great_famine
			days = -250
		}
    }
	prerequisite = { focus = ETH_begin_famine_recovery_program }
}

shared_focus = {
		id = ETH_reduce_reliance_on_subsistance_cattle_herding
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 17
		y = 3
		
	completion_reward = {
		modify_timed_idea = {
			idea = ETH_great_famine
			days = -500
		}
    }
	prerequisite = { focus = ETH_disctribute_plow_oxen_to_village_councils }
	prerequisite = { focus = ETH_fund_grain_dole_in_addis_ababa }
}
################### organise_council_on_modernisation ###################

shared_focus = {
		id = ETH_organise_council_on_modernisation
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 25
		y = 1
		
	completion_reward = {
		add_tech_bonus = {
			bonus = 0.2
			uses = 1
			category = construction_tech
		}
		add_political_power = 10
    }
	prerequisite = { focus = ETH_request_accounting_nobles }
}

shared_focus = {
		id = ETH_ethio_djibouti_railway
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 21
		y = 2
		
	completion_reward = {
		country_event = {
			id = TCS_ethiopia.23
		}
    }
	prerequisite = { focus = ETH_organise_council_on_modernisation }
}

shared_focus = {
		id = ETH_allow_total_foreign_control
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 19
		y = 3
		
	#completion_reward = {
	#	
    #}
	prerequisite = { focus = ETH_ethio_djibouti_railway }
	mutually_exclusive = { focus = ETH_a_shared_railway }
}

shared_focus = {
		id = ETH_invite_british_supervisers
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 18
		y = 4
		
	completion_reward = {
		271  = {
			add_building_construction = {
				type = infrastructure
				level = 2
				instant_build = yes
			}
		}
		add_political_power = -50
    }
	prerequisite = { focus = ETH_allow_total_foreign_control }
}

shared_focus = {
		id = ETH_invite_french_engineers
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 20
		y = 4
		
	completion_reward = {
		271  = {
			add_building_construction = {
				type = infrastructure
				level = 2
				instant_build = yes
			}
		}
		add_political_power = -50
    }
	prerequisite = { focus = ETH_allow_total_foreign_control }
}

shared_focus = {
		id = ETH_franco_british_leasing
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 19
		y = 5
		
	completion_reward = {
		give_military_access = ENG
		give_military_access = FRA
		add_political_power = -50
		add_stability = -0.05
		add_war_support = -0.05
		country_event = {
			id = TCS_ethiopia.24
		}
	}
	prerequisite = { focus = ETH_invite_british_supervisers }
	prerequisite = { focus = ETH_invite_french_engineers }
}

shared_focus = {
		id = ETH_construct_addis_ababa_european_quarter
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 19
		y = 6
		
	completion_reward = {
		add_ideas = ETH_addis_ababa_european_quarter
    }
	prerequisite = { focus = ETH_franco_british_leasing }
}

shared_focus = {
		id = ETH_a_shared_railway
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 23
		y = 3
		
	#completion_reward = {
	#	
    #}
	prerequisite = { focus = ETH_ethio_djibouti_railway }
	mutually_exclusive = { focus = ETH_allow_total_foreign_control }
}

shared_focus = {
		id = ETH_concessions_to_dr_vitalien
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 22
		y = 4
		
	completion_reward = {
		271  = {
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		country_event = {
			id = TCS_ethiopia.25
		}
    }
	prerequisite = { focus = ETH_a_shared_railway }
}

shared_focus = {
		id = ETH_joint_railroad_construction_Administration
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 24
		y = 4
		
	completion_reward = {
		815  = {
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		add_political_power = -25
    }
	prerequisite = { focus = ETH_a_shared_railway }
}

shared_focus = {
		id = ETH_the_franco_ethiopian_railway_company
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 23
		y = 5
		
	completion_reward = {
		271  = {
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		country_event = {
			id = TCS_ethiopia.26
		}
    }
	prerequisite = { focus = ETH_joint_railroad_construction_Administration }
	prerequisite = { focus = ETH_concessions_to_dr_vitalien }
}

shared_focus = {
		id = ETH_european_hotels_in_addis_ababa
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 23
		y = 6
		
	completion_reward = {
		add_political_power = 15
		ETH_feudalism_betterment = yes
    }
	prerequisite = { focus = ETH_the_franco_ethiopian_railway_company }
}

shared_focus = {
		id = ETH_addis_ababa_to_djibouti_telegraph_line
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 21
		y = 7
		
	completion_reward = {
		add_political_power = 10
		add_opinion_modifier = {
			target = FRA
			modifier = ETH_railroad
        }
		add_opinion_modifier = {
			target = RUS
			modifier = ETH_railroad
        }
		add_opinion_modifier = {
			target = GER
			modifier = ETH_railroad
        }
		add_opinion_modifier = {
			target = ENG
			modifier = ETH_railroad
        }
    }
	prerequisite = {
	focus = ETH_european_hotels_in_addis_ababa
	focus = ETH_construct_addis_ababa_european_quarter
	}
}

shared_focus = {
		id = ETH_public_works
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 29
		y = 2
		
	completion_reward = {
		add_tech_bonus = {
			bonus = 0.2
			uses = 1
			category = industry
		}
    }
	prerequisite = { focus = ETH_organise_council_on_modernisation }
}

shared_focus = {
		id = ETH_modern_bridges_in_oromia
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 27
		y = 3
		
	completion_reward = {
		271  = {
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
	}
	prerequisite = { focus = ETH_public_works }
}
shared_focus = {
		id = ETH_create_addis_ababa_market_square
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 26
		y = 4
		
	completion_reward = {
		add_political_power = 25
    }
	prerequisite = { focus = ETH_modern_bridges_in_oromia }
}

shared_focus = {
		id = ETH_hospital_construction_program
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 26
		y = 5
		
	completion_reward = {
		add_tech_bonus = {
			bonus = 0.2
			uses = 1
			category = support_tech
		}
    }
	prerequisite = { focus = ETH_create_addis_ababa_market_square }
}

shared_focus = {
		id = ETH_decree_the_privatisation_of_city_land
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 28
		y = 4
		
	completion_reward = {
		271 = {
			add_extra_state_shared_building_slots = 1
		}
    }
	prerequisite = { focus = ETH_modern_bridges_in_oromia }
}

shared_focus = {
		id = ETH_experiment_with_coal_power_generation
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 28
		y = 5
		
	completion_reward = {
		ETH_feudalism_betterment = yes
    }
	prerequisite = { focus = ETH_decree_the_privatisation_of_city_land }
}

shared_focus = {
		id = ETH_found_municipal_modernisation_councils
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 27
		y = 6
		
	completion_reward = {
		ETH_meneliks_reign_change = yes
		country_event = {
			id = TCS_ethiopia.27
		}
    }
	prerequisite = { focus = ETH_experiment_with_coal_power_generation }
	prerequisite = { focus = ETH_hospital_construction_program }
}

shared_focus = {
		id = ETH_encourage_meritocratic_governence_municipal_councils
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 26
		y = 7
		available = {
			always = no
		}
		
	#completion_reward = {
	#	
    #}
	prerequisite = { focus = ETH_found_municipal_modernisation_councils }
	mutually_exclusive = { focus = ETH_aristocratic_dominated_municipal_councils }
}

shared_focus = {
		id = ETH_aristocratic_dominated_municipal_councils
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 28
		y = 7
		available = {
			always = no
		}
		
	#completion_reward = {
	#
    #}
	prerequisite = { focus = ETH_found_municipal_modernisation_councils }
	mutually_exclusive = { focus = ETH_encourage_meritocratic_governence_municipal_councils }
}

shared_focus = {
		id = ETH_rudimentary_education_system
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 31
		y = 3
		
	completion_reward = {
		country_event = {
			id = TCS_ethiopia.29
		}
    }
	prerequisite = { focus = ETH_public_works }
}

shared_focus = {
		id = ETH_european_style_state_school_system
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 30
		y = 4
		
	#completion_reward = {
	#
    #}
	prerequisite = { focus = ETH_rudimentary_education_system }
	mutually_exclusive = { focus = ETH_fund_local_religous_schools }
}

shared_focus = {
		id = ETH_hire_european_administrators
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 30
		y = 5
		
	completion_reward = {
		ETH_feudalism_betterment = yes
		add_stability = -0.02
    }
	prerequisite = { focus = ETH_european_style_state_school_system }
}

shared_focus = {
		id = ETH_fund_local_religous_schools
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 32
		y = 4
		
	#completion_reward = {
	#
    #}
	prerequisite = { focus = ETH_rudimentary_education_system }
	mutually_exclusive = { focus = ETH_european_style_state_school_system }
}

shared_focus = {
		id = ETH_in_the_heart_of_the_vilage
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 32
		y = 5
		
	completion_reward = {
		add_stability = 0.02
    }
	prerequisite = { focus = ETH_fund_local_religous_schools }
}

shared_focus = {
		id = ETH_build_new_school_houses
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 31
		y = 6
		
	completion_reward = {
		ETH_feudalism_betterment = yes
		add_popularity = {
			ideology = pat_autocrat
			popularity = 0.01
		}
    }
	prerequisite = {
	focus = ETH_hire_european_administrators
	focus = ETH_in_the_heart_of_the_vilage
	}
}

shared_focus = {
		id = ETH_minor_amharic_literacy_campaign
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 31
		y = 7
		
	completion_reward = {
		ETH_meneliks_reign_change = yes
    }
	prerequisite = { focus = ETH_build_new_school_houses }
}

shared_focus = {
		id = ETH_found_addis_ababa_civic_police_force
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 25
		y = 8
		
	completion_reward = {
		add_stability = 0.01
		ETH_feudalism_betterment = yes
    }
	prerequisite = { focus = ETH_addis_ababa_to_djibouti_telegraph_line }
	prerequisite = { 
	focus = ETH_aristocratic_dominated_municipal_councils
	focus = ETH_encourage_meritocratic_governence_municipal_councils
	}
	prerequisite = { focus = ETH_minor_amharic_literacy_campaign }
}

shared_focus = {
		id = ETH_create_daily_amharic_language_newspapers
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 23
		y = 9
		
	completion_reward = {
		add_popularity = {
			ideology = soc_conservatism
			popularity = 0.01
		}
		add_popularity = {
			ideology = auth_democrat
			popularity = 0.01
		}
		add_political_power = 25
    }
	prerequisite = { focus = ETH_found_addis_ababa_civic_police_force }
}

shared_focus = {
		id = ETH_modern_plumbing_in_addis_ababa
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 25
		y = 9
		
	completion_reward = {
		ETH_meneliks_reign_change = yes
    }
	prerequisite = { focus = ETH_found_addis_ababa_civic_police_force }
}

shared_focus = {
		id = ETH_provincial_dirt_road_networks
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 27
		y = 9
		
	completion_reward = {
		815  = {
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		
    }
	prerequisite = { focus = ETH_found_addis_ababa_civic_police_force }
}

shared_focus = {
		id = ETH_africas_newest_boom_town
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 25
		y = 10
		
	completion_reward = {
		271 = {
			add_building_construction = {
				type = arms_factory
				level = 5
				instant_build = yes
			}
		}
		country_event = {
			id = TCS_ethiopia.28
		}
    }
	prerequisite = { focus = ETH_provincial_dirt_road_networks }
	prerequisite = { focus = ETH_modern_plumbing_in_addis_ababa }
	prerequisite = { focus = ETH_create_daily_amharic_language_newspapers }
}

########################
#MILITARY TREE ETHIOPIA#
########################

shared_focus = {
		id = ETH_victors_of_adwa
		icon = GFX_focus_generic_treaty
		cost = 6
		x = 40
		y = 0
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_uppgrade_feudal_army_structures
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_victors_of_adwa }
		mutually_exclusive = { focus = ETH_begin_to_implement_european_military_reforms }
		cost = 6
		x = 37
		y = 1
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_purchase_rifles_boer_war
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_uppgrade_feudal_army_structures }
		cost = 6
		x = 36
		y = 2
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_make_use_of_local_knowledge
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_purchase_rifles_boer_war }
		cost = 6
		x = 36
		y = 3
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_make_use_of_local_medical_practices
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_uppgrade_feudal_army_structures }
		cost = 6
		x = 38
		y = 2
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_national_levee_callup_work
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_make_use_of_local_medical_practices }
		cost = 6
		x = 38
		y = 3
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_draw_officers_from_local_nobility
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_national_levee_callup_work }
		prerequisite = { focus = ETH_make_use_of_local_knowledge }
		cost = 6
		x = 37
		y = 4
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_new_feudal_army
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_draw_officers_from_local_nobility }
		cost = 6
		x = 37
		y = 5
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_hit_and_run_tactics
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_new_feudal_army }
		cost = 6
		x = 35
		y = 6
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_peasant_farm_and_ranch_requisition_policy
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_hit_and_run_tactics }
		cost = 6
		x = 35
		y = 7
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_purchase_older_ottoman_arty_pieces
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_peasant_farm_and_ranch_requisition_policy }
		cost = 6
		x = 35
		y = 8
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_streamline_noble_command_structure
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_new_feudal_army }
		cost = 6
		x = 39
		y = 6
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_organise_national_conscription_census
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_streamline_noble_command_structure }
		cost = 6
		x = 39
		y = 7
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_rapid_redeployment_strategy
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_organise_national_conscription_census }
		cost = 6
		x = 39
		y = 8
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_old_ways_with_new_equipment
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_rapid_redeployment_strategy }
		prerequisite = { focus = ETH_purchase_older_ottoman_arty_pieces }
		cost = 6
		x = 37
		y = 9
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_begin_to_implement_european_military_reforms
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_victors_of_adwa }
		mutually_exclusive = { focus = ETH_uppgrade_feudal_army_structures }
		cost = 6
		x = 43
		y = 1
		
	#completion_reward = {
	#	
    #}
}

shared_focus = {
		id = ETH_found_royal_signlar_corps_horseback
		icon = GFX_focus_generic_treaty
		prerequisite = { focus = ETH_begin_to_implement_european_military_reforms }
		cost = 6
		x = 42
		y = 2
		
	#completion_reward = {
	#	
    #}
}