shared_focus = {
	id = ENG_balfour_ministry
	icon = GFX_balfour_ministry
	cost = 2
	x = 4
	y = 0

	available = {
	}

	completion_reward = {
        country_event = britain.0
	}
}

shared_focus = {
	id = ENG_question_trade_unions_liability
	icon = GFX_balfour_ministry
	prerequisite = { focus = ENG_balfour_ministry }
	cost = 2
	x = -2
	y = 1
	relative_position_id = ENG_balfour_ministry

	available = {
	}

	completion_reward = {
        add_political_power = 25
        country_event = britain.1
	}
}

shared_focus = {
	id = ENG_debate_aliens_act
	icon = GFX_pass_the_aliens_act
	prerequisite = { focus = ENG_balfour_ministry }
	cost = 2
	x = 0
	y = 1
	relative_position_id = ENG_balfour_ministry

	available = {
	}

	completion_reward = {
        country_event = britain.2
	}
}

shared_focus = {
	id = ENG_public_intoxication_ireland_bill
	icon = GFX_balfour_ministry
	prerequisite = { focus = ENG_balfour_ministry }
	cost = 2
	x = 2
	y = 1
	relative_position_id = ENG_balfour_ministry

	available = {
	}

	completion_reward = {
        country_event = britain.3
	}
}

shared_focus = {
	id = ENG_protests_chinese_labour
	icon = GFX_balfour_ministry
	prerequisite = { focus = ENG_question_trade_unions_liability }
	prerequisite = { focus = ENG_debate_aliens_act }
	cost = 2
	x = -1
	y = 2
	relative_position_id = ENG_balfour_ministry

	available = {
	}

	completion_reward = {
        add_political_power = 25
        country_event = britain.4
	}
}

shared_focus = {
	id = ENG_consolidate_red_cross
	icon = GFX_balfour_ministry
	prerequisite = { focus = ENG_public_intoxication_ireland_bill }
	prerequisite = { focus = ENG_debate_aliens_act }
	cost = 2
	x = 1
	y = 2
	relative_position_id = ENG_balfour_ministry

	available = {
	}

	completion_reward = {
       add_political_power = 25
	}
}

shared_focus = {
	id = ENG_trimming_naval_expenses
	icon = GFX_cut_naval_expenditure
	prerequisite = { focus = ENG_protests_chinese_labour }
	cost = 2
	x = -1
	y = 3
	relative_position_id = ENG_balfour_ministry

	available = {
	}

	completion_reward = {
       	country_event = britain.5
	}
}

shared_focus = {
	id = ENG_empower_committee_imperial_defense
	icon = GFX_balfour_ministry
	prerequisite = { focus = ENG_consolidate_red_cross }
	cost = 2
	x = 1
	y = 3
	relative_position_id = ENG_balfour_ministry

	available = {
	}

	completion_reward = {
        country_event = britain.6
	}
}

shared_focus = {
	id = ENG_army_performance_boer_war
	icon = GFX_balfour_ministry
	prerequisite = { focus = ENG_trimming_naval_expenses }
	prerequisite = { focus = ENG_empower_committee_imperial_defense }
	cost = 2
	x = 0
	y = 4
	relative_position_id = ENG_balfour_ministry

	available = {
	}

	completion_reward = {
        army_experience = 5
        country_event = britain.7
	}
}

shared_focus = {
	id = ENG_attempt_tarrif_reform
	icon = GFX_balfour_ministry
	prerequisite = { focus = ENG_army_performance_boer_war }
	cost = 2
	x = 0
	y = 5
	relative_position_id = ENG_balfour_ministry

	available = {
	}

	completion_reward = {
		country_event = britain.8
	}
}

shared_focus = {
	id = ENG_talks_liberal_unionists
	icon = GFX_goal_lib_con_agreement
	prerequisite = { focus = ENG_attempt_tarrif_reform }
	mutually_exclusive = { focus = ENG_reach_moderate_liberals }
	mutually_exclusive = { focus = ENG_reach_TUC }
	cost = 0
	x = -2
	y = 6
	relative_position_id = ENG_balfour_ministry

	available = {
		custom_trigger_tooltip = {
			always = no
			tooltip = ENG_tt_unlock_focus
		}
	}

	completion_reward = {
       
	}
}

shared_focus = {
	id = ENG_reach_moderate_liberals
	icon = GFX_goal_social_liberal_agreement
	prerequisite = { focus = ENG_attempt_tarrif_reform }
	mutually_exclusive = { focus = ENG_talks_liberal_unionists }
	mutually_exclusive = { focus = ENG_reach_TUC }
	cost = 0
	x = 0
	y = 6
	relative_position_id = ENG_balfour_ministry

	available = {
		custom_trigger_tooltip = {
			always = no
			tooltip = ENG_tt_unlock_focus
		}
	}
	
	completion_reward = {
       
	}
}

shared_focus = {
	id = ENG_reach_TUC
	icon = GFX_goal_social_democrat_agreement
	prerequisite = { focus = ENG_attempt_tarrif_reform }
	mutually_exclusive = { focus = ENG_talks_liberal_unionists }
	mutually_exclusive = { focus = ENG_reach_moderate_liberals }
	cost = 0
	x = 2
	y = 6
	relative_position_id = ENG_balfour_ministry

	available = {
		custom_trigger_tooltip = {
			always = no
			tooltip = ENG_tt_unlock_focus
		}
	}

	completion_reward = {
       
	}
}

shared_focus = {
	id = ENG_end_of_era
	icon = GFX_PER_placate_parliament
	prerequisite = { 
		focus = ENG_talks_liberal_unionists
		focus = ENG_reach_moderate_liberals
		focus = ENG_reach_TUC
	}
	cost = 0
	x = 0
	y = 7
	relative_position_id = ENG_balfour_ministry

	available = {
		custom_trigger_tooltip = {
			always = no
			tooltip = ENG_tt_unlock_focus
		}
	}

	completion_reward = {
       
	}
}

