focus_tree = {
	id = TCS_Honduras_Base
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = HON
		}
	}
	default = no
	shared_focus = HON_settle_border_dispute

	focus = {
		id = HON_generic_army_effort
		icon = GFX_goal_generic_allies_build_infantry
		cost = 10
		x = 1
		y = 0
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_army_effort"
			army_experience = 5
			add_tech_bonus = {
				name = land_doc_bonus
				bonus = 0.5
				uses = 1
				category = land_doctrine
			}
		}

	}
	focus = {
		id = HON_generic_aviation_effort
		icon = GFX_goal_generic_build_airforce
		cost = 10
		x = 5
		y = 0
		available = {
			date > 1921.1.1
		}
		complete_tooltip = {
			air_experience = 25
			if = {
				limit = {
					has_country_flag = aviation_effort_AB
				}
				add_building_construction = {
					type = air_base
					level = 2
					instant_build = yes
				}
			}
			add_tech_bonus = {
				name = air_doc_bonus
				bonus = 0.5
				uses = 1
				category = air_doctrine
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_aviation_effort"
			air_experience = 25
			if = {
				limit = {
					capital_scope = {
						NOT = {
							free_building_slots = {
								building = air_base
								size > 1
							}
						}
					}
				}
				random_owned_state = {
					limit = {
						free_building_slots = {
							building = air_base
							size > 1
						}
					}
					add_building_construction = {
						type = air_base
						level = 2
						instant_build = yes
					}
					ROOT = {
						set_country_flag = aviation_effort_AB
					}
				}
			}
			if = {
				limit = {
					capital_scope = {
						free_building_slots = {
							building = air_base
							size > 1
						}
					}
				}
				capital_scope = {
					add_building_construction = {
						type = air_base
						level = 2
						instant_build = yes
					}
					ROOT = {
						set_country_flag = aviation_effort_AB
					}
				}
			}
			add_tech_bonus = {
				name = air_doc_bonus
				bonus = 0.5
				uses = 1
				category = air_doctrine
			}
		}

	}
	focus = {
		id = HON_generic_naval_effort
		icon = GFX_goal_generic_construct_naval_dockyard
		cost = 10
		x = 9
		y = 0
		available = {
			any_owned_state = {
				is_coastal = yes
				is_controlled_by = ROOT
			}
		}
		complete_tooltip = {
			navy_experience = 25
			add_extra_state_shared_building_slots = 3
			add_building_construction = {
				type = dockyard
				level = 3
				instant_build = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_naval_effort"
			navy_experience = 25
			if = {
				limit = {
					NOT = {
						any_owned_state = {
							dockyard > 0
							free_building_slots = {
								building = dockyard
								size > 2
								include_locked = yes
							}
						}
					}
					any_owned_state = {
						is_coastal = yes
					}
				}
				random_owned_state = {
					limit = {
						is_coastal = yes
						free_building_slots = {
							building = dockyard
							size > 2
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 3
					add_building_construction = {
						type = dockyard
						level = 3
						instant_build = yes
					}
				}
				set_country_flag = naval_effort_built
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = naval_effort_built
					}
					any_owned_state = {
						dockyard > 0
						free_building_slots = {
							building = dockyard
							size > 2
							include_locked = yes
						}
					}
				}
				random_owned_state = {
					limit = {
						dockyard > 0
						free_building_slots = {
							building = dockyard
							size > 2
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 3
					add_building_construction = {
						type = dockyard
						level = 3
						instant_build = yes
					}
				}
				set_country_flag = naval_effort_built
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = naval_effort_built
					}
					NOT = {
						any_owned_state = {
							free_building_slots = {
								building = dockyard
								size > 2
								include_locked = yes
							}
						}
					}
				}
				random_state = {
					limit = {
						controller = {
							tag = ROOT
						}
						free_building_slots = {
							building = dockyard
							size > 2
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 3
					add_building_construction = {
						type = dockyard
						level = 3
						instant_build = yes
					}
				}
			}
		}

	}
	focus = {
		id = HON_generic_industrial_effort
		icon = GFX_goal_generic_production
		cost = 10
		x = 14
		y = 0
		available = {
			OR = {
				NOT = {
					is_ai = yes
				}
				is_subject = yes
				num_of_factories > 10
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_industrial_effort"
			add_tech_bonus = {
				name = industrial_bonus
				bonus = 0.5
				uses = 1
				category = industry
			}
		}
		ai_will_do = {
			factor = 2
		}

	}
	focus = {
		id = HON_generic_equipment_effort
		icon = GFX_goal_generic_small_arms
		cost = 10
		prerequisite = {
			focus = HON_generic_army_effort
		}
		x = 0
		y = 1
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_equipment_effort"
			add_tech_bonus = {
				name = infantry_weapons_bonus
				bonus = 0.5
				uses = 1
				category = infantry_weapons
				category = artillery
			}
		}

	}
	focus = {
		id = HON_generic_equipment_effort_2
		icon = GFX_goal_generic_army_artillery
		cost = 10
		prerequisite = {
			focus = HON_generic_equipment_effort
		}
		x = 0
		y = 2
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_equipment_effort_2"
			add_tech_bonus = {
				name = infantry_artillery_bonus
				bonus = 0.5
				uses = 1
				category = infantry_weapons
				category = artillery
			}
		}

	}
	focus = {
		id = HON_generic_equipment_effort_3
		icon = GFX_goal_generic_army_artillery2
		cost = 10
		available = {
			date > 1918.1.1
		}
		prerequisite = {
			focus = HON_generic_equipment_effort_2
		}
		prerequisite = {
			focus = HON_generic_doctrine_effort_2
		}
		x = 1
		y = 3
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_equipment_effort_3"
			add_tech_bonus = {
				name = infantry_artillery_bonus
				ahead_reduction = 1
				uses = 1
				category = infantry_weapons
				category = artillery
			}
		}

	}
	focus = {
		id = HON_generic_doctrine_effort
		icon = GFX_goal_generic_army_doctrines
		cost = 10
		prerequisite = {
			focus = HON_generic_army_effort
		}
		x = 2
		y = 1
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_doctrine_effort"
			army_experience = 5
			add_tech_bonus = {
				name = land_doc_bonus
				bonus = 0.5
				uses = 1
				category = land_doctrine
			}
		}

	}
	focus = {
		id = HON_generic_doctrine_effort_2
		icon = GFX_goal_generic_army_doctrines
		cost = 10
		prerequisite = {
			focus = HON_generic_doctrine_effort
		}
		x = 2
		y = 2
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_doctrine_effort_2"
			army_experience = 5
			add_tech_bonus = {
				name = land_doc_bonus
				bonus = 0.5
				uses = 1
				category = land_doctrine
			}
		}

	}
	focus = {
		id = HON_generic_aviation_effort_2
		icon = GFX_goal_generic_air_doctrine
		cost = 10
		prerequisite = {
			focus = HON_generic_aviation_effort
		}
		x = 5
		y = 1
		complete_tooltip = {
			air_experience = 25
			if = {
				limit = {
					has_country_flag = aviation_effort_2_AB
				}
				add_building_construction = {
					type = air_base
					level = 2
					instant_build = yes
				}
			}
			add_tech_bonus = {
				name = air_doc_bonus
				bonus = 0.5
				uses = 1
				category = air_doctrine
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_aviation_effort_2"
			air_experience = 10
			if = {
				limit = {
					capital_scope = {
						NOT = {
							free_building_slots = {
								building = air_base
								size > 1
							}
						}
					}
				}
				random_owned_state = {
					limit = {
						free_building_slots = {
							building = air_base
							size > 1
						}
					}
					add_building_construction = {
						type = air_base
						level = 1
						instant_build = yes
					}
					ROOT = {
						set_country_flag = aviation_effort_2_AB
					}
				}
			}
			if = {
				limit = {
					capital_scope = {
						free_building_slots = {
							building = air_base
							size > 1
						}
					}
				}
				capital_scope = {
					add_building_construction = {
						type = air_base
						level = 1
						instant_build = yes
					}
					ROOT = {
						set_country_flag = aviation_effort_2_AB
					}
				}
			}
			add_tech_bonus = {
				name = air_doc_bonus
				bonus = 0.5
				uses = 1
				category = air_doctrine
			}
		}

	}
	focus = {
		id = HON_generic_flexible_navy
		icon = GFX_goal_generic_build_navy
		cost = 10
		prerequisite = {
			focus = HON_generic_naval_effort
		}
		x = 8
		y = 1
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				all_owned_state = {
					OR = {
						is_coastal = no
						dockyard < 1
					}
				}
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_flexible_navy"
			add_tech_bonus = {
				name = sub_op_bonus
				bonus = 0.5
				uses = 2
				technology = convoy_interdiction_ti
				technology = unrestricted_submarine_warfare
				technology = wolfpacks
				technology = advanced_submarine_warfare
				technology = combined_operations_raiding
			}
		}

	}
	focus = {
		id = HON_generic_destroyer_effort
		icon = GFX_goal_generic_wolf_pack
		cost = 10
		prerequisite = {
			focus = HON_generic_naval_effort
		}
		x = 10
		y = 1
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				all_owned_state = {
					OR = {
						is_coastal = no
						dockyard < 1
					}
				}
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_destroyer_effort"
			add_tech_bonus = {
				name = dd_bonus
				bonus = 0.5
				ahead_reduction = 1
				uses = 1
				#
				#technology = basic_destroyer
				#technology = improved_destroyer
				#technology = advanced_destroyer
			}
		}

	}
	focus = {
		id = HON_generic_construction_effort
		icon = GFX_goal_generic_construct_civ_factory
		cost = 10
		prerequisite = {
			focus = HON_generic_infrastructure_effort
		}
		x = 13
		y = 2
		ai_will_do = {
			factor = 2
		}
		bypass = {
			custom_trigger_tooltip = {
				tooltip = construction_effort_tt
				all_owned_state = {
					free_building_slots = {
						building = industrial_complex
						size < 1
						include_locked = yes
					}
				}
			}
		}
		complete_tooltip = {
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_construction_effort"
			random_owned_state = {
				limit = {
					free_building_slots = {
						building = industrial_complex
						size > 0
						include_locked = yes
					}
					OR = {
						is_in_home_area = yes
						NOT = {
							owner = {
								any_owned_state = {
									free_building_slots = {
										building = industrial_complex
										size > 0
										include_locked = yes
									}
									is_in_home_area = yes
								}
							}
						}
					}
				}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
			}
		}

	}
	focus = {
		id = HON_generic_construction_effort_2
		icon = GFX_goal_generic_construct_civ_factory
		cost = 10
		prerequisite = {
			focus = HON_generic_construction_effort
		}
		x = 13
		y = 3
		ai_will_do = {
			factor = 2
		}
		bypass = {
			custom_trigger_tooltip = {
				tooltip = construction_effort_tt
				all_owned_state = {
					free_building_slots = {
						building = industrial_complex
						size < 1
						include_locked = yes
					}
				}
			}
		}
		complete_tooltip = {
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_construction_effort_2"
			random_owned_state = {
				limit = {
					free_building_slots = {
						building = industrial_complex
						size > 0
						include_locked = yes
					}
					OR = {
						is_in_home_area = yes
						NOT = {
							owner = {
								any_owned_state = {
									free_building_slots = {
										building = industrial_complex
										size > 0
										include_locked = yes
									}
									is_in_home_area = yes
								}
							}
						}
					}
				}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
			}
		}

	}
	focus = {
		id = HON_generic_infrastructure_effort
		icon = GFX_goal_generic_construct_infrastructure
		cost = 10
		prerequisite = {
			focus = HON_generic_industrial_effort
		}
		x = 14
		y = 1
		bypass = {
			custom_trigger_tooltip = {
				tooltip = infrastructure_effort_tt
				all_owned_state = {
					free_building_slots = {
						building = infrastructure
						size < 1
					}
				}
			}
		}
		complete_tooltip = {
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_infrastructure_effort"
			random_owned_state = {
				limit = {
					free_building_slots = {
						building = infrastructure
						size > 0
					}
					OR = {
						is_in_home_area = yes
						NOT = {
							owner = {
								any_owned_state = {
									free_building_slots = {
										building = infrastructure
										size > 0
									}
									is_in_home_area = yes
								}
							}
						}
					}
				}
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			random_owned_state = {
				limit = {
					free_building_slots = {
						building = infrastructure
						size > 0
					}
					OR = {
						is_in_home_area = yes
						NOT = {
							owner = {
								any_owned_state = {
									free_building_slots = {
										building = infrastructure
										size > 0
									}
									is_in_home_area = yes
								}
							}
						}
					}
				}
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}

	}
	focus = {
		id = HON_generic_production_effort
		icon = GFX_goal_generic_construct_mil_factory
		cost = 10
		prerequisite = {
			focus = HON_generic_infrastructure_effort
		}
		x = 15
		y = 2
		ai_will_do = {
			factor = 2
		}
		bypass = {
			custom_trigger_tooltip = {
				tooltip = production_effort_tt
				all_owned_state = {
					free_building_slots = {
						building = arms_factory
						size < 1
						include_locked = yes
					}
				}
			}
		}
		complete_tooltip = {
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_production_effort"
			random_owned_state = {
				limit = {
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
					OR = {
						is_in_home_area = yes
						NOT = {
							owner = {
								any_owned_state = {
									free_building_slots = {
										building = arms_factory
										size > 0
										include_locked = yes
									}
									is_in_home_area = yes
								}
							}
						}
					}
				}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}

	}
	focus = {
		id = HON_generic_production_effort_2
		icon = GFX_goal_generic_construct_mil_factory
		cost = 10
		prerequisite = {
			focus = HON_generic_production_effort
		}
		x = 15
		y = 3
		ai_will_do = {
			factor = 2
		}
		bypass = {
			custom_trigger_tooltip = {
				tooltip = production_effort_tt
				all_owned_state = {
					free_building_slots = {
						building = arms_factory
						size < 1
						include_locked = yes
					}
				}
			}
		}
		complete_tooltip = {
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus HON_generic_production_effort_2"
			random_owned_state = {
				limit = {
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
					OR = {
						is_in_home_area = yes
						NOT = {
							owner = {
								any_owned_state = {
									free_building_slots = {
										building = arms_factory
										size > 0
										include_locked = yes
									}
									is_in_home_area = yes
								}
							}
						}
					}
				}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}

	}
}