focus_tree = {
	id = TCS_Luxembourg_Pre_War
	country = {
		factor = 0
		modifier = {
			add = 20
			tag = LUX
		}
	}
	initial_show_position = {
		focus = LUX_1904_census
	}
	shared_focus = LUX_1904_census
	shared_focus = LUX_review_the_gendarmerie
	shared_focus = LUX_address_secularization
	continuous_focus_position = { x = 20 y = 1600 }
}