focus_tree = {
	id = TCS_Austria
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = AUS
		}
	}
	continuous_focus_position = { x = 2000 y = 1150 }
	default = no
	shared_focus = AUS_two_sides_of_the_leitha
}