### search_filters = {FOCUS_FILTER_POLITICAL}
### search_filters = {FOCUS_FILTER_RESEARCH}
### search_filters = {FOCUS_FILTER_INDUSTRY}
### search_filters = {FOCUS_FILTER_STABILITY}
### search_filters = {FOCUS_FILTER_WAR_SUPPORT}
### search_filters = {FOCUS_FILTER_MANPOWER}

focus_tree = {
	id = TCS_Ukraine_1918_Start
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = UKR
			# available = { # Is not a valid modifier -Brammeke @Bart
			# 	date > 1918.11.11
			# 	NOT = {
			# 		tag = UPR
			# 	}	
		    # }
		}
	}
	default = no
	continuous_focus_position = { x = 175 y = 1475 }

	focus = {
		id = UKR_Restoration_of_the_Ukrainian_Peoples_Republic
		icon = GFX_GENERIC_crush_pat_autocrat
		available = {
			date > 1918.11.11
			has_country_leader = {
				character = UKR_pavlo_skoropadskyi 
				ruling_only = yes 
			}
			has_country_flag = UKR_1918_anti_hetman_uprising
		}
		search_filters = {FOCUS_FILTER_POLITICAL}
		completion_reward = {
			add_political_power = 100
			army_experience = 50
			add_command_power = 50
            set_politics = {
                ruling_party = soc_democracy
                last_election = "1917.3.04"
                election_frequency = 60
                elections_allowed = yes
            }

			custom_effect_tooltip = ukraine.3.a.tooltip

			remove_ideas = UKR_incompetent_army
			remove_ideas = UKR_pavlo_skoropadskyi
			remove_ideas = UKR_anti_hetman_uprising

			add_ideas = UKR_directorate
			hidden_effect = {
				198 = { set_demilitarized_zone = no }
        	    992 = { set_demilitarized_zone = no }
                201 = { set_demilitarized_zone = no }
                199 = { set_demilitarized_zone = no }
                93 = { set_demilitarized_zone = no }
                991 = { set_demilitarized_zone = no }
                987 = { set_demilitarized_zone = no }
				203 = { set_demilitarized_zone = no }
				228 = { set_demilitarized_zone = no }
				221 = { set_demilitarized_zone = no }
				995 = { set_demilitarized_zone = no }
				197 = { set_demilitarized_zone = no }
				192 = { set_demilitarized_zone = no }
				remove_ideas = {
                    UKR_serhiy_herbel_hos
                }
                add_ideas = {
                    UKR_volodymyr_chekhivsky_hos
                }

				remove_ideas = {
                    UKR_gieorgij_afanasjew
                }
                add_ideas = {
                    UKR_volodymyr_chekhivsky_for
                }

				remove_ideas = {
                    UKR_ihor_kistyakivskyi
                }
                add_ideas = {
                    UKR_oleksandr_mytsyuk
                }

				remove_ideas = {
                    UKR_anton_rzhepetsky
                }
                add_ideas = {
                    UKR_vasyl_mazurenko
                }

				add_ideas = {
                    UKR_symon_petlyura
                }

				clr_country_flag = soc_democracy_opposition
                clr_country_flag = soc_liberal_opposition
                #clr_country_flag = lib_conservatism_opposition
                #clr_country_flag = auth_democrat_coalition
                #clr_country_flag = pat_autocrat_coalition
                #clr_country_flag = fascism_coalition

                set_country_flag = soc_democracy_coalition
                set_country_flag = soc_liberal_coalition
                #set_country_flag = lib_conservatism_coalition
                #set_country_flag = auth_democrat_opposition
                #set_country_flag = pat_autocrat_opposition
                #set_country_flag = fascism_opposition

			    set_country_flag = lib_conservatism_opposition
                set_country_flag = auth_democrat_opposition
			    set_country_flag = pat_autocrat_opposition
			    set_country_flag = fascism_opposition

				remove_advisor_role = {
                    character = "UKR_pavlo_skoropadskyi"
                    slot = army_chief
                }
				UKR_fedir_keller = { remove_unit_leader_role = yes }
				#load_oob = "UPR_1918"
				#VOA = {
                #    transfer_state = 196
                #    transfer_state = 200
                #}
			}
			#POL = { country_event = { id = poland_1918.2 } }
		}

		cost = 5
		
		x = 9
		y = 0

	}

	focus = {
		id = UKR_Fourth_Congress_of_Ukrainian_Social_Democratic_Labour_Party
		icon = GFX_goal_BSR_socialist_support
		cost = 2
		prerequisite = {
			focus = UKR_Restoration_of_the_Ukrainian_Peoples_Republic
		}

		x = 0
		y = 1

		relative_position_id = UKR_Restoration_of_the_Ukrainian_Peoples_Republic

		search_filters = {FOCUS_FILTER_STABILITY}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 2
				#is_historical_focus_on = yes
				#has_completed_focus = POL_rebirth_of_poland
			}
		}
		completion_reward = {
			add_political_power = 25
			add_popularity = {
                ideology = soc_democracy
                popularity = 0.1
            }
			add_popularity = {
                ideology = rev_socialism
                popularity = 0.1
            }
			add_popularity = {
                ideology = leninism
                popularity = 0.1
            }
		}
	}

	focus = {
		id = UKR_Autocephaly_of_Ukrainian_Orthodox_Church
		icon = GFX_GENERIC_orthodoxy
		cost = 1
		prerequisite = {
			focus = UKR_Restoration_of_the_Ukrainian_Peoples_Republic
		}

		x = 2
		y = 1

		relative_position_id = UKR_Restoration_of_the_Ukrainian_Peoples_Republic

		search_filters = {FOCUS_FILTER_POLITICAL}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 2
				#is_historical_focus_on = yes
				#has_completed_focus = POL_rebirth_of_poland
			}
		}
		completion_reward = {
			add_political_power = 20
			add_stability = 0.05
		}
	}

	focus = {
		id = UKR_State_Language_of_Ukraine_Law
		icon = GFX_WUK_WUPR_Constitution
		cost = 1
		prerequisite = {
			focus = UKR_Restoration_of_the_Ukrainian_Peoples_Republic
		}

		x = -2
		y = 1

		relative_position_id = UKR_Restoration_of_the_Ukrainian_Peoples_Republic

		search_filters = {FOCUS_FILTER_POLITICAL}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 2
				#is_historical_focus_on = yes
				#has_completed_focus = POL_rebirth_of_poland
			}
		}
		completion_reward = {
			add_political_power = 20
			add_stability = 0.05
		}
	}

	focus = {
		id = UKR_Labor_Congress_of_Ukraine
		icon = GFX_reformist_front
		cost = 2
		prerequisite = {
			focus = UKR_Fourth_Congress_of_Ukrainian_Social_Democratic_Labour_Party
		}
		prerequisite = {
			focus = UKR_State_Language_of_Ukraine_Law
		}
		prerequisite = {
			focus = UKR_Autocephaly_of_Ukrainian_Orthodox_Church
		}

		x = -1
		y = 1

		relative_position_id = UKR_Fourth_Congress_of_Ukrainian_Social_Democratic_Labour_Party

		search_filters = {FOCUS_FILTER_POLITICAL}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 2
				is_historical_focus_on = yes
				#has_completed_focus = POL_land_reform_and_partial_nationalisation_manifesto
			}
		}
		completion_reward = {
			add_political_power = 100
			remove_ideas = {
                UKR_agrarian_question
            }
			swap_ideas = {
                remove_idea = no_elections
                add_idea = universal_suffrage
            }
		}
	}

	focus = {
		id = UKR_Unification_Act
		icon = GFX_treaty_citizens
		cost = 1
		prerequisite = { focus = UKR_Fourth_Congress_of_Ukrainian_Social_Democratic_Labour_Party }
		prerequisite = { focus = UKR_State_Language_of_Ukraine_Law }
		prerequisite = { focus = UKR_Autocephaly_of_Ukrainian_Orthodox_Church }

		available = {
			WUK = {exists = yes}
		}

		x = 1
		y = 1

		relative_position_id = UKR_Fourth_Congress_of_Ukrainian_Social_Democratic_Labour_Party

		search_filters = {FOCUS_FILTER_POLITICAL}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 2
				is_historical_focus_on = yes
				#has_completed_focus = POL_Strengthening_State_administration
			}
		}
		
		completion_reward = {
			add_political_power = 50
			add_stability = 0.1
			UKR = { country_event = { id = ukraine.3 } }
			hidden_effect = {
            }
		}
	}

	focus = {
		id = UKR_New_Hryvnia_Currency
		icon = GFX_goal_FRA_fiscal_reform
		cost = 1
		prerequisite = {
			focus = UKR_Fourth_Congress_of_Ukrainian_Social_Democratic_Labour_Party
		}
		prerequisite = {
			focus = UKR_Autocephaly_of_Ukrainian_Orthodox_Church
		}
		prerequisite = {
			focus = UKR_Unification_Act
		}
		prerequisite = {
			focus = UKR_Labor_Congress_of_Ukraine
		}

		x = 0
		y = 2

		relative_position_id = UKR_Autocephaly_of_Ukrainian_Orthodox_Church

		search_filters = {FOCUS_FILTER_STABILITY}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 2
				#is_historical_focus_on = yes
				#has_completed_focus = POL_rebirth_of_poland
			}
		}
		completion_reward = {
			add_stability = 0.05
		}
	}

	focus = {
		id = UKR_Negotiation_With_Entente_Forces_In_Odessa
		icon = GFX_negotiate_stability
		cost = 1
		prerequisite = {
			focus = UKR_Fourth_Congress_of_Ukrainian_Social_Democratic_Labour_Party
		}
		prerequisite = {
			focus = UKR_State_Language_of_Ukraine_Law
		}
		prerequisite = {
			focus = UKR_Unification_Act
		}
		prerequisite = {
			focus = UKR_Labor_Congress_of_Ukraine
		}

		x = 0
		y = 2

		relative_position_id = UKR_State_Language_of_Ukraine_Law

		search_filters = {FOCUS_FILTER_STABILITY}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 2
				#is_historical_focus_on = yes
				#has_completed_focus = POL_rebirth_of_poland
			}
		}
		completion_reward = {
			add_political_power = -50
		}
	}

	focus = {
		id = UKR_The_Last_Stand
		icon = GFX_goal_generic_defence
		cost = 4
		prerequisite = { focus = UKR_Labor_Congress_of_Ukraine }
		prerequisite = { focus = UKR_Unification_Act }
		#prerequisite = { focus = UKR_New_Hryvnia_Currency }
		#prerequisite = { focus = UKR_Negotiation_With_Entente_Forces_In_Odessa }

		available = {
 			198 = {
				is_controlled_by_ROOT_or_ally = yes
			}
 			surrender_progress > 0.5
 		}

		x = 1
		y = 1

		relative_position_id = UKR_Labor_Congress_of_Ukraine

		search_filters = {FOCUS_FILTER_POLITICAL}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 2
				is_historical_focus_on = yes
				#has_completed_focus = POL_Strengthening_State_administration
			}
		}
		
		completion_reward = {
			add_political_power = -75
			# Move Capital to Winnica
 			set_capital = {
				state = 198 
				remember_old_capital = yes
			}
			add_victory_points = {
				province = 476
				value = 10
			}
			add_victory_points = {
				province = 6557
				value = 4
			}
			#add_victory_points = {
			#	province = 3457
			#	value = 3
			#}
		}
	}

	focus = {
		id = UKR_Directorate_Reorganization
		icon = GFX_goal_generic_defence
		cost = 3
		prerequisite = { focus = UKR_The_Last_Stand }
		prerequisite = { focus = UKR_New_Hryvnia_Currency }
		prerequisite = { focus = UKR_Negotiation_With_Entente_Forces_In_Odessa }

		available = {
 		}

		x = 0
		y = 1

		relative_position_id = UKR_The_Last_Stand

		search_filters = {FOCUS_FILTER_POLITICAL}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 2
				is_historical_focus_on = yes
				#has_completed_focus = POL_Strengthening_State_administration
			}
		}
		
		completion_reward = {
			add_political_power = 50
			add_war_support = 0.1
			add_country_leader_role = {
				character = "UKR_symon_petlyura"
				country_leader = {
					ideology=soc_democracy_subtype
					traits = { }
					expire="1965.1.1.1"
				}
				promote_leader = yes
			}
				
			custom_effect_tooltip = ukraine.4.a.tooltip
			hidden_effect = {
				remove_ideas = {
                    UKR_volodymyr_chekhivsky_hos
                }
				add_ideas = {
                    UKR_serhiy_ostapenko_hos
                }

				remove_ideas = {
                    UKR_volodymyr_chekhivsky_for
                }
				add_ideas = {
                    UKR_kostyantyn_matsiyevych_for
                }
            }

			set_victory_points = {
	            province = 3430
	            value = 12
            }
		}
	}
}
