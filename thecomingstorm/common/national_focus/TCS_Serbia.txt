shared_focus = {
	id = SER_unification_achieved
	icon = GFX_goal_generic_accept_constitution
	cost = 5
	x = 5
	y = 0

	ai_will_do = {
		factor = 10
	}
	
	available = {
		always = no
	}

	completion_reward = {
		
	}
}

shared_focus = {
	id = SER_belgrade_cetinje_railway
	icon = GFX_goal_generic_railroad_treaty
	prerequisite = { focus = SER_unification_achieved }
	cost = 5
	x = -2
	y = 1
	relative_position_id = SER_unification_achieved

	ai_will_do = {
		factor = 10
	}
	
	available = {
		is_neighbor_of = MNT
		MNT = {
			OR = {
				is_subject_of = ROOT
				is_in_faction_with = ROOT
			}
		}
	}

	completion_reward = {
		if = {
			limit = {
				107 = {
					owner = {
						OR = {
							tag = ROOT
							is_subject_of = ROOT
							is_in_faction_with = ROOT
						}
					}
				}
			}
			107 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
		if = {
			limit = {
				823 = {
					owner = {
						OR = {
							tag = ROOT
							is_subject_of = ROOT
							is_in_faction_with = ROOT
						}
					}
				}
			}
			823 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
		if = {
			limit = {
				824 = {
					owner = {
						OR = {
							tag = ROOT
							is_subject_of = ROOT
							is_in_faction_with = ROOT
						}
					}
				}
			}
			824 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
		if = {
			limit = {
				825 = {
					owner = {
						OR = {
							tag = ROOT
							is_subject_of = ROOT
							is_in_faction_with = ROOT
						}
					}
				}
			}
			825 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
		if = {
			limit = {
				105 = {
					owner = {
						OR = {
							tag = ROOT
							is_subject_of = ROOT
							is_in_faction_with = ROOT
						}
					}
				}
			}
			105 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
	}
}

shared_focus = {
	id = SER_political_and_economic_integration
	icon = GFX_Co-Owned_businesses
	prerequisite = { focus = SER_unification_achieved }
	cost = 10
	x = 0
	y = 1
	relative_position_id = SER_unification_achieved
	
	ai_will_do = {
		factor = 10
	}
	
	available = {
		MNT = {
			OR = {
				is_subject_of = ROOT
				is_in_faction_with = ROOT
			}
		}
	}

	completion_reward = {
		add_political_power = 100
	}
}

shared_focus = {
	id = SER_serbo_montenegrin_confederation
	icon = GFX_GENERIC_parliament
	prerequisite = { focus = SER_political_and_economic_integration }
	mutually_exclusive = { focus = SER_centralized_political_entity }
	cost = 10
	x = -1
	y = 1
	relative_position_id = SER_political_and_economic_integration
	
	ai_will_do = {
		factor = 10
	}
	
	available = {
		MNT = {
			OR = {
				is_subject_of = ROOT
				is_in_faction_with = ROOT
			}
		}
	}

	completion_reward = {
		
	}
}

shared_focus = {
	id = SER_centralized_political_entity
	icon = GFX_focus_generic_monarchy_1
	prerequisite = { focus = SER_political_and_economic_integration }
	mutually_exclusive = { focus = SER_serbo_montenegrin_confederation }
	cost = 10
	x = 1
	y = 1
	relative_position_id = SER_political_and_economic_integration
	
	ai_will_do = {
		factor = 10
	}
	
	available = {
		MNT = {
			OR = {
				is_subject_of = ROOT
				is_in_faction_with = ROOT
			}
		}
	}

	completion_reward = {
		
	}
}

shared_focus = {
	id = SER_unify_military_command
	icon = GFX_army_legislation
	prerequisite = { focus = SER_unification_achieved }
	cost = 10
	x = 2
	y = 1
	relative_position_id = SER_unification_achieved
	
	ai_will_do = {
		factor = 10
	}
	
	available = {
		MNT = {
			OR = {
				is_subject_of = ROOT
				is_in_faction_with = ROOT
			}
		}
	}

	completion_reward = {
		army_experience = 15
	}
}


 ##### Serbia Main Focuses #####

shared_focus = {
  	id = SER_purchase_foreign_equipment
  	icon = GFX_goal_generic_gun_purchase
  	cost = 10
  	x = 5
 	y = 0
 	ai_will_do = {
 		base = 100
 	}
 	completion_reward = {
 		# TBD
 	}
 	search_filters = {
 		FOCUS_FILTER_INTERNAL_AFFAIRS
 		FOCUS_FILTER_INDUSTRY
 	}
}
shared_focus = {
	id = SER_open_university_of_belgrade
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = SER_purchase_foreign_equipment
	}
	x = 0
	y = 1
	ai_will_do = {
		base = 100
	}
	relative_position_id = SER_purchase_foreign_equipment
	completion_reward = {
		# TBD
	}
	search_filters = {
		FOCUS_FILTER_RESEARCH
	}
}





