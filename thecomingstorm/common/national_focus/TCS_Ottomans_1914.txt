focus_tree = {
	id = TCS_Ottomans_1914
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = OTT
		}
	}
	continuous_focus_position = { x = 0 y = 1200 }
	default = no
	initial_show_position = {
		focus = OTT_entry_into_tgw
	}
	shared_focus = OTT_entry_into_tgw
	#shared_focus = GER_victory_bulow_block
	#shared_focus = GER_stengels_undermining_of_federalism
	#shared_focus = GER_Deutsches_kolonial_reich
}
