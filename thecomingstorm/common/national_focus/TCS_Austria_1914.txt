focus_tree = {
	id = TCS_Austria_1914
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = AUS
		}
	}
	continuous_focus_position = { x = 0 y = 1200 }
	default = no
	initial_show_position = {
		focus = AUS_montenegro_invasion
	}
	shared_focus = AUS_montenegro_invasion
	#shared_focus = GER_victory_bulow_block
	#shared_focus = GER_stengels_undermining_of_federalism
	#shared_focus = GER_Deutsches_kolonial_reich
}
