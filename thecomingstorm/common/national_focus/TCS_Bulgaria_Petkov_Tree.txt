### Petkov Cabinet ###
	shared_focus = {
		id = BUL_Petkov_cabinet
		icon = GFX_BUL_Dimitar_Petkov_cabinet
		cost = 5.00
		x = 4
		y = 0
 
    	completion_reward = {
			add_political_power = 10
		}
		
	}
	shared_focus = {
		id = BUL_Dealing_with_unrest
		icon = GFX_goal_pat_aut_agreement
		cost = 2.00
		prerequisite = {
			focus = BUL_Petkov_cabinet
		}

		completion_reward = {
			add_stability = 0.03
		}
		x = 4
		y = 1
	}
	shared_focus = {
		id = BUL_increase_power_old_guilds
		icon = GFX_goal_social_conservative_agreement
		cost = 1.00
		prerequisite = {
			focus = BUL_Dealing_with_unrest
		}
		completion_reward = {
			if = {
				limit = {
					has_idea = BUL_social_unrest
				}
				swap_ideas = {
            		remove_idea = BUL_social_unrest
            		add_idea = BUL_social_unrest2
        		}
			}
			else = {
				if = {
					limit = {
						has_idea = BUL_social_unrest2
					}	
					swap_ideas = {
						remove_idea = BUL_social_unrest2
						add_idea = BUL_social_unrest3
					}
				}
				else = {
					swap_ideas = {
            			remove_idea = BUL_social_unrest
            			add_idea = BUL_social_unrest2
        			}
				}
			}
		}
		x = 3
		y = 2
	}
	shared_focus = {
		id = BUL_fine_strikers
		icon = GFX_new_taxes
		cost = 1.00
		prerequisite = {
			focus = BUL_Dealing_with_unrest
		}
		completion_reward = {
			add_political_power = -25
			if = {
				limit = {
					has_idea = BUL_social_unrest
				}
				swap_ideas = {
            		remove_idea = BUL_social_unrest
            		add_idea = BUL_social_unrest2
        		}
			}
			else = {
				if = {
					limit = {
						has_idea = BUL_social_unrest2
					}	
					swap_ideas = {
						remove_idea = BUL_social_unrest2
						add_idea = BUL_social_unrest3
					}
				}
				else = {
					swap_ideas = {
            			remove_idea = BUL_social_unrest
            			add_idea = BUL_social_unrest2
        			}
				}
			}
		}
		x = 5
		y = 2
	}

	shared_focus = {
		id = BUL_compensations_from_strikers
		icon = GFX_grab_money_ottomans
		cost = 2.00
		prerequisite = {
			focus = BUL_increase_power_old_guilds
		}
		prerequisite = {
			focus = BUL_fine_strikers
		}

		x = 2
		y = 3

		completion_reward = {
			add_political_power = -25
			if = {
				limit = {
					has_idea = BUL_social_unrest
				}
				swap_ideas = {
            		remove_idea = BUL_social_unrest
            		add_idea = BUL_social_unrest2
        		}
			}
			else = {
				if = {
					limit = {
						has_idea = BUL_social_unrest2
					}	
					swap_ideas = {
						remove_idea = BUL_social_unrest2
						add_idea = BUL_social_unrest3
					}
				}
				else = {
					if = {
						limit = {
							has_idea = BUL_social_unrest3
						}	
						swap_ideas = {
							remove_idea = BUL_social_unrest3
							add_idea = BUL_social_unrest4
						}
					}
				}
			}
		}
	}
	shared_focus = {
		id = BUL_railway_strikes
		icon = GFX_goal_generic_railroads_for_citizens
		cost = 0.00
		prerequisite = {
			focus = BUL_fine_strikers
		}
		prerequisite = {
			focus = BUL_increase_power_old_guilds
		}
		bypass = {
			has_country_flag = BUL_railway_strike_flag
		}
		available = {
			custom_trigger_tooltip = {
				always = no
				tooltip = unlock_focuses
			}
		}
		x = 4
		y = 3
	}
	shared_focus = {
		id = BUL_deescalate_situation
		icon = GFX_empower_army
		cost = 1.00
		prerequisite = {
			focus = BUL_railway_strikes
		}

		x = 3
		y = 4

		completion_reward = {
			add_political_power = -50
			remove_ideas = { BUL_railway_strike_idea }
		}
	}	
	shared_focus = {
		id = BUL_university_crisis
		#icon = 
		cost = 0.00
		prerequisite = {
			focus = BUL_fine_strikers
		}
		bypass = {
			has_country_flag = BUL_university_crisis_flag
		}
		available = {
			custom_trigger_tooltip = {
				always = no
				tooltip = unlock_focuses
			}
		}
		x = 6
		y = 3
	}
	shared_focus = {
		id = BUL_introduce_press_censorship
		icon = GFX_discredit_the_press_2
		cost = 2.00
		prerequisite = {
			focus = BUL_university_crisis
		}
		x = 5
		y = 4

		completion_reward = {
			if = {
				limit = {
					has_idea = BUL_opposition_bloc
				}
				swap_ideas = {
            		remove_idea = BUL_opposition_bloc
            		add_idea = BUL_opposition_bloc2
        		}
			}
			else = {
				if = {
					limit = {
						has_idea = BUL_opposition_bloc2
					}	
					swap_ideas = {
						remove_idea = BUL_opposition_bloc2
						add_idea = BUL_opposition_bloc3
					}
				}
				else = {
					swap_ideas = {
            			remove_idea = BUL_opposition_bloc
            			add_idea = BUL_opposition_bloc2
        			}
				}
			}
		}
	}

	shared_focus = {
		id = BUL_cleanse_education_antigovernment
		icon = GFX_sponsor_scholarships
		cost = 2.00
		prerequisite = {
			focus = BUL_university_crisis
		}

		x = 7
		y = 4

		completion_reward = {
			if = {
				limit = {
					has_idea = BUL_opposition_bloc
				}
				swap_ideas = {
            		remove_idea = BUL_opposition_bloc
            		add_idea = BUL_opposition_bloc2
        		}
			}
			else = {
				if = {
					limit = {
						has_idea = BUL_opposition_bloc2
					}	
					swap_ideas = {
						remove_idea = BUL_opposition_bloc2
						add_idea = BUL_opposition_bloc3
					}
				}
				else = {
					swap_ideas = {
            			remove_idea = BUL_opposition_bloc
            			add_idea = BUL_opposition_bloc2
        			}
				}
			}
		}
	}	

	shared_focus = {
		id = BUL_declare_martial_law
		icon = GFX_goal_auth_dem_agreement
		cost = 1.00
		prerequisite = {
			focus = BUL_cleanse_education_antigovernment
		}
		prerequisite = {
			focus = BUL_introduce_press_censorship
		}
		available = {
			always = no
		}

		x = 6
		y = 5

		completion_reward = {
			custom_effect_tooltip = BUL_civil_war_tt
		}
	}