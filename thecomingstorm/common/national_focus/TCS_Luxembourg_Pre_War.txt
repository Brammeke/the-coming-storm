#!gfx:interface\KR_goals.gfx

shared_focus = {
	id = LUX_1904_census
	icon = GFX_goal_LUX_1904_national_census
	cost = 7.15
	x = 1
	y = 0

	available = {
	}

	completion_reward = {
	   add_political_power = 15
	   add_manpower = 25
	}
}

shared_focus = {
	id = LUX_industrial_renewal_act
	icon = GFX_GENERIC_industrial_act
    prerequisite = { focus = LUX_1904_census }
	cost = 7.15
	x = -1
	y = 1
    relative_position_id = LUX_1904_census
	available = {
	}

	completion_reward = {
		add_tech_bonus = {
			bonus = 0.20
			uses = 1
			category = industry
		}
    }
}

shared_focus = {
	id = LUX_fund_the_national_library
	icon = GFX_higher_salary
    prerequisite = { focus = LUX_1904_census }
	cost = 7.15
	x = 1
	y = 1
    relative_position_id = LUX_1904_census
	available = {
	}

	completion_reward = {
		add_tech_bonus = {
			bonus = 0.20
			uses = 1
			category = electronics
		}
    }
}

shared_focus = {
	id = LUX_coronation_of_william_iv
	icon = GFX_goal_LUX_coronation_of_william_iv
	prerequisite = { focus = LUX_industrial_renewal_act }
	prerequisite = { focus = LUX_fund_the_national_library }
	cost = 1
	x = 0
	y = 2
    relative_position_id = LUX_1904_census
	available = {
	}

	completion_reward = {
    }
}

shared_focus = {
	id = LUX_review_the_gendarmerie
	icon = GFX_focus_generic_military_academy
	available = {
		has_completed_focus = LUX_coronation_of_william_iv
	}
	cost = 7.15
	x = 13
	y = 0
    #relative_position_id = LUX_coronation_of_william_iv

	completion_reward = {
        army_experience = 20
        add_command_power = 15
    }
}

shared_focus = {
	id = LUX_purchase_modern_equipment
	icon = GFX_goal_generic_small_arms
	prerequisite = { focus = LUX_review_the_gendarmerie }
	cost = 7.15
	x = -1
	y = 1
    relative_position_id = LUX_review_the_gendarmerie
	available = {
	}

	completion_reward = {
		add_tech_bonus = {
			name = support_bonus
			bonus = 0.2
			uses = 1
			category = support_tech
		}
    }
}

shared_focus = {
	id = LUX_modernise_our_law_enforcement
	icon = GFX_focus_generic_military_mission
	prerequisite = { focus = LUX_review_the_gendarmerie }
	cost = 7.15
	x = 1
	y = 1
    relative_position_id = LUX_review_the_gendarmerie
	available = {
	}

	completion_reward = {
        add_stability = 0.01
    }
}

shared_focus = {
	id = LUX_extensive_military_exercises
	icon = GFX_focus_generic_little_entente
	prerequisite = { focus = LUX_purchase_modern_equipment }
	prerequisite = { focus = LUX_modernise_our_law_enforcement }
	cost = 7.15
	x = 0
	y = 2
    relative_position_id = LUX_review_the_gendarmerie
	available = {
	}

	completion_reward = {
        army_experience = 50
    }
}

shared_focus = {
	id = LUX_expand_our_volunteer_company
	icon = GFX_goal_generic_allies_build_infantry
	prerequisite = { focus = LUX_extensive_military_exercises }
	cost = 7.15
	x = 0
	y = 1
    relative_position_id = LUX_extensive_military_exercises
	available = {
	}

	completion_reward = {
		#custom_effect_tooltip = LUX_expand_our_volunteer_company_tt
		hidden_effect = {
			division_template = {
				name = "1. Kinneklech Groussherzoglech Gendarmeriedivisioun"
				priority = 1
				division_names_group = LUX_INF_01
				regiments = {
					infantry = { x = 0 y = 0 }		
				}
			}
		}
		random_owned_controlled_state = {
			limit = { ROOT = { has_full_control_of_state = PREV } }
			prioritize = { 8 }
			create_unit = {
				division = "name = \"1. Kinneklech Groussherzoglech Gendarmeriedivisioun\" division_template = \"1. Kinneklech Groussherzoglech Gendarmeriedivisioun\" start_experience_factor = 0.5" 
				owner = LUX
			}
		}			
	}			
}

shared_focus = {
	id = LUX_develop_a_new_service_kit
	icon = GFX_goal_generic_small_arms
	prerequisite = { focus = LUX_expand_our_volunteer_company }
	cost = 7.15
	x = -1
	y = 1
    relative_position_id = LUX_expand_our_volunteer_company
	available = {
	}

	completion_reward = {
	    add_tech_bonus = {
			name = infantry_weapons_bonus
			bonus = 0.20
			uses = 1
			category = infantry_weapons
		}
	}
}

shared_focus = {
	id = LUX_invite_boer_war_veterans
	icon = GFX_focus_generic_military_academy
	prerequisite = { focus = LUX_develop_a_new_service_kit }
	cost = 7.15
	x = -1
	y = 2
    relative_position_id = LUX_expand_our_volunteer_company
	available = {
	}

	completion_reward = {
	    add_tech_bonus = {
			name = land_doc_bonus
			bonus = 0.2
			uses = 1
			category = land_doctrine
		}
	}
}

shared_focus = {
	id = LUX_purchase_automobiles
	icon = GFX_GENERIC_motorized
	prerequisite = { focus = LUX_expand_our_volunteer_company }
	cost = 7.15
	x = 1
	y = 1
    relative_position_id = LUX_expand_our_volunteer_company
	available = {
	}

	completion_reward = {

	}
}

shared_focus = {
	id = LUX_emergency_volunteers_list
	icon = GFX_focus_generic_treaty
	prerequisite = { focus = LUX_purchase_automobiles }
	cost = 7.15
	x = 1
	y = 2
    relative_position_id = LUX_expand_our_volunteer_company
	available = {
	}

	completion_reward = {
		add_manpower = 2000
	}
}

shared_focus = {
	id = LUX_truely_ducal_army
	icon = GFX_focus_generic_home_defense
	prerequisite = { focus = LUX_invite_boer_war_veterans }
	prerequisite = { focus = LUX_emergency_volunteers_list }
	cost = 7.15
	x = 0
	y = 3
    relative_position_id = LUX_expand_our_volunteer_company
	available = {
	}

	completion_reward = {
		army_experience = 5
		add_tech_bonus = {
			name = infantry_weapons_bonus
			bonus = 0.20
			uses = 1
			category = infantry_weapons
		}
	}
}

shared_focus = {
	id = LUX_reconvene_the_chamber
	icon = GFX_GENERIC_parliament
	cost = 7.15
	x = 0
	y = 1
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_coronation_of_william_iv
	}
	relative_position_id = LUX_coronation_of_william_iv
	
	completion_reward = {
		country_event = {
			id = LUX_political.4
			hours = 2
		}
	}
}

shared_focus = {
	id = LUX_construction_conservatory
	icon = GFX_goal_generic_production
	cost = 7.15
	x = -1
	y = 1
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_reconvene_the_chamber
	}
	relative_position_id = LUX_reconvene_the_chamber
	
	completion_reward = {
		add_political_power = 25
		country_event = {
			id = LUX_flavor.1
			hours = 2
		}
	}
}

shared_focus = {
	id = LUX_loosen_press_restrictions
	icon = GFX_GENERIC_signing_document
	cost = 7.15
	x = 1
	y = 1
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_reconvene_the_chamber
	}
	relative_position_id = LUX_reconvene_the_chamber
	
	completion_reward = {
		custom_effect_tooltip = GENERIC_press_laws_tt
	}
}

shared_focus = {
	id = LUX_address_secularization
	icon = GFX_GENERIC_parliament
	cost = 7.15
	x = 7
	y = 0
	available = {
		AND = {
			has_completed_focus = LUX_loosen_press_restrictions
			has_completed_focus = LUX_construction_conservatory
		}
	}
	bypass = {}
	allow_branch = {}
	
	completion_reward = {
		country_event = {
			id = LUX_political.1
			hours = 2
		}
	}
}

shared_focus = {
	id = LUX_league_alone
	icon = GFX_goal_support_democracy
	cost = 7.15
	x = 1
	y = 1
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_address_secularization
	}
	mutually_exclusive = {
		focus = LUX_lenksblock
	}
	relative_position_id = LUX_address_secularization
	
	completion_reward = {
		custom_effect_tooltip = LUX_league_alone_tt
	}
}

shared_focus = {
	id = LUX_lenksblock
	icon = GFX_goal_generic_improve_relations
	cost = 7.15
	x = -1
	y = 1
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_address_secularization
	}
	mutually_exclusive = {
		focus = LUX_league_alone
	}
	relative_position_id = LUX_address_secularization
	
	completion_reward = {
		set_country_flag = soc_democracy_coalition
		custom_effect_tooltip = LUX_add_socialists_coalition_tt
	}
}

shared_focus = {
	id = LUX_worker_safety_laws
	icon = GFX_GENERIC_law_right_of_assembly
	cost = 7.15
	x = -1
	y = 1
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_lenksblock
	}
	relative_position_id = LUX_lenksblock
	
	completion_reward = {
		custom_effect_tooltip = LUX_improve_schoulkampf_tt
		hidden_effect = {
			LUX_schoulkampf_increase_lenksblock = yes
		}
	}
}

shared_focus = {
	id = LUX_business_tax_reforms
	icon = GFX_GENERIC_signing_document
	cost = 7.15
	x = 1
	y = 1
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_league_alone
	}
	relative_position_id = LUX_league_alone
	
	completion_reward = {
		custom_effect_tooltip = LUX_improve_schoulkampf_tt
		hidden_effect = {
			LUX_schoulkampf_increase_league = yes
		}
	}
}

shared_focus = {
	id = LUX_curtail_church_influence
	icon = GFX_focus_rom_parties_end
	cost = 7.15
	x = 0
	y = 2
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_league_alone
		focus = LUX_lenksblock
	}
	relative_position_id = LUX_address_secularization
	
	completion_reward = {
		add_stability = 0.01
		set_party_name = { 
			ideology = soc_conservatism 
			long_name = LUX_soc_conservatism_party_rietsblock_long 
			name = LUX_soc_conservatism_party_rietsblock 
		}
		set_party_name = { 
			ideology = auth_democrat 
			long_name = LUX_auth_democrat_party_rietsblock_long 
			name = LUX_auth_democrat_party_rietsblock 
		}
	}
}

shared_focus = {
	id = LUX_pressure_companies
	icon = GFX_goal_tfv_saf_anti_colonialist_crusade
	cost = 7.15
	x = 0
	y = 2
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_business_tax_reforms
	}
	prerequisite = {
		focus = LUX_curtail_church_influence
	}
	relative_position_id = LUX_league_alone
	
	completion_reward = {
		add_stability = 0.01
		add_political_power = 10
	}
}

shared_focus = {
	id = LUX_attempt_reform
	icon = GFX_GENERIC_voting
	cost = 7.15
	x = 0
	y = 3
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_pressure_companies
	}
	relative_position_id = LUX_league_alone
	
	completion_reward = {
		add_political_power = 25
		country_event = {
			id = LUX_political.2
			hours = 2
		}
	}
}

shared_focus = {
	id = LUX_insurance_less_fortunate
	icon = GFX_GENERIC_worker_housing
	cost = 7.15
	x = 0
	y = 2
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_worker_safety_laws
	}
	prerequisite = {
		focus = LUX_curtail_church_influence
	}
	relative_position_id = LUX_lenksblock
	
	completion_reward = {
		add_stability = 0.01
		add_political_power = 15
	}
}

shared_focus = {
	id = LUX_attempt_reform_lenksblock
	icon = GFX_GENERIC_law
	cost = 7.15
	x = 0
	y = 3
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_insurance_less_fortunate
	}
	relative_position_id = LUX_lenksblock
	
	completion_reward = {
		add_political_power = 20
		country_event = {
			id = LUX_political.3
			hours = 2
		}
	}
}

shared_focus = {
	id = LUX_end_school_fees
	icon = GFX_focus_renounce_the_treaty_of_trianon
	cost = 7.15
	x = -1
	y = 1
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_attempt_reform_lenksblock
	}
	relative_position_id = LUX_attempt_reform_lenksblock
	
	completion_reward = {
		custom_effect_tooltip = LUX_improve_schoulkampf_tt
		hidden_effect = {
			LUX_schoulkampf_increase_lenksblock = yes
		}
	}
}

shared_focus = {
	id = LUX_girl_schools
	icon = GFX_focus_generic_self_management
	cost = 7.15
	x = -1
	y = 2
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_end_school_fees
	}
	relative_position_id = LUX_attempt_reform_lenksblock
	
	completion_reward = {
		custom_effect_tooltip = LUX_improve_schoulkampf_tt
		hidden_effect = {
			LUX_schoulkampf_increase_lenksblock = yes
		}
	}
}

shared_focus = {
	id = LUX_end_compulsory_religious_education
	icon = GFX_goal_generic_forceful_treaty
	cost = 7.15
	x = 1
	y = 1
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_attempt_reform_lenksblock
		focus = LUX_attempt_reform
	}
	relative_position_id = LUX_attempt_reform_lenksblock
	
	completion_reward = {
		custom_effect_tooltip = LUX_improve_schoulkampf_tt
		if = {
			limit = {
				has_completed_focus = LUX_lenksblock
			}
			hidden_effect = {
				LUX_schoulkampf_increase_lenksblock = yes
			}
		}
		else_if = {
			limit = {
				has_completed_focus = LUX_league_alone
			}
			hidden_effect = {
				LUX_schoulkampf_increase_league = yes
			}
		}
	}
}

shared_focus = {
	id = LUX_end_church_supervision
	icon = GFX_focus_generic_concessions
	cost = 7.15
	x = 1
	y = 2
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_end_compulsory_religious_education
	}
	relative_position_id = LUX_attempt_reform_lenksblock
	
	completion_reward = {
		custom_effect_tooltip = LUX_improve_schoulkampf_tt
		if = {
			limit = {
				has_completed_focus = LUX_lenksblock
			}
			hidden_effect = {
				LUX_schoulkampf_increase_lenksblock = yes
			}
		}
		else_if = {
			limit = {
				has_completed_focus = LUX_league_alone
			}
			hidden_effect = {
				LUX_schoulkampf_increase_league = yes
			}
		}
	}
}

shared_focus = {
	id = LUX_limit_school_fees
	icon = GFX_goal_generic_national_unity
	cost = 7.15
	x = 1
	y = 1
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_attempt_reform
	}
	relative_position_id = LUX_attempt_reform
	
	completion_reward = {
		custom_effect_tooltip = LUX_improve_schoulkampf_tt
		hidden_effect = {
			LUX_schoulkampf_increase_league = yes
		}
	}
}

shared_focus = {
	id = LUX_liberal_teachings
	icon = GFX_goal_support_democracy
	cost = 7.15
	x = 1
	y = 2
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_limit_school_fees
	}
	relative_position_id = LUX_attempt_reform
	
	completion_reward = {
		custom_effect_tooltip = LUX_improve_schoulkampf_tt
		hidden_effect = {
			LUX_schoulkampf_increase_league = yes
		}
	}
}

shared_focus = {
	id = LUX_pass_school_reforms
	icon = GFX_GENERIC_law
	cost = 7.15
	x = -1
	y = 1
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_girl_schools
	}
	prerequisite = {
		focus = LUX_end_church_supervision
	}
	relative_position_id = LUX_end_church_supervision
	
	completion_reward = {
		swap_ideas = {
			remove_idea = LUX_schoulkampf_5
			add_idea = LUX_secularized_schools
		}
	}
}

shared_focus = {
	id = LUX_pass_school_reforms_league
	icon = GFX_GENERIC_law
	cost = 7.15
	x = 1
	y = 1
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_liberal_teachings
	}
	prerequisite = {
		focus = LUX_end_church_supervision
	}
	relative_position_id = LUX_end_church_supervision
	
	completion_reward = {
		swap_ideas = {
			remove_idea = LUX_schoulkampf_5_league
			add_idea = LUX_liberalized_schools
		}
	}
}

shared_focus = {
	id = LUX_open_constitutional_debate
	icon = GFX_GENERIC_parliament
	cost = 7.15
	x = 1
	y = 1
	available = {}
	bypass = {}
	allow_branch = {}
	prerequisite = {
		focus = LUX_pass_school_reforms
		focus = LUX_pass_school_reforms_league
	}
	relative_position_id = LUX_pass_school_reforms
	
	completion_reward = {
		#new tree
	}
}