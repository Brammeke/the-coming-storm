scripted_gui = {

	TCS_welcome_splash = {
		context_type = player_context
		window_name = "TCS_welcome_splash_container"

		visible = {
			has_country_flag = show_welcome_splash
		}

		effects = {
			continue_button_click = {
				clr_country_flag = show_welcome_splash
				#country_event = kr.political.1
			}
			credits_button_click = {
				if = {
					limit= { check_variable = { show = 1 } }
					clear_variable = show
					else = {
						set_variable = { show = 1 }
					}
				}
			}
		}
	}
}
