RWA_INF_01 = {
	name = "Ingabo zo mu bwoko"

	for_countries = { RWA }

	can_use = { always = yes }

	division_types = { "infantry" }

	fallback_name = "%d Ingabo zo mu bwoko"

	ordered = {
		1 = { "Ingabo zo mu bwoko 'Kigali'" }
		2 = { "Ingabo zo mu bwoko 'Gatsibo'" }
		3 = { "Ingabo zo mu bwoko 'Bugesera'" }
		4 = { "Ingabo zo mu bwoko 'Kirehe'" }
		5 = { "Ingabo zo mu bwoko 'Kayonza'" }
		6 = { "Ingabo zo mu bwoko 'Ngoma'" }
		7 = { "Ingabo zo mu bwoko 'Nyagatare'" }
		8 = { "Ingabo zo mu bwoko 'Rwamagana'" }
	}
}