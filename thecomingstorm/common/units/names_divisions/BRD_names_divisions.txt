BRD_INF_01 = {
	name = "Ubg-okō In-gabo"

	for_countries = { BRD }

	can_use = { always = yes }

	division_types = { "infantry" }

	fallback_name = "%d Ubg-okō In-gabo"

	ordered = {
		1 = { "Ubg-okō In-gabo 'Bujumbura'" }
		2 = { "Ubg-okō In-gabo 'Bururi'" }
		3 = { "Ubg-okō In-gabo 'Cankuzo'" }
		4 = { "Ubg-okō In-gabo 'Cibitoke'" }
		5 = { "Ubg-okō In-gabo 'Gitega'" }
		6 = { "Ubg-okō In-gabo 'Karuzi'" }
		7 = { "Ubg-okō In-gabo 'Kayanza'" }
		8 = { "Ubg-okō In-gabo 'Kirundo'" }
	}
}