IRE_MIL_01 = {
	name = "Workers' Militia"

	for_countries = { IRE }

	can_use = { always = yes }

	division_types = { "infantry" }

	fallback_name = "%d Brigade"

	ordered = {
		1 = { "Dublin Brigade" }
		2 = { "Belfast Brigade" }
		3 = { "Cork Brigade" }
	}
}

IRE_VOL_01 = {
	name = "Irish Volunteers"

	for_countries = { IRE }

	can_use = { always = yes }

	division_types = { "infantry" }

	fallback_name = "%d Brigade"

	ordered = {
		1 = { "Dublin Brigade" }
	}
}
