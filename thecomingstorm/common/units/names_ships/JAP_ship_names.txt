﻿##### JAPAN NAME LISTS #####
### DESTROYER NAMES###
JAP_DD_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_DESTROYERS

	for_countries = { JAP }

	type = ship
	ship_types = { ship_hull_light destroyer }			

	fallback_name = "Kuchikukan %d -go"					

	unique = {	
		#Shiratsuyu class
		"Shiratsuyu" "Shigure" "Murasame" "Yudachi" "Kawakaze" "Umikaze" "Samidare" "Yamakaze" "Harusame" "Suzukaze" 
		"Asashio" "Arashio" "Oshio" "Michishio" "Yamagumo" "Natsugumo" "Minegumo" "Asagumo" "Arare" "Kasumi" 
		"Kagero" "Shiranui" "Kuroshio" "Yukikaze" "Oyashio" "Hayashio" "Natsushio" "Hatsukaze" "Amatsukaze" "Isokaze" "Tokitsukaze" "Urakaze" "Arashi" "Hagikaze" "Nowaki" "Tanikaze" "Hamakaze" "Maikaze" 
		"Akigumo" "Yugumo" "Makigumo" "Kazagumo" "Naganami" "Makinami" "Takanami" "Onami" "Kiyonami" "Tamanami" "Suzunami" "Fujinami" "Hayanami" "Hamanami" "Asashimo" "Okinami" "Kishinami" 
		"Hayashimo" "Akishimo" "Kiyoshimo" 
		# Starting with Akizuki class
		"Akizuki" "Teruzuki" "Suzutsuki" "Hatsuzuki" "Niizuki" "Wakatsuki" "Shimotsuki" "Fuyutsuki" "Harutsuki" "Hanazuki" "Yoizuki" "Natsuzuki" 
		"Shimakaze" "Michitsuki" 
		# Cancelled/not completed
		"Umigiri" "Yamagiri" "Tanigiri" "Kawagiri" "Taekaze" "Kiyokaze" "Satokaze" "Murakaze" "Yamasame" "Akisame" "Natsusame" "Hayasame" "Takashio" "Akishio" "Harushio" "Wakashio" 
		"Michitsuki" "Kiyotsuki" "Otsuki" "Hazuki" "Yamazuki" "Urazuki" "Aogumo" "Benigumo" "Harugumo" "Amagumo" "Yaegumo" "Fuyugumo" "Yukigumo" "Okitsukaze" "Shimokaze" "Asagochi" 
		"Okaze" "Kochi" "Nishikaze" "Hae" "Kitakaze" "Hayakaze" "Natsukaze" "Fuyukaze" "Hatsunatsu" "Hatsuaki" "Hayaharu" "Hayaume" "Katsura" "Tobiume" 
		"Fuji" "Wakazakura" "Yamazakura" "Ashi" "Shinodake" "Yomogi" "Aoi" "Shiraume" "Kiku" "Kashiwa" "Kigiku" "Hatsugiku" "Akane" "Shiragiku" "Chigusa" "Natsugusa" "Akikusa" "Nogiku" "Susuki" 
		# Older Models
		"Momo" "Kashi" "Hinoki" "Yanagi" "Enoki" "Nara" 
		"Momi" "Kaya" "Nashi" "Take" "Kaki" "Tsuga" "Nire" "Kuri" "Hagi" "Hasu" "Warabi" "Tade" "Sumire" "Tsuta" "Ashi" "Wakatake" "Kuretake" "Sanae" "Sarawabi" "Asagao" "Yugao" 
		"Fuyo" "Karukaya" "Minekaze" "Sawakaze" "Okikaze" "Shimakaze" "Nadakaze" "Yakaze" "Hakaze" "Shiokaze" "Akikaze" "Yukaze" "Tachikaze" "Hokaze" "Nokaze" "Namikaze" "Numakaze" 
		"Kamikaze" "Asakaze" "Harukaze" "Matsukaze" "Hatakaze" "Oite" "Hayate" "Asanagi" "Yunagi" "Mutsuki" "Kisaragi" "Yayoi" "Uzuki" "Satsuki" "Minazuki" "Fumizuki" "Nagatsuki" 
		"Kikuzuki" "Mikazuki" "Mochizuki" "Yuzuki" "Fubuki" "Shirayuki" "Hatsuyuki" "Miyuki" "Murakumo" "Shinonome" "Usugumo" "Shirakumo" "Isonami" "Uranami" "Ayanami" "Shikinami" 
		"Asagiri" "Yugiri" "Amagiri" "Sagiri" "Oboro" "Akebono" "Sazanami" "Ushio" "Akatsuki" "Hibiki" "Ikazuchi" "Inazuma" "Hatsuharu" "Nenohi" "Wakaba" "Hatsushimo" "Ariake" "Yugure" 
	}
}

### PROTECTED/LIGHT CRUISER NAMES###
JAP_CL_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CL

	for_countries = { JAP }

	type = ship
	ship_types = { ship_hull_cruiser light_cruiser }
	
	fallback_name = "Keijun'yokan %d -go"

	unique = {
		#Naniwa Class
		"Naniwa" "Takachiho"
		#Matsushima Class
		"Itsukushima" "Matsushima" "Hashidate"
		#Chiyoda Class
		"Chiyoda"
		#Akitsushima Class
		"Akitsushima"
		#Suma Class
		"Suma" "Akashi"
		#Kasagi Class
		"Kasagi" "Chitose"
		#Niitaka Class
		"Niitaka" "Tsushima"
		#Otowa Class
		"Otowa"
		#Tone Class
		"Tone"
		#Yodo Class
		"Yodo" "Mogami"
		#Chikuma Class
		"Chikuma" "Yahagi" "Hirado"
		# Tenryū Class
		"Tenryū" "Tatsuta"
		#Kuma Class
		"Kuma" "Tama" "Kitakami" "Ōi" "Kiso"
		#Nagara Class
		"Nagara" "Isuzu" "Natori" "Yura" "Kinu" "Abukuma"
		#Sendai Class
		"Sendai" "Jintsū" "Naka" "Kako" "Ayase"  "Minase" "Otonase"
		#Yūbari Class
		"Yūbari"
	}
}

### ARMORED/HEAVY CRUISER NAMES###
JAP_CA_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CA

	for_countries = { JAP }

	type = ship
	ship_types = { ship_hull_cruiser heavy_cruiser }
	
	fallback_name = "Jun'yokan %d -go"

	unique = {
		#Asama Class
		"Asama" "Tokiwa"
		#Izumo Class
		"Izumo" "Iwate"
		#Yakumo Class
		"Yakumo"
		#Azuma Class
		"Azuma"
		#Kasuga Class
		"Kasuga" "Nisshin"
		#Furutaka Class
		"Furutaka" "Kako"
		#Aoba Class
		"Aoba" "Kinugasa"
		#Myōkō Class
		"Myōkō" "Nachi" "Haguro" "Ashigara"
		#Takao Class
		"Takao" "Atago" "Maya" "Chōkai"
		#Mogami Class
		"Mogami" "Mikuma"
		#Suzuya Class
		"Suzuya" "Kumano"
		#Tone Class
		"Tone" "Chikuma"	
	}
}

### BATTLESHIP NAMES ###
JAP_BB_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_BB

	for_countries = { JAP }

	type = ship
	ship_types = { ship_hull_heavy battleship }
	
	fallback_name = "Senkan %d -go"		

	unique = {
		#Fuji Class
		"Fuji" "Yashima"
		#Shikishima Class
		"Shikishima" "Hatsuse"
		#Asahi Class
		"Asahi"
		#Mikasa Class
		"Mikasa"
		#Katori Class
		"Katori" "Kashima"
		#Satsuma Class
		"Satsuma" "Aki"
		#Kawachi Class
		"Kawachi" "Settsu"
		#Fusō Class
		"Fusō" "Yamashiro"
		#Ise Class
		"Ise" "Hyūga"
		#Nagato Class
		"Nagato" "Mutsu"
		#Tosa Class
		"Tosa" "Kaga"
		#Kii Class
		"Kii" "Owari"
	}
}

### BATTLECRUISER NAMES ###
JAP_BC_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_BC

	for_countries = { JAP }

	type = ship
	ship_types = { ship_hull_heavy battle_cruiser }
	
	fallback_name = "Jun'yosenkan %d -go"		

	unique = {
		#Tsukuba Class
		"Tsukuba" "Ikoma"
		#Ibuki Class
		"Ibuki" "Kurama"
		#Kongō Class
		"Kongō" "Hiei" "Haruna" "Kirishima"
		#Amagi
		"Amagi" "Akagi" "Atago" "Takao"
	}
}

### AIRCRAFT CARRIER NAMES ###
JAP_CV_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CARRIERS

	for_countries = { JAP }

	type = ship
	ship_types = { ship_hull_carrier carrier }
	
	fallback_name = "Kokubokan %d -go"	

	unique = {
			"Akagi" "Soryu" "Hiryu" "Shokaku" "Zuikaku" "Junyo" "Hiyo" "Taiho" "Unryu" "Amagi" "Katsuragi" "Shinano" "Kasagi CV" "Aso" "Ikoma" 
			"Kurama" "Kaga" 
	}
}

### SUBMARINES ###
JAP_SS_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_SUBMARINES

	for_countries = { JAP }

	type = ship
	ship_types = { ship_hull_submarine submarine }
	
	fallback_name = "I-%d"
	
	ordered = {
		51 = {"I-%d"}
		52 = {"I-%d"}
		1 = {"I-%d"}
		2 = {"I-%d"} 
		3 = {"I-%d"} 
		4 = {"I-%d"} 
		53 = {"I-%d"}
		54 = {"I-%d"}
		55 = {"I-%d"}
		58 = {"I-%d"}
		121 = {"I-%d"}
		122 = {"I-%d"}
		123 = {"I-%d"}
		124 = {"I-%d"}
		56 = {"I-%d"}
		57 = {"I-%d"}
		59 = {"I-%d"}
		60 = {"I-%d"}
		63 = {"I-%d"}
		61 = {"I-%d"}
		62 = {"I-%d"}
		64 = {"I-%d"}
		65 = {"I-%d"}
		66 = {"I-%d"}
		67 = {"I-%d"}
		5 = {"I-%d"} 
		68 = {"I-%d"}
		69 = {"I-%d"}
		70 = {"I-%d"}
		71 = {"I-%d"}
		72 = {"I-%d"}
		73 = {"I-%d"}
		6 = {"I-%d"} 
		7 = {"I-%d"} 
		8 = {"I-%d"} 
		74 = {"I-%d"}
		75 = {"I-%d"}
		9 = {"I-%d"} 
		10 = {"I-%d"}
		11 = {"I-%d"} 
		12 = {"I-%d"}
		13 = {"I-%d"}
		14 = {"I-%d"}
		15 = {"I-%d"}
		16 = {"I-%d"}
		17 = {"I-%d"}
		18 = {"I-%d"}
		19 = {"I-%d"}
		20 = {"I-%d"}
		21 = {"I-%d"}
		22 = {"I-%d"}
		23 = {"I-%d"}
		24 = {"I-%d"}
		25 = {"I-%d"}
	}
}


### THEME: JAPANESE PREFECTURES ###
JAP_PREFECTURES = {
	name = NAME_THEME_PREFECTURES

	for_countries = { JAP }

	type = ship

	unique = {
		"Yamashiro" "Kawachi" "Izumi" "Settsu" "Iga" "Ise" "Shima" "Owari" "Mikawa" "Totomi" "Suruga" "Izu" "Kai" "Sagami" "Musashi" "Awa" "Kazusa" 
		"Shimosa" "Hitachi" "Omi" "Mino" "Hida" "Shinano" "Kozuke" "Shimotsuke" "Dewa" "Mutsu" "Wakasa" "Echizen" "Kaga" "Noto" "Etchu" "Echigo" 
		"Sado" "Tanba" "Tango" "Tajima" "Inaba" "Hoki" "Izumo" "Iwami" "Oki" "Harima" "Mimasaka" "Bizen" "Bitchu" "Bingo" "Aki" "Suo" "Nagato" 
		"Kii" "Awaji" "Awa" "Sanuki" "Iyo" "Tosa" "Buzen" "Chikuzen" "Chikugo" "Hizen" "Higo" "Hyuga" "Osumi" "Satsuma" "Iki" "Tsushima" "Oshima" 
		"Shiribeshi" "Iburi" "Ishikari" "Teshio" "Kitami" "Hidaka" "Tokachi" "Kushiro" "Nemuro" "Chishima" 
		"Hokkaido" "Aichi" "Ehime" "Gunma" "Hyogo" "Ishikawa" "Iwate" "Kagawa" "Kanagawa" "Mie" "Miyagi" "Okinawa" "Shiga" "Shimane" "Tochigi" "Yamanashi"
	}
}
## THEME: JAPANESE CITIES ###
JAP_CITIES = {
	name = NAME_THEME_CITIES

	for_countries = { JAP }

	type = ship

	unique = {
		"Tokyo" "Yokohama" "Osaka" "Nagoya" "Sapporo" "Kobe" "Kyoto" "Fukuoka" "Kawasaki" "Saitama" "Hiroshima" "Sendai" "Kitakyushu" "Chiba" "Setagaya" 
		"Sakai" "Niigata" "Hamamatsu" "Shizuoka" "Sagamihara" "Nerima" "Okayama" "Oita" "Kumamoto" "Edogawa" "Adachi" "Kagoshima" "Funabashi" "Hachioji" 
		"Kawaguchi" "Himeji" "Suginami" "Itabashi" "Matsuyama" "Higashiosaka" "Utsunomiya" "Matsudo" "Nishinomiya" "Kurashiki" "Ichikawa" "Fukuyama" 
		"Amagasaki" "Kanazawa" "Nagasaki" "Koto" "Katsushika" "Yokusaka" "Toyama" "Toyota" "Takamatsu" "Machida" "Gifu" "Hirakata" "Fujisawa" "Kashiwa" 
		"Toyonaka" "Nagano" "Toyohashi" "Ichinomiya" "Wakayama" "Okazaki" "Miyazaki" "Nara" "Suita" "Takatsuki" "Shinagawa" "Asahikawa" "Iwaki" "Kochi" 
		"Takasaki" "Koriyama" "Tokorozawa" "Kawagoe" "Kita" "Akita" "Otsu" "Koshigaya" "Maebashi" "Naha" "Nakano" "Shinjuku" "Yokaichi" "Aomori" "Kurume" 
		"Kasugai" "Morioka" "Akashi" "Fukushima" "Tsu" "Shimonoseki" "Nagaoka" "Ichihara" "Hakodate" "Yao" "Ibaraki" "Fukui" "Meguro" "Kakogawa" "Tokushima" 
		"Mito" "Hiratsuka" "Toshima" "Yamagata" "Sasebo" "Fuchu" "Kure" "Hachinohe" "Saga" "Neyagawa" "Soka" "Sumida" "Fuji" "Kasukabe" "Chigasaki" "Matsumoto"   
		"Atsugi" "Yamato" "Ageo" "Takarazuka" "Chofu" "Ota" "Tsukuba" "Numazu" "Joetsu" "Shibuya" "Minato" "Kumagaya" "Isesaki" "Kishiwada" "Tottori" "Kofu" 
		"Odawara" "Suzuka" "Matsue" "Hitachi" "Bunkyo" "Arakawa" "Itami" "Nishitokyo" "Yamaguchi" "Uji"		
	}
}

JAP_WEATHER = {
	name = NAME_THEME_WEATHER

	for_countries = { JAP }

	type = ship

	unique = {
		"Kawakaze" "Tanikaze" "Minekaze" "Sawakaze" "Okikaze" "Shimakaze" "Nadakaze" "Yakaze" "Hakaze" "Shiokaze" "Akikaze" "Yukaze" "Tachikaze" "Hokaze" "Nokaze" 
		"Namikaze" "Numakaze" "Kamikaze" "Asakaze" "Harukaze" "Matsukaze" "Hatakaze" "Oite" "Hayate" "Asanagi" "Yunagi" 
		"Fubuki" "Shirayuki" "Hatsuyuki" "Miyuki" "Murakumo" "Shinonome" "Usugumo" "Shirakumo" "Isonami" "Uranami" "Ayanami" "Shikinami" 
		"Asagiri" "Yugiri" "Amagiri" "Sagiri" "Oboro" "Akebono" "Sazanami" "Ushio" "Akatsuki" "Hibiki" "Ikazuchi" "Inazuma" 
		"Shiratsuyu" "Shigure" "Murasame" "Yudachi" "Umikaze" "Samidare" "Yamakaze" "Harusame" "Suzukaze"
		"Asashio" "Arashio" "Oshio" "Michishio" "Yamagumo" "Natsugumo" "Minegumo" "Asagumo" "Arare" "Kasumi"
		"Kagero" "Shiranui" "Kuroshio" "Yukikaze" "Oyashio" "Hayashio" "Natsushio" "Hatsukaze" "Amatsukaze" "Isokaze" "Tokitsukaze" "Urakaze" "Arashi" 
		"Hagikaze" "Nowaki" "Tanikaze" "Hamakaze" "Maikaze" "Akigumo" "Yugumo" "Makigumo" "Kazagumo" "Naganami" "Makinami" "Takanami" "Onami" "Kiyonami" "Tamanami" 
		"Suzunami" "Fujinami" "Hayanami" "Hamanami" "Asashimo" "Okinami" "Kishinami" "Hayashimo" "Akishimo" "Kiyoshimo"
	}
}

JAP_NATURE = {
	name = NAME_THEME_NATURE

	for_countries = { JAP }

	type = ship

	unique = {
			"Momo" "Kashi" "Hinoki" "Yanagi" "Enoki" "Maki" "Keyaki" "Kuwa" "Tsubaki" "Nara" 
			"Momi" "Kaya" "Nashi" "Take" "Kaki" "Tsuga" "Nire" "Kuri" "Kiku" "Aoi" "Fuji" "Hagi" "Susuki" "Hishi" "Hasu" "Warabi" "Tade" "Sumire" "Tsuta" "Ashi" 
			"Yomogi" "Wakatake" "Kuretake" "Sanae" "Sarawabi" "Asagao" "Yugao" "Fuyo" "Karukaya"
			"Matsu" "Ume" "Kiri" "Sugi" "Sakura" "Kaba" "Kaede" "Yaezakura" "Yadake" "Kudzu" "Madake" "Hayaume" "Tobiume" "Wakazakura" "Yamazakura" 
			"Tachibana" "Shii" "Kusunoki" "Odake" "Hatsuzakura" "Hatsuume" "Azusa" "Mikura" "Miyake" "Awaji" "Kurahashi" "Nomi" "Chiburi" "Yashiro" 
			"Kusagaki" "Shinodake" "Shiraume" "Kashiwa" "Kigiku" "Hatsugiku" "Akane" "Shiragiku" "Chigusa" "Wakakusa" "Natsugusa" "Akikusa" "Tochi" "Nogiku" 			
		}
}

JAP_RIVER_MOUNTAIN = {
	name = NAME_THEME_RIVER_MOUNTAIN

	for_countries = { JAP }

	type = ship

	unique = {
		"Asama" "Furutaka" "Kako" "Aoba" "Kinugasa" "Myoko" "Nachi" "Haguro" "Ashigara" "Takao" "Atago" "Maya" "Chokai" "Mogami" "Mikuma" "Suzuya" 
		"Kumano" "Tone" "Chikuma" "Ibuki" "Yodo" "Hirado" "Yahagi" "Tenryu" "Tatsuta" "Kuma" "Tama" "Kitikami" "Oi" "Kiso" "Nagara" "Isuzu" "Natori" 
		"Yura" "Kinu" "Abukuma" "Yubari" "Sendai" "Jintsu" "Naka" "Agano" "Noshiro" "Yahagi" "Sakawa" "Oyodo"
		"Ayase" "Minase" "Otonase" 
	}
}
